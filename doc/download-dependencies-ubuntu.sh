#!/bin/bash

function ask {
while true; do
if [ "${2:-}" = "Y" ]; then
prompt="Y/n"
default=Y
elif [ "${2:-}" = "N" ]; then
prompt="y/N"
default=N
else
prompt="y/n"
default=
fi
# Ask the question
read -p "$1 [$prompt] " REPLY
# Default?
if [ -z "$REPLY" ]; then
REPLY=$default
fi
# Check if the reply is valid
case "$REPLY" in
Y*|y*) return 0 ;;
N*|n*) return 1 ;;
esac
done
}

INSTALLDIR=~/dependencies-prototype-implementation

if ask "Do you want to install apt-get dependencies now?"; then
    sudo apt-get -y install build-essential libssl-dev trousers libtspi-dev libcrypto++-dev
fi

if ask "Do you want to download/install Boost now?"; then
    mkdir -p /tmp/1234-dep-install/
    cd /tmp/1234-dep-install/

    wget http://netcologne.dl.sourceforge.net/project/boost/boost/1.55.0/boost_1_55_0.tar.gz

    echo "Extracting archive now..."
    tar -xf boost_1_55_0.tar.gz

    echo "Installing boost library now"
    mkdir -p $INSTALLDIR
    mv boost_1_55_0/ $INSTALLDIR/

    cd $INSTALLDIR/boost_1_55_0/tools/build/v2/

    ./bootstrap.sh > /dev/null

    ./b2 install --prefix=$INSTALLDIR/boost_build

    if ask "Add $INSTALLDIR/boost_build/bin to \$PATH by modifying .bashrc?"; then
         echo "" >> ~/.bashrc
         echo "export PATH=\$PATH:$INSTALLDIR/boost_build/bin" >> ~/.bashrc
    fi
    
    if ask "Add $INSTALLDIR/boost_1_55_0/ to environment variable \$BOOST_ROOT by modifying .bashrc?"; then
        echo "export BOOST_ROOT=$INSTALLDIR/boost_1_55_0" >> ~/.bashrc
    fi
fi

if ask "Do you wish to modify ~/.bashrc to set required environment variables now?"; then
     echo "" >> ~/.bashrc
     echo "export PATH=\$PATH:$INSTALLDIR/boost_build/bin" >> ~/.bashrc
     echo "export OPENSSL_ROOT=/usr" >> ~/.bashrc
     echo "export CRYPTOPP_ROOT=/usr" >> ~/.bashrc
     echo "export TROUSERS_ROOT=/usr" >> ~/.bashrc
     echo "export BOOST_ROOT=$INSTALLDIR/boost_1_55_0" >> ~/.bashrc
     echo "" >> ~/.bashrc

     echo "Restart your terminal for the changes to take effect"
fi

