#!/bin/sh
# http://blog.didierstevens.com/2008/12/30/howto-make-your-own-cert-with-openssl/

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <newfilename> <serialnumber>"
    exit 0;
fi

OPENSSL=/c/OpenSSL-Win32/bin/openssl.exe

NAME=$1
SERIAL=$2

$OPENSSL genrsa -out $NAME.key 4096
$OPENSSL req -new -key $NAME.key -out $NAME.csr
$OPENSSL x509 -req -days 365 -in $NAME.csr -CA ca.crt -CAkey ca.key -set_serial $SERIAL -out $NAME.crt
cat $NAME.key >> $NAME.crt
rm $NAME.key
rm $NAME.csr