# README #

This is a prototype implementation of a new logging suite (including client, server and verifier applications). The client signs each log message before storing/transmitting them to a syslog server. The supported methods of signing are:

*    Regular RSA-signing (`RsaSigner`)
*    Hash-Chain-signing (`HashChainSigner`)
*    Itkis-Reyzin-based-FSS-signing (`ItkizReyzinSigner`)
*    RSA-singing with Trusted Platform Module(`TpmRsaSigner`)

You can read more about the functionality of each signer in the master thesis accompanied by this project.

It also includes an truncation detection module (again read the thesis), called `AntiTruncationTimestamper`.

Additional documentation regarding the functionality of the client can be found in `example-client-config.xml`.

## COMPILATION ##

1.    Download the git repository to somewhere on your hard drive.
2.    Download the dependencies (see below).
3.    Run the b2 command (see below).
3.    Profit.

### DEPENDENCIES ###

There are four external dependencies (plus the "Boost.Build" make system): 

*    BOOST library 1.55.0 
     -    BOOST.BUILD
*    OpenSSL
*    Crypto++ 5.6.1
*    TrouSerS (linux only, used by TpmRsaSigner)

Below are the necessary instructions to install these dependencies on your system:

**For Ubuntu users:**

There are now a script that can be used for installing *all* external dependencies!

Simply run `doc/download-dependencies-ubuntu.sh` and respond to the on-screen questions.

#### OpenSSL ####

General steps:

1.    Download
2.    Install
3.    Set OPENSSL_ROOT environment variable

##### Windows #####

1.    Download the _Visual C++ 2008 Redistributables_ from [here](http://slproweb.com/products/Win32OpenSSL.html).
2.    Install the _Visual C++ 2008 Redistributables_.
3.    Download the _Win32 OpenSSL v1.0.1f_ or later pre-compiled binary distributions from [here](http://slproweb.com/products/Win32OpenSSL.html)
4.    Install the pre-compiled binarys.
5.    Create OPENSSL_ROOT environment variable to "C:\OpenSSL-Win32" or whereever you installed OpenSSL.

##### Linux #####

Ubuntu: 
1.    `sudo apt-get install libssl-dev`
2.    Create OPENSSL_ROOT environment variable to /usr/ e.g. by issuing th ecommand: `echo -e "\nexport OPENSSL_ROOT=/usr/" >> ~./bashrc`

#### BOOST ####

Both Linux and Windows:

1. Download boost library 1.55.0 from [here](http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.zip/download)
2. Extract (to `C:\boost_1_55_0\` or `/usr/local/boost_1_55_0` for example). 
3. Create BOOST_ROOT environment variable pointing to `C:\boost_1_55_0` or wherever you extracted boost to.

##### BOOST.BUILD #####

1. Navigate to the `BOOST_ROOT/tools/build/v2` folder.
2. Run `./bootstrap.bat` or `./bootstrap.sh` from a terminal prompt.
3. Run `./b2 --prefix=INSTALLDIR` where `INSTALLDIR` is whatever directory path you wish to install Boost.Build to. For example `C:\Boost.Build` or `/usr/local/boost_build`.
4. Add `INSTALLDIR/bin` to your PATH environment variable.

#### Crypto++ ####

General steps:

1.    Download
2.    Install
3.    Set CRYPTOPP_ROOT environment variable

##### Windows #####

1. Download the latest version from [here](http://www.cryptopp.com/)
2. Extract to a directory of your choosing.
3. Open the visual studio solution.
4. Build the projects.
5. Create the CRYPTOPP_ROOT environment variable and let it point to the root directory of your extracted crypto++ files.

##### Linux (Ubuntu) #####

1. `sudo apt-get install libcrypto++-dev`
2. Create CRYPTOPP_ROOT environment variable to /usr/ e.g. by issuing th ecommand: `echo -e "\nexport CRYPTOPP_ROOT=/usr/" >> ~./bashrc`

#### TrouSerS (linux only) ####

Ubuntu:

1. `sudo apt-get install trousers libtspi-dev`
2. `echo "export TROUSERS_ROOT=/usr" >> ~/.bashrc`

### BUILD ###

**Note** that the git repository depends on a few submodules that also must be initialized after cloning:

```git submodule update --init --recursive```

Now that all dependencies are installed we can finaly compile the client and server with the following command

Navigate to the source folder (the location of this README.md).

```b2```

This is the most simple way of compiling the program, but since we are probably using a multi-core computer we can speed things up significantly by using the `-j NJOBS` command.

```b2 -j 5 ```

This builds the project with 5 parallel jobs, speeding things up significantly on a multi-core machine.

You can also build either client or server separately with the `b2 Client` or `b2 Server` commands.

The compiled results are Server and Client (Server.exe and Client.exe on windows) in the `./bin/<toolset>/threading-multi/` folder.

**Notes on Linux**

There have been problems with compiling the project on any other version of GCC than 4.7, if you are experiencing difficulties in getting the program to compile please try to install g++-4.7 and compile with the following command instead:

``` b2 toolset=gcc-4.7 ```
 
## INSTALLATION ##

For server:

```b2 install-server ```

For client:

```b2 install-client ```

For both:

```b2 install-all ```

The above commands copies the relevant binaries to the `dist` folder in the project directory. Take the contents of that folder and put them anywhere you want (INSTALLDIR).

## USAGE ##

### Preparing SSL certificates ###

Please refer to other documentations regarding how to create your own certificates (an example can be found inside the `test` folder though).

### Server ###

```Server.exe --help```

Should answer the most immediate questions regarding running the program. Almost all configuration errors will be displayed immediately upon launching the program.

Upon receiving logs from a client it stores them in a sqlite3 database file in the `logs` folder (can be changed from the command line). Each client has it's own database file.

### Client ###

```Client.exe --help```

Should answer the most immediate questions regarding running the program. Almost all configuration errors will be displayed immediately upon launching the program.

Please see the `example-client-config.xml` document for the most up-to-date information regarding configuration of the client.

#### TpmRsaSigner ####

In its current state TpmRsaSigner needs the TPM to have both its ownership authorization and passkey for the SRK to be set to the "well-known-value" (20 bytes of zeros). You can do this with the following command:

```tpm_takeownership -y -z```

See the TrouSerS library documentation for more information.

# Verifcation application #

The `verifier/` sub-directory contains a simple GUI verifier written in C# and visual studio 2013. This application can open the log files produced by the server for each connected client and perform verification of each log entry based on their signatures.

Please note that this application reads the _entire_ log file into memory. It may therefore crash when reading very large log files. This limitation should be fixed before using this application in a production environment.

# Documentation #

We recommend all users of this prototype to at least glance through our master thesis which is based on this prototype. It is the best source for information regarding the signers and the verification process.