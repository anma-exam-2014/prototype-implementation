#ifndef LOGMESSAGE_H
#define LOGMESSAGE_H

#include <boost/shared_ptr.hpp>
#include <string>
#include <map>
#include <stdexcept>

namespace Common
{
	class LogMessage
	{
	public:
		typedef boost::shared_ptr<LogMessage> Ptr;
		typedef std::map<std::string, std::string> StructuredDataContent;
		typedef std::map<std::string, StructuredDataContent> StructuredDataCollection;

		enum FacilityNumber
		{
			//Kernel messages
			FN_KERN,
			//User-level messages
			FN_USER,
			//mail system
			FN_MAIL,
			//system daemons
			FN_DAEMON,
			//security/authorization messages
			FN_AUTH,
			//Messages generated internally by syslogd
			FN_SYSLOG,
			//line printer subsystem
			FN_LPR,
			//network news subsystem
			FN_NEWS,
			//UUCP subsystem
			FN_UUCP,
			//clock daemon
			FN_CLOCK,
			//security/authorization messages
			FN_AUTHPRIV,
			//FTP daemon
			FN_FTP,
			//NTP subsystem
			FN_NTP,
			//log audit
			FN_LOG_AUDIT,
			//log alert
			FN_LOG_ALERT,
			//clock daemon
			FN_CRON,
			//local use 0
			FN_LOCAL0,
			//local use 1
			FN_LOCAL1,
			//local use 2
			FN_LOCAL2,
			//local use 3
			FN_LOCAL3,
			//local use 4
			FN_LOCAL4,
			//local use 5
			FN_LOCAL5,
			//local use 6
			FN_LOCAL6,
			//local use 7
			FN_LOCAL7
		};

		enum SeverityValue
		{
			Emergency,
			Alert,
			Critical,
			Error,
			Warning,
			Notice,
			Informational,
			Debug
		};

		LogMessage(FacilityNumber fn, SeverityValue sv, bool SignThis = true);

		FacilityNumber GetFacilityNumber();
		void SetFacilityNumber(FacilityNumber fn);

		SeverityValue GetSeverityValue();
		void SetSeverityValue(SeverityValue sv);

		std::string GetContent();
		void SetContent(std::string);

		std::string GetAppName();
		void SetAppName(std::string);

		std::string GetMessageType();
		void SetMessageType(std::string);

		uint64_t GetTimestamp();
		void SetTimestamp(uint64_t);

		StructuredDataCollection& GetStructuredData();

		//Takes all current values and generates a valid syslog message
		std::string GenerateFrame();

        //Indicates whether or not this log message should be signed
        //beeing signed. Only false for special messages already containing
        //some sort of signature
        bool DoNotSignThis()
        {
            return !signMe;
        }

		// any non empty string is set as the NEW default value.
		// returns value as set _before_ method was called
		static std::string GlobalVersion(std::string newValue = "");

		// any non empty string is set as the NEW default value.
		// returns value as set _before_ method was called
		static std::string GlobalHostname(std::string newValue = "");

		// any non empty string is set as the NEW default value.
		// returns value as set _before_ method was called
		static std::string GlobalProcid(std::string newValue = "");

        struct ParsedData
        {
            FacilityNumber facilityNumber;
            SeverityValue severityValues;
            std::string timestamp;
            std::string hostname;
            std::string appName;
            std::string procId;
            std::string messageType;
            std::string structuredData;
            std::string content;
        };

        static ParsedData ParseString(std::string message);

        class ParseError : public std::runtime_error
        {
        public:
            ParseError(const std::string& msg)
                : std::runtime_error(msg)
            {}
        };

	private:
        bool signMe = true;
		std::string content;
		std::string messageType;
		std::string appName;
		uint64_t timestamp;
		StructuredDataCollection structuredDataCollection;
		FacilityNumber facilityNumber;
		SeverityValue severityValues;
	};
}

#endif
