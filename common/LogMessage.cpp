#include "LogMessage.h"
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/predicate.hpp>

using namespace Common;

LogMessage::LogMessage(LogMessage::FacilityNumber fn, LogMessage::SeverityValue sv, bool SignThis)
{
	facilityNumber = fn;
	severityValues = sv;
	appName = "-";
	messageType = "-";
    signMe = SignThis;
}

LogMessage::FacilityNumber LogMessage::GetFacilityNumber()
{
	return facilityNumber;
}

void LogMessage::SetFacilityNumber(LogMessage::FacilityNumber fn)
{
	facilityNumber = fn;
}

LogMessage::SeverityValue LogMessage::GetSeverityValue()
{
	return severityValues;
}

void LogMessage::SetSeverityValue(LogMessage::SeverityValue sv)
{
	severityValues = sv;
}

std::string LogMessage::GetContent()
{
	return content;
}

void LogMessage::SetContent(std::string content)
{
	this->content = content;
}

uint64_t LogMessage::GetTimestamp()
{
	return timestamp;
}

void LogMessage::SetTimestamp(uint64_t timestamp)
{
	this->timestamp = timestamp;
}

std::string LogMessage::GetAppName()
{
	return appName;
}

void LogMessage::SetAppName(std::string appName)
{
	this->appName = appName;
}

std::string LogMessage::GetMessageType()
{
	return messageType;
}

void LogMessage::SetMessageType(std::string messageType)
{
	this->messageType = messageType;
}

// any non empty string is set as the NEW default value.
// returns value as set _before_ method was called
std::string LogMessage::GlobalVersion(std::string newValue)
{
	static std::string value = "1";
	if (!newValue.empty())
	{
		std::string oldValue = value;
		value = newValue;
		return oldValue;
	}
	return value;
}

// any non empty string is set as the NEW default value.
// returns value as set _before_ method was called
std::string LogMessage::GlobalHostname(std::string newValue)
{
	static std::string value = "-";
	if (!newValue.empty())
	{
		std::string oldValue = value;
		value = newValue;
		return oldValue;
	}
	return value;
}

// any non empty string is set as the NEW default value.
// returns value as set _before_ method was called
std::string LogMessage::GlobalProcid(std::string newValue)
{
	static std::string value = "-";
	if (!newValue.empty())
	{
		std::string oldValue = value;
		value = newValue;
		return oldValue;
	}
	return value;
}


LogMessage::StructuredDataCollection& LogMessage::GetStructuredData()
{
	return structuredDataCollection;
}

//SPACE as separator
#define SP '\x20'

int calculatePrival(LogMessage::FacilityNumber fn, LogMessage::SeverityValue sv);
std::string convertTimestamp(uint64_t time);
std::string generateStructuredData(LogMessage::StructuredDataCollection collection);

//Takes all current values and generates a valid syslog message
std::string LogMessage::GenerateFrame()
{

	std::stringstream frame;
	frame << '<' << calculatePrival(this->GetFacilityNumber(), this->GetSeverityValue()) << '>';
	frame << GlobalVersion();
	frame << SP;
	frame << convertTimestamp(this->GetTimestamp());
	frame << SP;
	frame << GlobalHostname();
	frame << SP;
	frame << this->GetAppName();
	frame << SP;
	frame << GlobalProcid();
	frame << SP;
	frame << this->GetMessageType();
	frame << SP;
	frame << generateStructuredData(this->GetStructuredData());
	frame << SP;
	frame << this->GetContent();
	return frame.str();
}

int calculatePrival(LogMessage::FacilityNumber fn, LogMessage::SeverityValue sv)
{
	return ((int)fn) * 8 + ((int)sv);
}


std::string convertTimestamp(uint64_t millis)
{
	const boost::posix_time::ptime timestamp(boost::gregorian::date(1970, boost::date_time::Jan, 1),
		boost::posix_time::milliseconds(millis));

	//// Get the time offset in current day
	const boost::posix_time::time_duration td = timestamp.time_of_day();

	const long hours = td.hours();
	const long minutes = td.minutes();
	const long seconds = td.seconds();
	const long milliseconds = (long)(td.total_milliseconds() - ((hours * 3600 + minutes * 60 + seconds) * 1000));
	const long day = timestamp.date().day().as_number();
	const long month = timestamp.date().month().as_number();
	const long year = timestamp.date().year();

	char buf[50];
#ifdef _WIN32
#pragma warning(disable : 4996)
#endif
	sprintf(buf, "%04ld-%02ld-%02ldT%02ld:%02ld:%02ld.%03ldZ", year, month, day, hours, minutes, seconds, milliseconds);

	return buf;
}

std::string escapeStructuredData(std::string data)
{
	std::stringstream escaped;

	BOOST_FOREACH(const char ch, data)
	{
		if (ch == '"' || ch == '\\' || ch == ']')
			escaped << '\\';
		escaped << ch;
	}

	return escaped.str();
}

std::string generateStructuredData(LogMessage::StructuredDataCollection collection)
{
	if (collection.empty())
		return "-";

	std::stringstream data;

	BOOST_FOREACH(LogMessage::StructuredDataCollection::value_type& outer, collection)
	{
		data << '[' << outer.first;

		BOOST_FOREACH(LogMessage::StructuredDataContent::value_type& inner, outer.second)
		{
			data << SP << inner.first << "=\"" << escapeStructuredData(inner.second) << '"';
		}

		data << ']';
	}

	return data.str();
}

LogMessage::ParsedData LogMessage::ParseString(std::string message)
{
    char c;
    std::stringstream ss(message);
    int prival;
    int version;

    ParsedData log;

    ss >> c;
    if (c != '<')
        throw ParseError(std::string("expected \'") + '<' + "\'");

    ss >> prival;
    try{
        log.facilityNumber = (FacilityNumber)(prival >> 3);
        log.severityValues = (SeverityValue)(prival & 7);
    }
    catch (const boost::bad_lexical_cast&)
    {
        throw ParseError("Could not validate facility number or severity value.");
    }

    ss >> c;
    if (c != '>')
        throw ParseError(std::string("expected \'") + '>' + "\'");

    ss >> version;

    if (version != 1)
        throw ParseError(
        "Only support of syslog verison 1 (RFC 5424) is implemented: expected 1 got: "
        + boost::lexical_cast<std::string>(version)
        );

    ss >> log.timestamp >> log.hostname >> log.appName >> log.procId >> log.messageType;

    //read structured data
    ss >> log.structuredData;

    if (log.structuredData != "-")
    {
        if (!boost::algorithm::starts_with(log.structuredData, "["))
            throw ParseError("Expected '[' got " + log.structuredData);

        std::string tmp;
        do{
            ss >> tmp;
            log.structuredData += " " + tmp;
        } while (!boost::algorithm::ends_with(tmp, "\"]"));
    }

    //content
    log.content = ss.str().substr((unsigned int)ss.tellg() + 1);

    return log;
}