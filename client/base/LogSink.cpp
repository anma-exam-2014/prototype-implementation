#include "LogSink.h"
#include "../core/PersistentStorage.h"
#include "../core/ThreadFactory.h"
#include "LogAggregator.h"

#include <boost/lexical_cast.hpp>

using namespace Base;
using namespace Base::FactoryImpl;


SinkCreator::SinkCreator(const std::string& classname)
{
	Core::ThreadFactory::registerSink(classname, this);
}

LogSink::LogSink(Core::ConfigOptions options)
{
}

void LogSink::LoopTick(uint64_t realTickLengtMs)
{
    auto info = LoopTickUpdate(realTickLengtMs);
    BaseTransmitter::PublishLastRecieverStatus(info);
}