#include "LogAggregator.h"
#include "../core/ThreadFactory.h"

using namespace Base;
using namespace Base::FactoryImpl;

AggregatorCreator::AggregatorCreator(const std::string& classname)
{
	Core::ThreadFactory::registerAggregator(classname, this);
}

LogAggregator::LogAggregator(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: BaseTransmitter(nextStep)
{
}

LogAggregator::~LogAggregator()
{
}

