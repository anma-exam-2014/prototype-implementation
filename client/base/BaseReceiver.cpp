#include "BaseReceiver.h"
#include "../core/PersistentStorage.h"

#include <boost/lexical_cast.hpp>

using namespace Base;
using namespace Core;

class StopException{};

BaseReceiver::BaseReceiver()
{
}

void BaseReceiver::Main()
{
	static std::string fieldname = InstanceName() + ".ReceivedMessages";
	std::string value = Core::PersistentStorageService::getInstance()->GetSync(fieldname);
	if (!value.empty())
		_ReceievedMessagesAllTime = boost::lexical_cast<long>(value);
	else
		_ReceievedMessagesAllTime = 0;

	try{
		lastTick = boost::get_system_time();
		while (true)
		{
			CallHandleMessages(WaitForMessage());
		}
	}
	catch (StopException)
	{
		//do nothing, just quit the thread
	}
}

void BaseReceiver::CallHandleMessages(MsgPtrArr msgs)
{
	boost::mutex::scoped_lock lock(handleMessageMutex);
	AddToCounterReceievedMessagesAllTime(msgs.size());
	HandleMessages(msgs);
}

void BaseReceiver::Stop()
{
	shouldStop = true;
}

void BaseReceiver::PutMessageOnQueue(Common::LogMessage::Ptr msg)
{
	boost::mutex::scoped_lock lock(mutex);
	bool const was_empty = queue.empty();
	queue.push_back(msg);

	lock.unlock(); // unlock the mutex BEFORE notify_one()

	if (was_empty)
	{
		condition_variable.notify_one();
	}
}

void BaseReceiver::PutMessagesOnQueue(MsgPtrArr msgs)
{
	boost::mutex::scoped_lock lock(mutex);
	bool const was_empty = queue.empty();
	queue.insert(queue.end(), msgs.begin(), msgs.end());

	lock.unlock(); // unlock the mutex BEFORE notify_one()

	if (was_empty)
	{
		condition_variable.notify_one();
	}
}

std::vector<Common::LogMessage::Ptr> BaseReceiver::WaitForMessage()
{
	boost::mutex::scoped_lock lock(mutex);
	while (queue.empty())
	{
		if (shouldStop)
			throw StopException();
		boost::system_time timeout = lastTick + boost::posix_time::milliseconds(100);
		condition_variable.timed_wait(lock, timeout);
		auto currentTime = boost::get_system_time();
		LoopTick((currentTime - lastTick).total_milliseconds());
		lastTick = currentTime;
	}

	MsgPtrArr copy(queue.begin(), queue.end());
	queue.clear();
	return copy;
}

int BaseReceiver::MessageCount()
{
	boost::mutex::scoped_lock lock(mutex);
	return queue.size();
}

void BaseReceiver::AddToCounterReceievedMessagesAllTime(long toAdd)
{
	static std::string fieldname = InstanceName() + ".ReceivedMessages";
	_ReceievedMessagesAllTime += toAdd;
	PersistentStorageService::getInstance()->SetAsync(fieldname, boost::lexical_cast<std::string>(_ReceievedMessagesAllTime));
}

void BaseReceiver::BlockHandleMessagesWhile(std::function< void(void) > lambda)
{
	boost::mutex::scoped_lock lock(handleMessageMutex);
	lambda();
}