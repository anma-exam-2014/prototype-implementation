#include "LogSource.h"
#include "../core/ThreadFactory.h"

using namespace Base;
using namespace Base::FactoryImpl;


SourceCreator::SourceCreator(const std::string& classname)
{
	Core::ThreadFactory::registerSource(classname, this);
}

LogSource::LogSource(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: BaseTransmitter(nextStep)
{
	uniqueConfigName = options["unique-id"];
	if (uniqueConfigName.empty())
	{
		throw ConstructionException("Option \"unique-id\" must be set for LogSources!");
	}
}