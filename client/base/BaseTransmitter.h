#ifndef BASE_TRANSMITTER_H
#define BASE_TRANSMITTER_H

#include "../../common/LogMessage.h"
#include "BaseThread.h"
#include "BaseReceiver.h"
#include <boost/thread.hpp>
#include <vector>

namespace Base
{
	class BaseTransmitter : public virtual BaseThread
	{
	public:
		typedef boost::shared_ptr<BaseTransmitter> Ptr;

		BaseTransmitter() = delete;
		BaseTransmitter(BaseTransmitter& cpy) = delete;

        struct ReceiverStatusInfo
        {
            //if the queue of the last reciever step is empty
            bool queueEmpty;
            //if queueEmpty is true, then this is the time it has been empty
            //if queueEmpty is false the value of this is undefined.
            uint64_t timeEmptyQueue;
            //the sequence number of the last message to be handled by
            //the last reciever step
            uint64_t lastMessageSequenceNumber;
            //if the last reciever step is ready to recieve more messages
            bool canSendImmediately;
        };

        static void PublishLastRecieverStatus(ReceiverStatusInfo info);

        virtual ~BaseTransmitter();

	protected:

		BaseTransmitter(BaseReceiver::Ptr nextStep);

		BaseReceiver::Ptr GetNextStep();

        virtual void HandleLastRecieverStatusUpdate(ReceiverStatusInfo info) {};

	private:
		BaseReceiver::Ptr nextStep;

        static boost::mutex staticMutex;
        static std::vector<BaseTransmitter*> instances;
	};
}

#endif