#include "BaseTransmitter.h"

using namespace Base;

std::vector<BaseTransmitter*> BaseTransmitter::instances;
boost::mutex BaseTransmitter::staticMutex;

BaseTransmitter::BaseTransmitter(BaseReceiver::Ptr nextStep)
{
    boost::mutex::scoped_lock lock(staticMutex);
    this->nextStep = nextStep;
    instances.push_back(this);
}

BaseTransmitter::~BaseTransmitter()
{
    boost::mutex::scoped_lock lock(staticMutex);
    instances.erase(std::remove(instances.begin(), instances.end(), this), instances.end());
}

BaseReceiver::Ptr BaseTransmitter::GetNextStep()
{
	return this->nextStep;
}

void BaseTransmitter::PublishLastRecieverStatus(ReceiverStatusInfo info)
{
    for (auto instance : instances)
    {
        instance->HandleLastRecieverStatusUpdate(info);
    }
}