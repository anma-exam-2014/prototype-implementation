#include "BaseThread.h"

#include <boost/foreach.hpp>
#include <vector>

using namespace Base;

void VerifyNonDuplicateName(std::string instanceName);

BaseThread::BaseThread()
{
	t = 0;
	status = NOT_STARTED;
}

BaseThread::~BaseThread()
{
	delete t;
}

BaseThread::STATUS BaseThread::GetStatus()
{
	return status;
}

std::exception BaseThread::GetException()
{
	return exception;
}

void BaseThread::_start()
{
	try
	{
		Main();
	}
	catch (std::exception &e)
	{
		exception = e;
		status = STOPPED_ERROR;
	}
}

void BaseThread::Start()
{
	//check if there are any dublicate names
	//before we start
	VerifyNonDuplicateName(InstanceName());

	t = new boost::thread([this]()
	{
		this->_start();
	}
	);
	status = RUNNING;
}

void BaseThread::StopJoin(bool force)
{
    //notify thread to quite nicely
	Stop();
    if (force)
    {
        //forcibly interrupt thread, not nice
        t->interrupt();
    }

    //we give the thread some time to quit
	if (t->try_join_for(boost::chrono::milliseconds(60000)))
	{
        //we can't wait forever so we forcibly interrupt the thread
		t->interrupt();
        //should not be long now
		t->join();
	}
    //alright, we are done
	status = FINISHED;
}

void VerifyNonDuplicateName(std::string instanceName)
{
	static std::vector<std::string> allThreadNames(10);

	for (std::string& name : allThreadNames)
	{
		if (instanceName == name)
		{
			throw std::runtime_error("Duplicate \"unique-id\" detected for value \"" + instanceName + "\"!");
		}
	}
	allThreadNames.push_back(instanceName);
}