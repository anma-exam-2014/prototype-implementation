#ifndef BASE_RECEIEVER_H
#define BASE_RECEIEVER_H

#include <vector>
#include <boost/thread.hpp>
#include <boost/foreach.hpp>
#include "BaseThread.h"
#include "../../common/LogMessage.h"

//forward declaration, neccessary for benchmarking
namespace Core{ class Benchmark; }

namespace Base
{
	class BaseReceiver : public virtual BaseThread
	{
	public:
		typedef boost::shared_ptr<BaseReceiver> Ptr;
		typedef std::vector<Common::LogMessage::Ptr> MsgPtrArr;

		BaseReceiver();

		void PutMessageOnQueue(Common::LogMessage::Ptr msg);
		void PutMessagesOnQueue(MsgPtrArr msgs);

	protected:

		virtual void Main() override;
		virtual void Stop() override;

		virtual void HandleMessages(MsgPtrArr msg) = 0;
		void CallHandleMessages(MsgPtrArr msg);

		virtual void LoopTick(uint64_t realTickLengtMs) {};

		long ReceievedMessagesAllTime(){
			return _ReceievedMessagesAllTime;
		};

		void BlockHandleMessagesWhile(std::function< void(void) > lambda);

	private:

		MsgPtrArr WaitForMessage();
		int MessageCount();

		void AddToCounterReceievedMessagesAllTime(long);
		long _ReceievedMessagesAllTime;

		bool shouldStop = false;

		boost::posix_time::ptime lastTick;

		//Direct access to queue is forbidden except by 
		// mutex protected member functions
		MsgPtrArr queue;
		mutable boost::mutex mutex;
		mutable boost::mutex handleMessageMutex;
		boost::condition_variable condition_variable;

		//Neccessary for benchmarking
		friend class Core::Benchmark;
	};
}

#endif
