#include "LogFilter.h"
#include "../core/ThreadFactory.h"

using namespace Base;
using namespace Base::FactoryImpl;


FilterCreator::FilterCreator(const std::string& classname)
{
	Core::ThreadFactory::registerFilter(classname, this);
}

LogFilter::LogFilter(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: BaseTransmitter(nextStep)
{
	uniqueConfigName = options["unique-id"];
	if (uniqueConfigName.empty())
	{
		throw ConstructionException("Option \"unique-id\" must be set for LogFilters!");
	}
}