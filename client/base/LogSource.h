#ifndef LOG_SOURCE_H
#define LOG_SOURCE_H

#include "BaseReceiver.h"
#include "BaseTransmitter.h"
#include "../core/ConfigParser.h"

#ifdef _WIN32
#pragma warning( disable: 4250 )
#endif

namespace Base
{
	class LogSource : public BaseTransmitter
	{
	public:
		typedef boost::shared_ptr<LogSource> Ptr;

		LogSource(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

	protected:
		std::string InstanceName() override
		{
			return uniqueConfigName;
		}
	private:
		std::string uniqueConfigName;
	};

	namespace FactoryImpl
	{
		class SourceCreator
		{
		public:
			SourceCreator(const std::string& classname);
			virtual ~SourceCreator() {};

			virtual LogSource* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) = 0;
		};

		template <class T>
		class SourceCreatorImpl : public SourceCreator
		{
		public:
			SourceCreatorImpl<T>(const std::string& classname) : SourceCreator(classname) {}
			virtual ~SourceCreatorImpl<T>() {}

			virtual LogSource* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
			{
				return new T(nextStep, options);
			}
		};
	}
}

#define REGISTER_SOURCE_CLASS(classname) \
	private: \
	static const Base::FactoryImpl::SourceCreatorImpl<classname> creator;

#define REGISTER_SOURCE_CLASS_IMPL(classname) \
	const Base::FactoryImpl::SourceCreatorImpl<classname> classname::creator(#classname);


#endif