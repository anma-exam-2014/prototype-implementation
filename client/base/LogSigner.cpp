#include "LogSigner.h"
#include "../core/ThreadFactory.h"

using namespace Base;
using namespace Base::FactoryImpl;


SignerCreator::SignerCreator(const std::string& classname)
{
	Core::ThreadFactory::registerSigner(classname, this);
}

LogSigner::LogSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: BaseTransmitter(nextStep)
{
}