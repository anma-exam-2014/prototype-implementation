#ifndef LOG_SINK_H
#define LOG_SINK_H

#include "../core/ConfigParser.h"
#include "BaseReceiver.h"
#include "BaseTransmitter.h"

#ifdef _WIN32
#pragma warning( disable: 4250 )
#endif

namespace Base
{

	class LogSink : public BaseReceiver
	{
	public:
		typedef boost::shared_ptr<LogSink> Ptr;
		
		LogSink(Core::ConfigOptions options);

        virtual void HandleMessages(MsgPtrArr msg) = 0;

    protected:

        virtual void LoopTick(uint64_t realTickLengtMs) override;
        virtual BaseTransmitter::ReceiverStatusInfo LoopTickUpdate(uint64_t realTickLengtMs) = 0;

	};

	namespace FactoryImpl
	{
		class SinkCreator
		{
		public:
			SinkCreator(const std::string& classname);
			virtual ~SinkCreator() {};

			virtual LogSink* create(Core::ConfigOptions options) = 0;
		};

		template <class T>
		class SinkCreatorImpl : public SinkCreator
		{
		public:
			SinkCreatorImpl<T>(const std::string& classname) : SinkCreator(classname) {}
			virtual ~SinkCreatorImpl<T>() {}

			virtual LogSink* create(Core::ConfigOptions options)
			{ 
				return new T(options);
			}
		};
	}
}

#define REGISTER_SINK_CLASS(classname) \
	private: \
	static const Base::FactoryImpl::SinkCreatorImpl<classname> creator;

#define REGISTER_SINK_CLASS_IMPL(classname) \
	const Base::FactoryImpl::SinkCreatorImpl<classname> classname::creator(#classname);

#endif