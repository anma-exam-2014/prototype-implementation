#ifndef LOG_TIME_STAMPER_H
#define LOG_TIME_STAMPER_H

#include "BaseReceiver.h"
#include "BaseTransmitter.h"
#include "../core/ConfigParser.h"
#include <boost/thread.hpp>

#ifdef _WIN32
#pragma warning( disable: 4250 )
#endif

namespace Base
{
	class LogAggregator : public BaseReceiver, public BaseTransmitter
	{
	public:
		typedef boost::shared_ptr<LogAggregator> Ptr;

		LogAggregator(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		virtual ~LogAggregator();
	};

	namespace FactoryImpl
	{
		class AggregatorCreator
		{
		public:
			AggregatorCreator(const std::string& classname);
			virtual ~AggregatorCreator() {};

			virtual LogAggregator* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) = 0;
		};

		template <class T>
		class AggregatorCreatorImpl : public AggregatorCreator
		{
		public:
			AggregatorCreatorImpl<T>(const std::string& classname) : AggregatorCreator(classname) {}
			virtual ~AggregatorCreatorImpl<T>() {}

			virtual LogAggregator* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
			{
				return new T(nextStep, options);
			}
		};
	}
}

#define REGISTER_AGGREGATOR_CLASS(classname) \
	private: \
	static const Base::FactoryImpl::AggregatorCreatorImpl<classname> creator;

#define REGISTER_AGGREGATOR_CLASS_IMPL(classname) \
	const Base::FactoryImpl::AggregatorCreatorImpl<classname> classname::creator(#classname);


#endif