#ifndef LOG_FILTER_H
#define LOG_FILTER_H

#include "BaseReceiver.h"
#include "BaseTransmitter.h"
#include "../core/ConfigParser.h"

#ifdef _WIN32
#pragma warning( disable: 4250 )
#endif

namespace Base
{
	class LogFilter : public BaseReceiver, public BaseTransmitter
	{
	public:
		typedef boost::shared_ptr<LogFilter> Ptr;

		LogFilter(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

	protected:
		std::string InstanceName() override
		{
			return uniqueConfigName;
		}
	private:
		std::string uniqueConfigName;

	};

	namespace FactoryImpl
	{
		class FilterCreator
		{
		public:
			FilterCreator(const std::string& classname);
			virtual ~FilterCreator() {};

			virtual LogFilter* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) = 0;
		};

		template <class T>
		class FilterCreatorImpl : public FilterCreator
		{
		public:
			FilterCreatorImpl<T>(const std::string& classname) : FilterCreator(classname) {}
			virtual ~FilterCreatorImpl<T>() {}

			virtual LogFilter* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
			{
				return new T(nextStep, options);
			}
		};
	}
}

#define REGISTER_FILTER_CLASS(classname) \
	private: \
	static const Base::FactoryImpl::FilterCreatorImpl<classname> creator;

#define REGISTER_FILTER_CLASS_IMPL(classname) \
	const Base::FactoryImpl::FilterCreatorImpl<classname> classname::creator(#classname);


#endif