#ifndef LOG_SIGNER_H
#define LOG_SIGNER_H

#include "BaseReceiver.h"
#include "BaseTransmitter.h"
#include "../core/ConfigParser.h"

#ifdef _WIN32
#pragma warning( disable: 4250 )
#endif

namespace Base
{
	class LogSigner : public BaseReceiver, public BaseTransmitter
	{
	public:
		typedef boost::shared_ptr<LogSigner> Ptr;

		LogSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

	private:

		virtual bool BenchmarkUpdateKey(){ return false;  }

		//Neccessary for benchmarking
		friend class Core::Benchmark;
	};

	namespace FactoryImpl
	{
		class SignerCreator
		{
		public:
			SignerCreator(const std::string& classname);
			virtual ~SignerCreator() {};

			virtual LogSigner* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) = 0;
		};

		template <class T>
		class SignerCreatorImpl : public SignerCreator
		{
		public:
			SignerCreatorImpl<T>(const std::string& classname) : SignerCreator(classname) {}
			virtual ~SignerCreatorImpl<T>() {}

			virtual LogSigner* create(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
			{
				return new T(nextStep, options);
			}
		};
	}
}

#define REGISTER_SIGNER_CLASS(classname) \
	private: \
	static const Base::FactoryImpl::SignerCreatorImpl<classname> creator;

#define REGISTER_SIGNER_CLASS_IMPL(classname) \
	const Base::FactoryImpl::SignerCreatorImpl<classname> classname::creator(#classname);

#endif
