#ifndef BASE_THREAD_H
#define BASE_THREAD_H

#include <boost/shared_ptr.hpp>
#include "../core/BaseException.h"
#include <boost/thread.hpp>

namespace Base
{
	class BaseThread
	{
	public:
		typedef boost::shared_ptr<BaseThread> Ptr;

		enum STATUS{
			NOT_STARTED,
			RUNNING,
			FINISHED,
			STOPPED_ERROR
		};

		BaseThread();
		virtual ~BaseThread();

		STATUS GetStatus();

		std::exception GetException();

		void Start();
		void StopJoin(bool force=false);

		class ConstructionException : public Core::BaseException
		{
		public:
			ConstructionException(const char* const& msg) :Core::BaseException(msg){};
			ConstructionException(const std::string msg) :Core::BaseException(msg){};
		};

	protected:
		virtual void Main() = 0;
		virtual void Stop() = 0;

		//Something unique that persists between 
		//restarts of the application.
		virtual std::string InstanceName() = 0;

	private:
		boost::thread *t;
		STATUS status;
		std::exception exception;
		void _start();
	};
}

#endif