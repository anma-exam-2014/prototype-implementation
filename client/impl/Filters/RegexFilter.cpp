#include "RegexFilter.h"
#include <regex>

using namespace Impl;

REGISTER_FILTER_CLASS_IMPL(RegexFilter)

RegexFilter::RegexFilter(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: LogFilter(nextStep, options)
{
	for (auto i = options.begin(); i != options.end(); i++)
	{
		if (i->first == "pattern")
		{
			if (!i->second.empty())
			{
				pattern = i->second;
			}
			else
			{
				throw BaseThread::ConstructionException("ERROR: Must specify pattern.");
			}
		}
		if (i->first == "mode")
		{
			if (i->second == "inclusive")
			{
				inclusive = true;
			}
			else if (i->second == "exclusive")
			{
				inclusive = false;
			}
			else
			{
				throw BaseThread::ConstructionException("ERROR: Unrecognized mode.");
			}
		}
	}
}

void RegexFilter::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
		if (!pattern.empty())
		{
			if ((inclusive && std::regex_match(msg->GetContent(), std::regex(pattern))) ||
				(!inclusive && !std::regex_match(msg->GetContent(), std::regex(pattern))))
			{
				GetNextStep()->PutMessageOnQueue(msg);
			}
			else
			{
				//Do nothing
			}
		}
	}
}
