#include "../../base/LogFilter.h"

namespace Impl
{
	class RegexFilter : public Base::LogFilter
	{
		REGISTER_FILTER_CLASS(RegexFilter)

	public:

		RegexFilter(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		void HandleMessages(MsgPtrArr msg) override;

	protected:
		std::string InstanceName() override
		{
			return "RegexFilter";
		}

	private:
		std::string pattern;
		bool inclusive = false;
	};
}