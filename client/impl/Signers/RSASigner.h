#ifndef RSA_SIGNER_H
#define RSA_SIGNER_H

#include "../../base/LogSigner.h"
#include "../../core/PersistentStorage.h"
#include "../../../common/DEFINITIONS.h"
#include "../../../common/SecureString/SecureString.h"
#include <rsa.h>
#include <osrng.h>
#include <pssr.h>
#include <sha.h>

namespace Impl
{
	class RsaSigner : public Base::LogSigner
	{
		REGISTER_SIGNER_CLASS(RsaSigner);

	public:
		RsaSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);
		~RsaSigner();

		void HandleMessages(MsgPtrArr msgs);

	protected:
		std::string InstanceName() override
		{
			return "RsaSigner";
		}

	private:
		void InitKey();
		void LoadKey();
		void GenerateKey();
		void SynchronizeKey();

		Caelus::Utilities::SecureString ToHex(const unsigned char* buf, int length);
		std::string base64_encode(const byte* src, uint32_t len);

		RSA_MAC_ALG signer;
		CryptoPP::RSA::PublicKey publicKey;
		CryptoPP::RSA::PrivateKey secretKey;
		int keylen = 2048;

		Core::PersistentStorageService* data;
		volatile int zero = 0;
		CryptoPP::AutoSeededRandomPool rng;
	};
}

#endif