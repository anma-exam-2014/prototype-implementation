#ifndef _WIN32
#include "TpmRsaSigner.h"
#include "../../../common/DEFINITIONS.h"

#ifdef _WIN32
#pragma warning(disable : 4996)
#endif
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#ifdef _WIN32
#include <base64.h>
#else
#include <cryptopp/base64.h>
#endif

using namespace Impl;

#ifdef _WIN32
#define __func__ __FUNCTION__
#endif

#define TSS_DBG(message,tResult) \
	do {\
		if(tResult != TSS_SUCCESS){\
			throw Base::BaseThread::ConstructionException(\
					std::string(__func__) \
					+ std::string(":") \
					+ boost::lexical_cast<std::string>(__LINE__) \
					+ std::string(":") \
					+ std::string(message) \
					+ std::string(" ") \
					+ Trspi_Error_String(tResult));\
		}\
	} while(0)

#define TSS_DBG_r(message,tResult) \
	do {\
		if(tResult != TSS_SUCCESS){\
			throw std::runtime_error(\
					std::string(__func__) \
					+ std::string(":") \
					+ boost::lexical_cast<std::string>(__LINE__) \
					+ std::string(":") \
					+ std::string(message) \
					+ Trspi_Error_String(tResult));\
		}\
	} while(0)

void print_hex(BYTE *buf, UINT32 len)
{
	UINT32 i = 0, j;

	while (i < len) {
		for (j=0; (j < 4) && (i < len); j++, i+=4)
			printf("%02x%02x%02x%02x ",
				buf[i] & 0xff, buf[i+1] & 0xff,
				buf[i+2] & 0xff, buf[i+3] & 0xff);
		printf("\n");
	}
}

TSS_UUID toTssUuid(boost::uuids::uuid& from)
{
	TSS_UUID to;
	memcpy(&to, &(from.data), from.size());
	return to;
}

TSS_UUID toTssUuid(std::string& from)
{
	boost::uuids::uuid id = boost::lexical_cast<boost::uuids::uuid>(from);
	return toTssUuid(id);
}

boost::uuids::uuid toBoostUuid(TSS_UUID& from)
{
	boost::uuids::uuid to;
	memcpy(&(to.data), &from, to.size());
	return to;
}
///
/// Convert up to len bytes of binary data in src to base64 and return the result
///
/// \param src Source binary data.
/// \param len The number of bytes of src to convert.
///
/// \return The resulting string.
///
std::string base64_encode(const BYTE* src, uint32_t len)
{
	std::string encoded;
	CryptoPP::StringSource ss(src, len, true,
	    new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded), false, 0));
	return encoded;
}


REGISTER_SIGNER_CLASS_IMPL(TpmRsaSigner);

TpmRsaSigner::TpmRsaSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
	: LogSigner(nextStep, options)
{
	storage = Core::PersistentStorageService::getInstance();

	initTpm();

	std::string signingKeyUuid = storage->GetSync("TpmRsaSigningKeyBlob");

	if(signingKeyUuid.empty())
	{
		signingKeyUuid = generateNewSigningKey();
		//save the identifier so that we may use it on the next boot.
		storage->SetSync("TpmRsaSigningKeyBlob", signingKeyUuid);
		//load the key into the Tpm
		loadSigningKey();
		//send a special init message to server so that the verifier get's all required information
		ForwardPublicInfo(nextStep);
		//batch zero is for init data sent by ForwardPublicInfo() above
		nextBatchId = 1;
		//store the value
		saveNextBatchNumber();
	}
	else
	{
		//print uuid
		SIGNING_KEY_UUID = toTssUuid(signingKeyUuid);
		//load the key into the Tpm
		loadSigningKey();
		//get batchid
		nextBatchId = boost::lexical_cast<int>( storage->GetSync("TpmRsaSigningNextBatchId") );
	}

}

TpmRsaSigner::~TpmRsaSigner()
{
	if(hContext != 0)
	{
		Tspi_Context_FreeMemory(hContext, NULL);
		Tspi_Context_Close(hContext);
	}
}

void TpmRsaSigner::initTpm()
{
	try{
		TSS_RESULT result;
		TSS_HPOLICY srkUsagePolicy;
		BYTE wks[20]; // Place to put the well known secret

		memset(wks, 0, 20); // Set wks to the well known secret of 20 bytes of all zeros

		// Create Context
		result = Tspi_Context_Create(&hContext);
		TSS_DBG("Tspi_Context_Create", result);

		// Connect to Context
		result = Tspi_Context_Connect(hContext, NULL);
		TSS_DBG("Tspi_Context_Connect", result);

		//load tpm object
		result = Tspi_Context_GetTpmObject(hContext, &hTPM);
		TSS_DBG("Tspi_Context_GetTpmObject (hash)", result);

		//Load Key By UUID
		result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM,
			TSS_UUID_SRK, &hSRK);
		TSS_DBG("Tspi_Context_LoadKeyByUUID (hSRK)", result);

		//Get Policy Object
		result = Tspi_GetPolicyObject(hSRK, TSS_POLICY_USAGE,
			&srkUsagePolicy);
		TSS_DBG("Tspi_GetPolicyObject", result);

		//Set Secret
		result = Tspi_Policy_SetSecret(srkUsagePolicy, TSS_SECRET_MODE_SHA1, 20, wks);
		TSS_DBG("Tspi_Policy_SetSecret", result);
	}
	catch(...)
	{
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
}

void TpmRsaSigner::loadSigningKey() {
	try{
		TSS_RESULT result;
		//load the key into the Tpm
		result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM, SIGNING_KEY_UUID, &hMSigningKey);
		TSS_DBG("Tspi_Context_LoadKeyByUUID (hMSigningKey)", result);
	}
	catch(...)
	{
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
}

//returns the uuid of the new key
std::string TpmRsaSigner::generateNewSigningKey()
{
	try{
		TSS_RESULT result;

		//Create Signing Key object
		result =
			Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_RSAKEY,
			TSS_KEY_SIZE_2048 |
			TSS_KEY_TYPE_SIGNING |
			TSS_KEY_NO_AUTHORIZATION,
			&hMSigningKey);
		TSS_DBG("Tspi_Context_CreateObject (signing key)", result);


		//Set attribute on key
		result = Tspi_SetAttribUint32(hMSigningKey, TSS_TSPATTRIB_KEY_INFO,
			TSS_TSPATTRIB_KEYINFO_SIGSCHEME,
			TSS_SS_RSASSAPKCS1V15_SHA1);
		TSS_DBG("Tspi_SetAttribUint32", result);

		//Create the key
		result = Tspi_Key_CreateKey(hMSigningKey, hSRK, 0);
		TSS_DBG("Tspi_Key_CreateKey (signing key)", result);

		auto keyUuid = boost::uuids::random_generator()();

		Tspi_Context_RegisterKey(hContext, hMSigningKey, TSS_PS_TYPE_SYSTEM, toTssUuid(keyUuid), TSS_PS_TYPE_SYSTEM, TSS_UUID_SRK);
		TSS_DBG("Tspi_Context_RegisterKey (signing key)", result);

		SIGNING_KEY_UUID = toTssUuid(keyUuid);

		return boost::lexical_cast<std::string>(keyUuid);
	}
	catch(...)
	{
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
}

void TpmRsaSigner::saveNextBatchNumber()
{
	storage->SetAsync("TpmRsaSigningNextBatchId", boost::lexical_cast<std::string>(nextBatchId));
}

void TpmRsaSigner::ForwardPublicInfo(BaseReceiver::Ptr nextstep)
{
	try{
		TSS_RESULT result;
		BYTE *exponent;
		UINT32 exponentLen;
		BYTE *modulus;
		UINT32 modulusLen;

		result = Tspi_GetAttribData(hMSigningKey, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_EXPONENT, &exponentLen, &exponent);
		TSS_DBG("Tspi_GetAttribData (signing key exponent)", result);
		result = Tspi_GetAttribData(hMSigningKey, TSS_TSPATTRIB_RSAKEY_INFO, TSS_TSPATTRIB_KEYINFO_RSA_MODULUS, &modulusLen, &modulus);
		TSS_DBG("Tspi_GetAttribData (signing key modulus)", result);

		Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FacilityNumber::FN_SYSLOG, Common::LogMessage::SeverityValue::Informational));
		msg->SetContent(TPMRSA_INIT_MESSAGE_CONTENT);

		Common::LogMessage::StructuredDataContent signature;
		signature[TPMRSA_VERSION] = TPMRSA_LATEST_VERSION;
		signature[TPMRSA_BATCH] = "0";
		signature[TPMRSA_SIGNKEY_EXPONENT] = base64_encode(exponent, exponentLen);
		signature[TPMRSA_SIGNKEY_MODULUS] = base64_encode(modulus, modulusLen);
		msg->GetStructuredData()[SIGN_METHOD_TPMRSA] = signature;

		boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
		uint64_t millis = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();

		msg->SetTimestamp(millis);

		//send to server
		nextstep->PutMessageOnQueue(msg);
	}
	catch(...)
	{
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
}

void TpmRsaSigner::doSign(std::string signMe, Common::LogMessage::StructuredDataContent &putSignatureHere)
{
	TSS_RESULT result;
	TSS_HHASH hHash;
    TSS_VALIDATION validationData;
    BYTE *nonce;

    //generate NONCE
	result = Tspi_TPM_GetRandom(hTPM, sizeof(TPM_NONCE), &nonce);
    TSS_DBG_r("Tspi_TPM_GetRandom (hash)", result);

    //put NONCE into data struct
    validationData.rgbExternalData = nonce;
    validationData.ulExternalDataLength = sizeof(TPM_NONCE);

	// create hash object
	result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_HASH, TSS_HASH_SHA1, &hHash);
	TSS_DBG_r("Tspi_Context_CreateObject (hash)", result);

	//hash the data
	result = Tspi_Hash_UpdateHashValue(hHash, signMe.length(), (BYTE*)signMe.c_str());
	TSS_DBG_r("Tspi_Hash_UpdateHashValue", result);

	//sign hash with timestamp
    result = Tspi_Hash_TickStampBlob(hHash, hMSigningKey, &validationData);
    TSS_DBG_r("Tspi_Hash_TickStampBlob", result);

	putSignatureHere[TPMRSA_SIG_INFO] = base64_encode(validationData.rgbData, validationData.ulDataLength);
	putSignatureHere[TPMRSA_SIGNATURE] = base64_encode(validationData.rgbValidationData, validationData.ulValidationDataLength);

	return;
}

void TpmRsaSigner::HandleMessages(MsgPtrArr msgs)
{
	try
	{

		int index = 0;
		std::stringstream batchToSign;

		BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
        {
            if (msg->DoNotSignThis())
            {
                continue;
            }

			Common::LogMessage::StructuredDataContent signature;
			signature[TPMRSA_BATCH] = boost::lexical_cast<std::string>(nextBatchId);
			signature[TPMRSA_INDEX] = boost::lexical_cast<std::string>(index);

			if (index == 0)
			{
				signature[TPMRSA_SIGNATURE] = TPMRSA_PLACEHOLDER;
				signature[TPMRSA_SIG_INFO] = TPMRSA_PLACEHOLDER;
				signature[TPMRSA_BATCH_SIZE] = boost::lexical_cast<std::string>(msgs.size());
			}

			msg->GetStructuredData()[SIGN_METHOD_TPMRSA] = signature;

			batchToSign << msg->GenerateFrame() ;

			index++;
		}

        if(index > 0)
        {
            //only the first message in a batch get the signature since
            //it's generated over the entire batch
            auto &putSignatureHere = msgs[0]->GetStructuredData()[SIGN_METHOD_TPMRSA];

            //SIGN!
            doSign(batchToSign.str(), putSignatureHere);

            //send all messages to the next stage (server)
            GetNextStep()->PutMessagesOnQueue(msgs);

            //make ready for next batch
            nextBatchId++;
            saveNextBatchNumber();
        }
        else
        {
            GetNextStep()->PutMessagesOnQueue(msgs);
        }
	}
	catch(std::runtime_error &e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
	catch(...)
	{
		if(hContext != 0)
		{
			Tspi_Context_FreeMemory(hContext, NULL);
			Tspi_Context_Close(hContext);
		}
		hContext = 0;
		throw;
	}
}

#endif
