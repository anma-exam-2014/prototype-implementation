#include "ItkisReyzinSigner.h"
#include <base64.h>
#include <boost/lexical_cast.hpp>

//#include <ctime>

using namespace Impl;

REGISTER_SIGNER_CLASS_IMPL(ItkisReyzinSigner)

#define S(x, y) x + boost::lexical_cast<std::string>(y)

ItkisReyzinSigner::ItkisReyzinSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) : LogSigner(nextStep, options)
{
	data = Core::PersistentStorageService::getInstance();
	int tmp;
	for (auto i = options.begin(); i != options.end(); i++)
	{
		tmp = ((tmp = strtol(options[i->first].data(), 0, 10)) > 0) ? tmp : throw BaseThread::ConstructionException("Invalid parameter: " + i->first);
		if (i->first == "k")
		{
			k = tmp;
		}
		else if (i->first == "l")
		{
			l = tmp;
		}
		else if (i->first == "T")
		{
			T = tmp;
		}
		else if (i->first == "key-evolve-time")
		{
			if (Mode == NONE)
			{
				Mode = UPDATE_MODE::TIME;
			}
			else
			{
				Mode = UPDATE_MODE::HYBRID;
			}
			MaxTime = tmp;
		}
		else if (i->first == "key-evolve-sign")
		{
			if (Mode == NONE)
			{
				Mode = UPDATE_MODE::SIGN;
			}
			else
			{
				Mode = UPDATE_MODE::HYBRID;
			}
			MaxSign = tmp;
		}
		else
		{
			std::cout << "Unknown parameter: '" << i->first << "'" << std::endl;
		}
	}
	
	if (Mode == NONE)
	{
		Mode = SIGN;
	}

	Epoch = boost::posix_time::ptime(boost::gregorian::date(1970, 1, 1));

	InitKey();
}

ItkisReyzinSigner::~ItkisReyzinSigner()
{

}

void ItkisReyzinSigner::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
    {
        if (msg->DoNotSignThis())
        {
            GetNextStep()->PutMessageOnQueue(msg);
            continue;
        }

		if (!isKeyValid())
		{
			std::cout << "Key is invalid, update required!" << std::endl;
			if ( !UpdateKey() )
			{
				GenerateKey();
				SynchronizeKey();
			}
		}

		//Insert placeholders for the signature and, if one exists, also insert the hash of the previous entry 
		Common::LogMessage::StructuredDataContent structuredData;
		structuredData[ITKISREYZIN_SIG_Z] = "";
		structuredData[ITKISREYZIN_SIG_SIGMA] = "";
		structuredData[ITKISREYZIN_SIG_E] = "";
		structuredData[ITKISREYZIN_SIG_J] = "";
		if (!previousEntryHash.empty())
		{
			structuredData[ITKISREYZIN_PREVIOUS_ENTRY_HASH] = previousEntryHash;
		}
		msg->GetStructuredData()[SIGN_METHOD_ITKISREYZIN] = structuredData;

		//Generate signature
		ItkisReyzinSigner::Signature sign = Sign(msg->GenerateFrame());

		//Insert signature values into the entry
		structuredData[ITKISREYZIN_SIG_Z] = sign.z;
		structuredData[ITKISREYZIN_SIG_SIGMA] = sign.sigma;
		structuredData[ITKISREYZIN_SIG_E] = sign.e;
		structuredData[ITKISREYZIN_SIG_J] = sign.j;

		msg->GetStructuredData()[SIGN_METHOD_ITKISREYZIN] = structuredData;
		GetNextStep()->PutMessageOnQueue(msg);
	}
}

void ItkisReyzinSigner::InitKey()
{
	if (isOldKeyPresentAndValid())
	{
		LoadKey();
	}
	else
	{
		GenerateKey();
		SynchronizeKey();
	}
}

void ItkisReyzinSigner::GenerateKey()
{
	std::cout << "Generating key (" << k << " bits)." << std::endl;

	clock_t begin = std::clock();

	CryptoPP::AutoSeededRandomPool RNG;

	//Step 1 - Generate 2 primes p1 and p2 (which is in the form 2*q + 1 where q also is a prime)
	CryptoPP::PrimeAndGenerator prime1(1, RNG, (k / 2) - 1);
	CryptoPP::PrimeAndGenerator prime2(1, RNG, (k / 2) - 1);
	
	//Step 2 - n = p1 * p2
	CryptoPP::Integer n = prime1.Prime() * prime2.Prime();

	//Step 3 - t1 = random integer from Z_n
	CryptoPP::Integer t1(RNG, n.BitCount()-1);

	//Step 4 & 5 - f2 = e2 * e3 * e4 * ... * eT mod phi(n) (phi(n) = 4 * q1 * q2)
	CryptoPP::Integer init(RNG, 32);
	CryptoPP::Integer e1;
	CryptoPP::Integer f2;
	CryptoPP::Integer phi = (4 * prime1.SubPrime() * prime2.SubPrime());

	GeneratePrimeChain(1, init, e1, f2, phi);
	//GeneratePrimeChainOptimized(1, init, e1, f2, n);

	//Step 6 - s = t1 ^ f2 mod n
	CryptoPP::Integer s1 = CryptoPP::a_exp_b_mod_c(t1, f2, n);

	//Step 7 - v = 1/(s1^e1) mod n
	CryptoPP::Integer v = CryptoPP::a_exp_b_mod_c(s1, e1, n).InverseMod(n);

	//Step 8 - t2 = t1^e1
	CryptoPP::Integer t2 = CryptoPP::a_exp_b_mod_c(t1, e1, n);

	// Init public key
	pub.n = n;
	pub.v = v;
	pub.T = T;

	// Init private key
	priv.j = 1;
	priv.s = s1;
	priv.t = t2;
	priv.e = e1;

	//Write key to persistent storage
	SaveKey();

	//Write current configuration parameters to persistent storage
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::SP_K), boost::lexical_cast<std::string>(k));
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::SP_L), boost::lexical_cast<std::string>(l));
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::SP_T), boost::lexical_cast<std::string>(T));
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::MODE), boost::lexical_cast<std::string>(Mode));
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::MAX_TIME_PER_KEY), boost::lexical_cast<std::string>(MaxTime));
	data->SetSync(S(ITKISREYZIN_CONFIG, CONFIG_PARAMETERS::MAX_SIGNATURES_PER_KEY), boost::lexical_cast<std::string>(MaxSign));

	clock_t end = std::clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

	std::cout << "Key sucessfully generated (" << elapsed_secs << " seconds)." << std::endl;
}

/*
	This function sets 'first' to prime e_j+1 and 'product' to prime e_j+2 * e_j+3 * ... * e_jT
*/
void ItkisReyzinSigner::GeneratePrimeChain(int j, CryptoPP::Integer prev, CryptoPP::Integer &first, CryptoPP::Integer &product, CryptoPP::Integer mod)
{
	CryptoPP::Integer lastPrime = prev;
	CryptoPP::Integer limit = CryptoPP::Integer::Power2(l);
	int tmp = (int)pow(10, (int)std::log10(T) + 1);
	product = 1;

	clock_t res = 0;
	for (int i = j; i <= T; i++)
	{
		unsigned char* bytes = new unsigned char[lastPrime.ByteCount()];
		for (size_t k = 0; k < lastPrime.ByteCount(); k++)
		{
			bytes[k] = lastPrime.GetByte(k);
		}

		unsigned char hash[ITKISREYZIN_HASH_LEN];
		ITKISREYZIN_HASH_ALG().CalculateDigest(hash, bytes, lastPrime.ByteCount());
		
		unsigned int seed = (hash[3] << 24) | (hash[2] << 16) | (hash[1] << 8) | (hash[0]);//TODO: Make use of entire hash

		CryptoPP::LC_RNG rng(seed);
		
		CryptoPP::Integer min = (limit * (long)((1 + ((double)(i - 1) / T)) * tmp)) / tmp;
		CryptoPP::Integer max = (limit * (long)((1 + ((double)i / T)) * tmp)) / tmp;
		CryptoPP::Integer prime = CryptoPP::Integer(rng, min, max - 1, CryptoPP::Integer::RandomNumberType::PRIME);

		if (i == j)
		{
			first = prime;
		}
		else
		{
			product = CryptoPP::a_times_b_mod_c(product, prime, mod);
		}

		memset(bytes, zero, lastPrime.ByteCount());
		memset(hash, zero, ITKISREYZIN_HASH_LEN);
		delete[] bytes;
		seed = zero;

		lastPrime = prime;
	}
}

//void PublicKeySigner::GeneratePrimeChainOptimized(int j, CryptoPP::Integer prev, CryptoPP::Integer &first, CryptoPP::Integer &product, CryptoPP::Integer mod)
//{
//	clock_t begin = std::clock();
//
//	CryptoPP::Integer lastPrime = prev;
//	product = 1;
//
//	//CryptoPP::Integer S = (4 * std::log2(T)) + 1;
//	CryptoPP::Integer S = 72;
//	clock_t res = 0;
//	for (int i = j; i <= T; i++)
//	{
//		unsigned char* bytes = new unsigned char[lastPrime.ByteCount()];
//		for (size_t k = 0; k < lastPrime.ByteCount(); k++)
//		{
//			bytes[k] = lastPrime.GetByte(k);
//		}
//
//		unsigned char hash[PUBLICKEY_HASH_LEN];
//		PUBLICKEY_HASH_ALG().CalculateDigest(hash, bytes, lastPrime.ByteCount());
//
//		unsigned int seed = (hash[3] << 24) | (hash[2] << 16) | (hash[1] << 8) | (hash[0]);//TODO: Make use of entire hash
//
//		CryptoPP::LC_RNG rng(seed);
//
//		CryptoPP::Integer min = (i - 1) * S;
//		CryptoPP::Integer max = (i * S);
//		clock_t b1 = std::clock();
//		CryptoPP::Integer prime = CryptoPP::Integer(rng, min, max, CryptoPP::Integer::RandomNumberType::PRIME);
//		clock_t s1 = std::clock();
//		res += (s1 - b1);
//		
//		if (i == j)
//		{
//			first = prime;
//		}
//		else
//		{
//			product = CryptoPP::a_times_b_mod_c(product, prime, mod);
//		}
//
//		memset(bytes, zero, lastPrime.ByteCount());
//		memset(hash, zero, PUBLICKEY_HASH_LEN);
//		delete[] bytes;
//		seed = zero;
//
//		lastPrime = prime;
//	}
//
//	clock_t end = std::clock();
//	std::cout << "Optimized: " << double(end - begin) / CLOCKS_PER_SEC << " (" << res << ")" << std::endl;
//}

bool ItkisReyzinSigner::UpdateKey()
{
	double elapsed_secs;
	//Step 1
	if (priv.j == pub.T)
	{
		return false;
	}

	clock_t begin = std::clock();

	PrivateKey newPriv;
	newPriv.j = priv.j + 1;

	//Step 2 - Regenerate e's
	CryptoPP::Integer ej;
	CryptoPP::Integer f;
	GeneratePrimeChain(newPriv.j, priv.e, ej, f, pub.n);

	//Step 3 - sj+1 = tj+1 ^ (ej+2 * ej+3 * ... * eT) mod n
	newPriv.s = CryptoPP::a_exp_b_mod_c(priv.t, f, pub.n);

	//Step 4 - tj+2 = tj+1 ^ ej+1 mod n
	newPriv.t = CryptoPP::a_exp_b_mod_c(priv.t, ej, pub.n);
	newPriv.e = ej;

	//TODO: Zero sensitive data

	priv = newPriv;

	//Write key to persistent storage
	SaveKey();

	clock_t end = std::clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	std::cout << "UpdateKey: " << elapsed_secs << " seconds." << std::endl;

	return true;
}

ItkisReyzinSigner::Signature ItkisReyzinSigner::Sign(std::string M)
{
	CryptoPP::AutoSeededRandomPool RNG;
	CryptoPP::Integer r(RNG, pub.n.BitCount()-1);
	CryptoPP::Integer y = CryptoPP::a_exp_b_mod_c(r, priv.e, pub.n);

	unsigned char finalHash[ITKISREYZIN_HASH_LEN];

	CryptoPP::Integer tmp[3] = {priv.j, priv.e, y};
	auto Hash = ITKISREYZIN_HASH_ALG();
	for (int i = 0; i < 3; i++)
	{
		size_t len = tmp[i].ByteCount();
		unsigned char* bytes = new unsigned char[len];
		for (size_t j = 0; j < len; j++)
		{
			bytes[j] = tmp[i].GetByte(j);
		}
		Hash.Update(bytes, len);
		memset(bytes, zero, len);
		delete[] bytes;
	}
	//Hash.Update(messageHash, PUBLICKEY_HASH_LEN);
	Hash.Update((byte*)M.data(), M.length());
	Hash.Final(finalHash);

	CryptoPP::Integer sigma(finalHash, ITKISREYZIN_HASH_LEN);
	CryptoPP::Integer z = CryptoPP::a_times_b_mod_c(r, a_exp_b_mod_c(priv.s, sigma, pub.n), pub.n);

	//
	ItkisReyzinSigner::Signature signature;
	std::stringstream ss;

	ss << z;
	signature.z = ss.str(); ss.str(std::string()); ss.clear();

	ss << sigma;
	signature.sigma = ss.str(); ss.str(std::string()); ss.clear();

	ss << priv.e;
	signature.e = ss.str(); ss.str(std::string()); ss.clear();

	ss << priv.j;
	signature.j = ss.str(); ss.str(std::string()); ss.clear();

	//
	unsigned char messageHash[ITKISREYZIN_HASH_LEN];
	ITKISREYZIN_HASH_ALG().CalculateDigest(messageHash, (byte*)M.data(), M.length());
	previousEntryHash = base64_encode(messageHash, ITKISREYZIN_HASH_LEN);
	data->SetAsync(ITKISREYZIN_PREVIOUS_ENTRY_HASH, previousEntryHash);
	
	//
	SignaturesWithKey++;
	data->SetAsync(ITKISREYZIN_SIGN_LAST_UPDATE, boost::lexical_cast<std::string>(SignaturesWithKey));

	return signature;
}

void ItkisReyzinSigner::SynchronizeKey()
{
	Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FacilityNumber::FN_SYSLOG, Common::LogMessage::SeverityValue::Notice));
	msg->SetContent(ITKISREYZIN_INIT_MESSAGE_CONTENT);

	Common::LogMessage::StructuredDataContent structuredData;

	std::stringstream ss;
	ss << pub.n;
	structuredData[ITKISREYZIN_PUB_N] = ss.str();
	ss.str(std::string()); ss.clear();

	ss << pub.v;
	structuredData[ITKISREYZIN_PUB_V] = ss.str();
	ss.str(std::string()); ss.clear();

	ss << T;
	structuredData[ITKISREYZIN_PUB_T] = ss.str();
	ss.str(std::string()); ss.clear();

	ss << l;
	structuredData[ITKISREYZIN_PUB_L] = ss.str();
	ss.str(std::string()); ss.clear();

	if (!previousEntryHash.empty())
	{
		structuredData[ITKISREYZIN_PREVIOUS_ENTRY_HASH] = previousEntryHash;
	}

	msg->GetStructuredData()[SIGN_METHOD_ITKISREYZIN] = structuredData;
	msg->SetTimestamp(MillisecondsSinceEpoch());

	unsigned char messageHash[ITKISREYZIN_HASH_LEN];
	ITKISREYZIN_HASH_ALG().CalculateDigest(messageHash, (byte*)msg->GenerateFrame().data(), msg->GenerateFrame().length());
	previousEntryHash = base64_encode(messageHash, ITKISREYZIN_HASH_LEN);

	data->SetAsync(ITKISREYZIN_PREVIOUS_ENTRY_HASH, previousEntryHash);

	GetNextStep()->PutMessageOnQueue(msg);
}

void ItkisReyzinSigner::LoadKey()
{
	LastUpdate = strtoll(data->GetSync(ITKISREYZIN_TIME_LAST_UPDATE).data(), 0, 10);
	SignaturesWithKey = strtol(data->GetSync(ITKISREYZIN_SIGN_LAST_UPDATE).data(), 0, 10);
	previousEntryHash = data->GetSync(ITKISREYZIN_PREVIOUS_ENTRY_HASH);

	CryptoPP::Integer vals[6];
	for (int i = 0; i < 6; i++)
	{
		unsigned char* tmp;
		int len = data->GetSync(S(ITKISREYZIN_KEY, i), tmp);
		vals[i] = CryptoPP::Integer(tmp, len);
		memset(tmp, zero, len);
		delete[] tmp;
	}


	PrivateKey sk;
	sk.j = vals[0].IsConvertableToLong() ? vals[0].ConvertToLong() : throw ConstructionException("Unable to load key.");
	sk.s = vals[1];
	sk.t = vals[2];
	sk.e = vals[3];

	PublicKey pk;
	pk.n = vals[4];
	pk.v = vals[5];
	pk.T = T;

	priv = sk;
	pub = pk;
}

void ItkisReyzinSigner::SaveKey()
{
	LastUpdate = MillisecondsSinceEpoch();
	SignaturesWithKey = 0;


	data->SetSync(ITKISREYZIN_TIME_LAST_UPDATE, boost::lexical_cast<std::string>(LastUpdate));
	data->SetSync(ITKISREYZIN_SIGN_LAST_UPDATE, boost::lexical_cast<std::string>(SignaturesWithKey));

	CryptoPP::Integer tmp[6] = { priv.j, priv.s, priv.t, priv.e, pub.n, pub.v };
	for (int i = 0; i < 6; i++)
	{
		size_t size = tmp[i].ByteCount();
		unsigned char* bytes = new unsigned char[size];
		for (size_t j = 0; j < size; j++)
		{
			bytes[j] = tmp[i].GetByte((size-1)-j);
		}
		data->SetSync(S(ITKISREYZIN_KEY, i), bytes, size);
		memset(bytes, zero, size);
		delete[] bytes;
	}

	data->SetSync(ITKISREYZIN_KEY_EXIST, "yes sir!");
}

bool ItkisReyzinSigner::isKeyValid()
{
	switch (Mode)
	{
		case TIME:
		{
			uint64_t currentTime = MillisecondsSinceEpoch();
			if (double(currentTime - LastUpdate) / CLOCKS_PER_SEC >= MaxTime)
			{
				return false;
			}
			break;
		}
		case SIGN:
		{
			if (SignaturesWithKey >= MaxSign)
			{
				return false;
			}
			break;
		}
		case HYBRID:
		{
			uint64_t currentTime = MillisecondsSinceEpoch();
			if (SignaturesWithKey >= MaxSign || double(currentTime - LastUpdate) / CLOCKS_PER_SEC >= MaxTime)
			{
				return false;
			}
			break;
		}
		default:
		{
			throw std::runtime_error("PublicKeySigner: Unrecognized mode");
			break;
		}
	}
	return true;
}

bool ItkisReyzinSigner::isOldKeyPresentAndValid()
{
	int vals[6] = {k, l, T, Mode, MaxTime, MaxSign};
	for (int i = 0; i < 6; i++)
	{
		std::string conf = data->GetSync(S(ITKISREYZIN_CONFIG, i));
		if (!conf.empty() && vals[i] != strtol(conf.data(), 0, 10))
		{
			std::cout << "Configuration has changed." << std::endl;
			return false;
		}
	}

	if (data->GetSync(ITKISREYZIN_KEY_EXIST).empty())
	{
		std::cout << "Initial setup required." << std::endl;
		return false;
	}

	return true;
}

std::string ItkisReyzinSigner::base64_encode(const byte* src, uint32_t len)
{
	std::string encoded;
	CryptoPP::StringSource ss(src, len, true,
		new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded), false, 0));
	return encoded;
}

uint64_t ItkisReyzinSigner::MillisecondsSinceEpoch()
{
	return (boost::posix_time::microsec_clock::universal_time() - Epoch).total_milliseconds();
}

bool ItkisReyzinSigner::BenchmarkUpdateKey()
{
	UpdateKey();
	return true;
}
