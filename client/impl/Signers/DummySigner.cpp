#include "DummySigner.h"

using namespace Impl;

REGISTER_SIGNER_CLASS_IMPL(DummySigner)

DummySigner::DummySigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
	: LogSigner(nextStep, options)
{

}

void DummySigner::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
        if (msg->DoNotSignThis())
        {
            GetNextStep()->PutMessageOnQueue(msg);
            continue;
        }

        Common::LogMessage::StructuredDataContent signature;
        signature["HASH"] = "9ASDJKLK213098SD901A";

        msg->GetStructuredData()["SIGNDUMMY"] = signature;

        //do this to simulate work done by other singing methods
        std::string frame = msg->GenerateFrame();

		GetNextStep()->PutMessageOnQueue(msg);
	}
}