#ifndef TPMRSASIGNER_H
#define TPMRSASIGNER_H

#include "../../base/LogSigner.h"
#include "../../core/PersistentStorage.h"

#include <trousers/tss.h>
#include <trousers/trousers.h>

namespace Impl
{
	class TpmRsaSigner : public Base::LogSigner
	{
		REGISTER_SIGNER_CLASS(TpmRsaSigner);

	public:
		TpmRsaSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		virtual ~TpmRsaSigner();

		void HandleMessages(MsgPtrArr msg);

	protected:
		std::string InstanceName() override
		{
			return "TpmRsaSigner";
		}

	private:

		void initTpm();

		std::string generateNewSigningKey();
		void loadSigningKey();
		void ForwardPublicInfo(BaseReceiver::Ptr nextstep);
		void saveNextBatchNumber();
		void doSign(std::string signMe, Common::LogMessage::StructuredDataContent &putSignatureHere);

		int nextBatchId;
		Core::PersistentStorageService* storage;

		TSS_HCONTEXT hContext;
		TSS_HTPM hTPM;
		TSS_HKEY hSRK;
		TSS_HKEY hMSigningKey;
		TSS_UUID SIGNING_KEY_UUID;
	};
}

#endif
