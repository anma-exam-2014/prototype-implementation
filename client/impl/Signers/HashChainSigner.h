#ifndef HASH_CHAIN_SIGNER_H
#define HASH_CHAIN_SIGNER_H

#include "../../base/LogSigner.h"
#include "../../core/PersistentStorage.h"
#include "../../../common/DEFINITIONS.h"
#include "../../../common/SecureString/SecureString.h"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

namespace Impl
{
	class HashChainSigner : public Base::LogSigner
	{
		REGISTER_SIGNER_CLASS(HashChainSigner);

	public:
		HashChainSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);
		~HashChainSigner();

		void HandleMessages(MsgPtrArr msg);

	protected:
		std::string InstanceName() override
		{
			return "HashChainSigner";
		}

	private:
		//Key management methods
		void InitKey(unsigned char* &ptr);
		void LoadKey(unsigned char* &ptr);
		void GenerateKey(unsigned char* &ptr);
		void UpdateKey(unsigned char* &ptr);
		void SynchronizeKey(unsigned char* &ptr);
		//std::string* SetupParameters();

		//Utility methods
		Caelus::Utilities::SecureString ToHex(const unsigned char* buf, int length);
		//bool verify_certificate(bool preverified, boost::asio::ssl::verify_context& ctx);

		//Benchmarking methods
		bool BenchmarkUpdateKey();

		//Member variables
		unsigned char* key;
		std::string previousEntry;
		Core::PersistentStorageService* data;
		std::string null;
		volatile int zero = 0;
	};
}

#endif
