#include "HashChainSigner.h"
#include "../../base/BaseThread.h"

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#ifdef _WIN32
#include <filters.h>
#include <sha.h>
#include <hmac.h>
#include <osrng.h>
#include <hex.h>
#else
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hmac.h>
#include <cryptopp/osrng.h>
#include <hex.h>
#endif

using namespace Impl;
using namespace boost::asio;
using namespace Caelus::Utilities;

#ifdef _WIN32
#pragma warning(disable : 4244)
#endif

REGISTER_SIGNER_CLASS_IMPL(HashChainSigner)

HashChainSigner::HashChainSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) : LogSigner(nextStep, options)
{
	key = NULL;
	data = Core::PersistentStorageService::getInstance();

	for (int i = 0; i < HASHCHAIN_HASH_LEN * 2; i++)
	{
		null += "0";
	}

	InitKey(key);
}

HashChainSigner::~HashChainSigner()
{
	memset(key, zero, sizeof(key));
	delete[] key;
}

std::string hex_encode(const byte* src, uint32_t len)
{
	std::string encoded;
	CryptoPP::StringSource ss(src, len, true,
		new CryptoPP::HexEncoder(new CryptoPP::StringSink(encoded), false, 0, "", ""));
	return encoded;
}

void HashChainSigner::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
        if (msg->DoNotSignThis())
        {
            GetNextStep()->PutMessageOnQueue(msg);
            continue;
        }
		Common::LogMessage::StructuredDataContent signature;
		if (msg->GetStructuredData().count(SIGN_METHOD_HASHCHAIN))
		{
			signature = msg->GetStructuredData()[SIGN_METHOD_HASHCHAIN];
		}

		SecureString mac(HASHCHAIN_HASH_LEN);
		unsigned char hash_buf[HASHCHAIN_HASH_LEN], mac_buf[HASHCHAIN_HASH_LEN];

		signature[HASHCHAIN_SIGNATURE] = "";
		if (!previousEntry.empty())
		{
			signature[HASHCHAIN_PREVIOUS_ENTRY] = previousEntry;
		}
		msg->GetStructuredData()[SIGN_METHOD_HASHCHAIN] = signature;

		//Hash log entry
		//HASHCHAIN_HASH_ALG().CalculateDigest((byte*)hash_buf, (const byte*)msg->GenerateFrame().data(), msg->GenerateFrame().length());
		HASHCHAIN_HASH_ALG hash;
		std::string nosignatureMsg = msg->GenerateFrame();
		hash.Update((const byte*)nosignatureMsg.data(), nosignatureMsg.length());
		hash.Final((byte*)hash_buf);
		
		//Sign
		HASHCHAIN_MAC_ALG hmac((byte*)key, HASHCHAIN_HASH_LEN);
		CryptoPP::ArraySource(hash_buf, HASHCHAIN_HASH_LEN, true, new CryptoPP::HashFilter(hmac, new CryptoPP::ArraySink(mac_buf, sizeof(mac_buf))));

		mac = ToHex(mac_buf, sizeof(mac_buf));
		signature[HASHCHAIN_SIGNATURE] = mac.getUnsecureString();
		mac.UnsecuredStringFinished();

		msg->GetStructuredData()[SIGN_METHOD_HASHCHAIN] = signature;
		GetNextStep()->PutMessageOnQueue(msg);

		hash.Restart();
		std::string updatedMsg = msg->GenerateFrame();
		hash.Update((const byte*)updatedMsg.data(), updatedMsg.length());
		hash.Final((byte*)hash_buf);
		previousEntry = hex_encode(hash_buf, HASHCHAIN_HASH_LEN);
		data->SetAsync(HASHCHAIN_PREVIOUS_ENTRY_PS, previousEntry);

		//Evolve the key
		UpdateKey(key);
	}
}

SecureString HashChainSigner::ToHex(const unsigned char* buf, int length)
{
	char* finalhash = new char[length * 2 + 1];
	char hexval[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	for (int j = 0; j < length; j++)
	{
		finalhash[j * 2] = hexval[((buf[j] >> 4) & 0xF)];
		finalhash[(j * 2) + 1] = hexval[(buf[j]) & 0x0F];
	}
	finalhash[length * 2] = '\0';

	SecureString tmp(finalhash, 0, true);
	return tmp;
}

void HashChainSigner::InitKey(unsigned char* &ptr)
{
	if (data->GetSync(HASHCHAIN_CURRENT_KEY_PS).length() != 0)
	{
		LoadKey(key);
	}
	else
	{
		GenerateKey(key);
		SynchronizeKey(key);
	}
}

void HashChainSigner::LoadKey(unsigned char* &ptr)
{
	// Allocate new space with the appropriate length
	ptr = new unsigned char[HASHCHAIN_HASH_LEN];

	//Load the current key from persistent storage
	unsigned char* tmp = NULL;
	int len = data->GetSync(HASHCHAIN_CURRENT_KEY_PS, tmp);
	SecureString key(reinterpret_cast<char*>(tmp), len, false, true);
	memset(tmp, zero, len);

	if (key.length() == HASHCHAIN_HASH_LEN)
	{
		for (unsigned int i = 0; i < key.length(); i++)
		{
			ptr[i] = key.at(i);
		}
		data->SetSync(HASHCHAIN_CURRENT_KEY_PS, null);
	}
	else
	{
		throw ConstructionException("Hash chain key length mismatch.");
	}

	previousEntry = data->GetSync(HASHCHAIN_PREVIOUS_ENTRY_PS);
}

void HashChainSigner::GenerateKey(unsigned char* &ptr)
{
	// Allocate new space with the appropriate length
	ptr = new unsigned char[HASHCHAIN_HASH_LEN];

	// Randomize a new key
	CryptoPP::AutoSeededRandomPool RNG;
	for (int i = 0; i < HASHCHAIN_HASH_LEN; i++)
	{
		ptr[i] = RNG.GenerateByte();
	}
}

void HashChainSigner::UpdateKey(unsigned char* &ptr)
{
	// Allocate space for the new key
	unsigned char* tmp = new unsigned char[HASHCHAIN_HASH_LEN];

	// The new key is created by hashin the old one
	HASHCHAIN_HASH_ALG().CalculateDigest(tmp, ptr, HASHCHAIN_HASH_LEN);

	// Remove the old key
	memset(ptr, zero, HASHCHAIN_HASH_LEN);
	delete[] ptr;

	// Assign the new key to the provided pointer
	ptr = tmp;
	data->SetAsync(HASHCHAIN_CURRENT_KEY_PS, ptr, HASHCHAIN_HASH_LEN);//TODO: FIX!!!
}

void HashChainSigner::SynchronizeKey(unsigned char* &ptr)
{
	previousEntry = "";

    Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FN_SYSLOG, Common::LogMessage::Informational));

	//msg->SetMessageType(MSG_TYPE_KEY_INITIALIZATION);

	msg->SetContent(HASHCHAIN_INIT_MESSAGE_CONTENT);

    Common::LogMessage::StructuredDataContent structuredData;

	//structuredData[MSG_TYPE_KEY_INITIALIZATION] = "true";
	structuredData[HASHCHAIN_INITIAL_KEY] = hex_encode(ptr, HASHCHAIN_HASH_LEN);

	msg->GetStructuredData()[SIGN_METHOD_HASHCHAIN] = structuredData;

	boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
	uint64_t millis = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
	msg->SetTimestamp(millis);

	GetNextStep()->PutMessageOnQueue(msg);
}

bool HashChainSigner::BenchmarkUpdateKey()
{
	UpdateKey(key);
	return true;
}
