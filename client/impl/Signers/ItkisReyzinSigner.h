#ifndef ITKIS_REYZIN_SIGNER_H
#define ITKIS_REYZIN_SIGNER_H

#include "../../base/LogSigner.h"
#include "../../core/PersistentStorage.h"
#include "../../../common/DEFINITIONS.h"
#include "../../../common/SecureString/SecureString.h"

#include <boost/date_time/posix_time/posix_time.hpp>

#ifdef _WIN32
#include <nbtheory.h>
#include <osrng.h>
#else
#include <cryptopp/nbtheory.h>
#include <cryptopp/osrng.h>

//this is a workaround for a bug in gcc
NAMESPACE_BEGIN(CryptoPP)
Integer a_times_b_mod_c(const Integer &x, const Integer& y, const Integer& m);
Integer a_exp_b_mod_c(const Integer &x, const Integer& e, const Integer& m);
NAMESPACE_END
#endif
namespace Impl
{
	class ItkisReyzinSigner : public Base::LogSigner
	{
		REGISTER_SIGNER_CLASS(ItkisReyzinSigner);

	public:
		ItkisReyzinSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);
		~ItkisReyzinSigner();

		void HandleMessages(MsgPtrArr msgs);

	protected:
		std::string InstanceName() override
		{
			return "PublicKeySigner";
		}

	private:
		struct PrivateKey
		{
			int j;
			CryptoPP::Integer s;
			CryptoPP::Integer t;
			CryptoPP::Integer e;
		};

		struct PublicKey
		{
			CryptoPP::Integer n;
			CryptoPP::Integer v;
			int T;
		};

		struct Signature
		{
			std::string z;
			std::string sigma;
			std::string e;
			std::string j;
		};

		enum UPDATE_MODE
		{
			NONE = -1,
			TIME,
			SIGN,
			HYBRID
		};

		enum CONFIG_PARAMETERS
		{
			SP_K = 0,
			SP_L,
			SP_T,
			MODE,
			MAX_TIME_PER_KEY,
			MAX_SIGNATURES_PER_KEY
		};

		//Security parameters
		int k = 2048;
		int l = 128;
		int T = 1000;

		//Keys
		PublicKey pub;
		PrivateKey priv;

		//
		std::string previousEntryHash;

		//Key management methods
		void InitKey();
		void GenerateKey();
		bool UpdateKey();
		void SynchronizeKey();
		Signature Sign(std::string);
		void GeneratePrimeChain(int j, CryptoPP::Integer prev, CryptoPP::Integer &first, CryptoPP::Integer &product, CryptoPP::Integer mod);
		//void GeneratePrimeChainOptimized(int j, CryptoPP::Integer prev, CryptoPP::Integer &first, CryptoPP::Integer &product, CryptoPP::Integer mod);

		bool BenchmarkUpdateKey();

		void SaveKey();
		void LoadKey();

		bool isKeyValid();
		bool isOldKeyPresentAndValid();
		std::string base64_encode(const byte* src, uint32_t len);
		uint64_t MillisecondsSinceEpoch();

		//Setup stuff
		UPDATE_MODE Mode = NONE;
		int MaxTime = 3600 * 6;
		int MaxSign = 1000;

		int SignaturesWithKey = 0;
		uint64_t LastUpdate;

		//Other
		Core::PersistentStorageService* data;
		volatile int zero = 0;
		boost::posix_time::ptime Epoch;
	};
}

#endif
