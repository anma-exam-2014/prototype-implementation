#include "RsaSigner.h"
#include "../../base/BaseThread.h"
#include <base64.h>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <filters.h>

using namespace Impl;
using namespace Caelus::Utilities;

REGISTER_SIGNER_CLASS_IMPL(RsaSigner)

RsaSigner::RsaSigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options) :LogSigner(nextStep, options)
{
	data = Core::PersistentStorageService::getInstance();
	if (options.count("key-length") > 0)
	{
		keylen = boost::lexical_cast<int>(options["key-length"]);
	}
	InitKey();
}

RsaSigner::~RsaSigner()
{

}

void RsaSigner::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
    {
        if (msg->DoNotSignThis())
        {
            GetNextStep()->PutMessageOnQueue(msg);
            continue;
        }

		Common::LogMessage::StructuredDataContent signature;

		signature[RSA_SIGNATURE] = "";
		msg->GetStructuredData()[SIGN_METHOD_RSA] = signature;

		//Hash log entry
		std::string frame = msg->GenerateFrame();

		//Sign
		std::string sig;
		CryptoPP::StringSource(frame, true, new CryptoPP::SignerFilter(rng, signer, new CryptoPP::StringSink(sig)));

		signature[RSA_SIGNATURE] = base64_encode((byte*)sig.data(), sig.length());

		msg->GetStructuredData()[SIGN_METHOD_RSA] = signature;
		GetNextStep()->PutMessageOnQueue(msg);
	}
}

void RsaSigner::InitKey()
{
	if (!data->GetSync(RSA_KEY).empty())
	{
		LoadKey();
	}
	else
	{
		GenerateKey();
		SynchronizeKey();
	}

	signer = RSA_MAC_ALG(secretKey);
}

void RsaSigner::LoadKey()
{
	std::string key = data->GetSync(RSA_KEY);
	std::string s[3];
	int pos = -1;
	
	for (int i = 0; i < 3; i++)
	{
		int tmp = pos + 1;
		pos = key.find(".", tmp);
		s[i] = key.substr(tmp, pos - tmp);
	}

	CryptoPP::Integer n(s[0].data());
	CryptoPP::Integer d(s[1].data());
	CryptoPP::Integer e(s[2].data());
	
	secretKey = CryptoPP::RSA::PrivateKey();
	secretKey.Initialize(n, e, d);
	publicKey = CryptoPP::RSA::PublicKey();
	publicKey.Initialize(n, e);
}

void RsaSigner::GenerateKey()
{
	std::cout << "Generating key (" << keylen << " bits)." << std::endl;
	clock_t begin = std::clock();

	CryptoPP::InvertibleRSAFunction params;
	params.GenerateRandomWithKeySize(rng, keylen);
	this->secretKey = CryptoPP::RSA::PrivateKey(params);
	this->publicKey = CryptoPP::RSA::PublicKey(params);

	std::stringstream ss;
	ss << secretKey.GetModulus();
	ss << secretKey.GetPrivateExponent();
	ss << secretKey.GetPublicExponent();

	data->SetSync(RSA_KEY, ss.str());

	clock_t end = std::clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	std::cout << "Key sucessfully generated (" << elapsed_secs << " seconds)." << std::endl;
}

void RsaSigner::SynchronizeKey()
{
	Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FacilityNumber::FN_SYSLOG, Common::LogMessage::SeverityValue::Notice));
	msg->SetContent(RSA_INIT_MESSAGE_CONTENT);

	Common::LogMessage::StructuredDataContent structuredData;

	std::stringstream ss;
	ss << publicKey.GetModulus();
	structuredData[RSA_PUB_N] = ss.str(); ss.str(std::string()); ss.clear();
	ss << publicKey.GetPublicExponent();
	structuredData[RSA_PUB_E] = ss.str(); ss.str(std::string()); ss.clear();
	msg->GetStructuredData()[SIGN_METHOD_RSA] = structuredData;

	boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
	uint64_t millis = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();

	msg->SetTimestamp(millis);

	//send to server
	GetNextStep()->PutMessageOnQueue(msg);
}

SecureString RsaSigner::ToHex(const unsigned char* buf, int length)
{
	char* finalhash = new char[length * 2 + 1];
	char hexval[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	for (int j = 0; j < length; j++)
	{
		finalhash[j * 2] = hexval[((buf[j] >> 4) & 0xF)];
		finalhash[(j * 2) + 1] = hexval[(buf[j]) & 0x0F];
	}
	finalhash[length * 2] = '\0';

	SecureString tmp(finalhash, 0, true);
	return tmp;
}

std::string RsaSigner::base64_encode(const byte* src, uint32_t len)
{
	std::string encoded;
	CryptoPP::StringSource ss(src, len, true,
		new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded), false, 0));
	return encoded;
}