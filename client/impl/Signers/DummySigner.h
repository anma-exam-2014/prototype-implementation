#ifndef DUMMYSIGNER_H
#define DUMMYSIGNER_H

#include "../../base/LogSigner.h"

namespace Impl
{
	class DummySigner : public Base::LogSigner
	{
		REGISTER_SIGNER_CLASS(DummySigner);

	public:
		DummySigner(BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		void HandleMessages(MsgPtrArr msg);

	protected:
		std::string InstanceName() override
		{
			return "DummySigner";
		}
	};
}

#endif