#include "DummySource.h"
#include "../../base/LogAggregator.h"
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>
#include <thread>

using namespace Impl;
using namespace Core;
using namespace Common;

REGISTER_SOURCE_CLASS_IMPL(DummySource)

DummySource::DummySource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: LogSource(nextStep, options)
{
	running = false;
	try
	{
		for (auto i = options.begin(); i != options.end(); i++)
		{
			if (i->first == "generate-interval")
			{
				interval = std::stoi(i->second);
			}
		}
	}
	catch (...)
	{
		std::cout << "ERROR: An error occured while parsing source options." << std::endl;
	}
}

void DummySource::Main()
{
	running = true;
	long i = 0;
	while (running)
	{
		LogMessage::Ptr msg(new LogMessage(LogMessage::FN_SYSLOG, LogMessage::Debug));
		msg->SetContent("The amount of data sent is " + boost::lexical_cast<std::string>(i++));

		GetNextStep()->PutMessageOnQueue(msg);
		if (interval)
		{
			boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));
		}
	}
}

void DummySource::Stop()
{
	running = false;
}
