#ifdef _WIN32
#ifndef WINDOWS_SOURCE_H
#define WINDOWS_SOURCE_H

#include "../../base/LogSource.h"
#include <Windows.h>
#include <winevt.h>
#include <atlstr.h>

namespace Impl
{
	class WindowsSource : public Base::LogSource
	{
		REGISTER_SOURCE_CLASS(WindowsSource)

	public:
		WindowsSource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);
		~WindowsSource();

	protected:

		void Main() override;

		void Stop() override;

	private:
		bool running = false;
		int timeout = 1000;

		HANDLE WaitHandle;
		EVT_HANDLE hSubscription;
		std::wstring Channel;
		std::wstring Query;
		_EVT_SUBSCRIBE_FLAGS mode = EvtSubscribeStartAtOldestRecord;

		Common::LogMessage::Ptr CreateLogMessage(std::string xml);
		DWORD ParseEvents(EVT_HANDLE hResults);
		DWORD ParseEvent(EVT_HANDLE hEvent);
		std::string UTF8_Encode(const std::wstring &wstr);
		//std::wstring UTF8_Decode(const std::string &str);
	};
}

#endif
#endif
