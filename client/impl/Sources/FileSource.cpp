#include "FileSource.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <errno.h>


#ifdef _WIN32
#include <tchar.h>
#else
#include <sys/types.h>
#include <sys/inotify.h>

#define MAX_EVENTS	1024										/*Max. number of events to process at one go*/
#define LEN_NAME	16											/*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  ( sizeof (struct inotify_event) ) 			/*size of one event*/
#define BUF_LEN     ( MAX_EVENTS * ( EVENT_SIZE + LEN_NAME )) 	/*buffer to store the data of events*/

#endif

using namespace Impl;
using namespace Core;
using namespace Common;
using namespace Base;

REGISTER_SOURCE_CLASS_IMPL(FileSource)

FileSource::FileSource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options) : LogSource(nextStep, options)
{

	for (auto i = options.begin(); i != options.end(); i++)
	{
		if (i->first == "filepath")
		{
			filepath = boost::filesystem::path(i->second);
			if(filepath.generic_string() == "")
			{
				throw BaseThread::ConstructionException("Invalid configuration value: filepath");
			}
		}
		else if(i->first == "max-row-length")
		{
			maxRowLength = std::stoi(i->second);
			if(maxRowLength < 1)
			{
				throw BaseThread::ConstructionException("Invalid configuration value: maxRowLength");
			}			
		}
		else if(i->first == "delimiter")
		{
			delimiter = Escape(i->second);
			if(delimiter < 0)
			{
				throw BaseThread::ConstructionException("Invalid configuration value: delimiter");
			}
		}
		else
		{
			//Unrecognized option
			std::cout << "Unrecognized option found: " << i->first << std::endl;
		}
	}

	//Load the current file length
	std::ifstream in(filepath.generic_string().c_str(), std::ios::in);
	if(in.is_open())
	{
		in.seekg (0, in.end);
		currentFilePos = (long)in.tellg();
		in.close();
		if(currentFilePos < 0)
		{
			throw BaseThread::ConstructionException("Failed to fetch specified files length");
		}
	}
	else
	{
		throw BaseThread::ConstructionException("Failed to open specified file");
	}

#ifdef _WIN32
	std::string p(filepath.generic_string());
	LPTSTR path = (LPTSTR)p.c_str();
	TCHAR Drive[3];
	TCHAR File[_MAX_FNAME];
	TCHAR Extension[_MAX_EXT];
	TCHAR Directory[_MAX_DIR];

	_tsplitpath_s(path, Drive, 3, Directory, _MAX_DIR, File, _MAX_FNAME, Extension, _MAX_EXT);
	Drive[2] = (TCHAR)'\0';

	ChangeHandle = FindFirstChangeNotification(Directory, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE);

	if (ChangeHandle == INVALID_HANDLE_VALUE)
	{
		throw BaseThread::ConstructionException("ERROR: FindFirstChangeNotification function failed with code " + boost::lexical_cast<std::string>(static_cast<unsigned long>(GetLastError())) + ".");
	}

	// Make a final validation check on our handle.
	if (ChangeHandle == NULL)
	{
		throw BaseThread::ConstructionException("ERROR: Unexpected NULL from FindFirstChangeNotification.");
	}
#else
	//Initialize the inotify descriptor and add a inotify watch descriptor for the specified path
	inotify_fd = inotify_init();
	if (inotify_fd < 0)
	{
		throw BaseThread::ConstructionException("Failed to initialize inotify descriptor");
	}

	inotify_wd = inotify_add_watch(inotify_fd, filepath.parent_path().generic_string().c_str(), IN_MODIFY);
	if (inotify_wd == -1)
	{
		throw BaseThread::ConstructionException("Failed to add inotify watch filter");
	}
#endif

	//Set up remaining variables
	buffer = new char[maxRowLength];
	running = true;
}

FileSource::~FileSource()
{
	delete[] buffer;

#ifdef _WIN32
	FindCloseChangeNotification(ChangeHandle);
#else
	inotify_rm_watch(inotify_fd, inotify_wd);
	close(inotify_fd);
#endif
}

void FileSource::Main()
{
#ifdef _WIN32
	
	DWORD WaitStatus;

	while (running)
	{
		WaitStatus = WaitForSingleObject(ChangeHandle, (DWORD)timeout);

		switch (WaitStatus)
		{
			case WAIT_OBJECT_0:
			{
				//Note that this will also be called when other files in the same directory are changed.
				ParseNewData();

				if (FindNextChangeNotification(ChangeHandle) == FALSE)
				{
					std::cout << "ERROR: FindNextChangeNotification function failed." << std::endl;
					ExitProcess(GetLastError());
				}
				break;
			}
			case WAIT_TIMEOUT:
			{
				//Timeout
				break;
			}

			default:
			{
				std::cout << "ERROR: Unhandled WaitStatus." << std::endl;
				ExitProcess(GetLastError());
				break;
			}
		}
	}
#else
	struct timeval time;
	int i=0;
	char buf[BUF_LEN];
	fd_set set;

	while (running)
	{
		i = 0;
    	time.tv_sec = 0;
    	time.tv_usec = (int)(timeout * 1000);

		FD_ZERO(&set);
		FD_SET(inotify_fd, &set);

		int sret = select(inotify_fd+1, &set, NULL, NULL, &time);
		if(sret > 0)
		{
			int length = read(inotify_fd, buf, BUF_LEN);
			if (length < 0)
			{
				std::cout << "ERROR: " << strerror(errno) << std::endl;
			}

			while (i < length)
			{
				struct inotify_event *event = (struct inotify_event *) &buf[i];
				if (event->len && event->mask & IN_MODIFY && event->name == filepath.filename().generic_string())
				{
					ParseNewData();
				}

				i += EVENT_SIZE + event->len;
			}
		}
		else if(sret == 0)
		{
			//Timeout
		}
		else
		{
			std::cout << "ERROR: " << strerror(errno) << std::endl;
		}
	}
#endif
}

void FileSource::Stop()
{
	running = false;
}

void FileSource::ParseNewData()
{
	//Open the file
	std::ifstream in(filepath.generic_string().c_str(), std::ios::in);

	if(in.is_open() && currentFilePos >= 0)
	{
		//Move pointer to the last read character
		in.seekg (currentFilePos, in.beg);

		//Parse all new lines until EOF
		while(!in.getline(buffer, maxRowLength).eof())
		{
			LogMessage::Ptr msg(new LogMessage(LogMessage::FN_LOG_ALERT, LogMessage::Notice));
			msg->SetContent(std::string(buffer));
			GetNextStep()->PutMessageOnQueue(msg);

			currentFilePos = (long)in.tellg();
		}

		//Clean up
		in.close();
	}
	else
	{
		std::cout << "Failed to open '" << filepath.generic_string().c_str() << "'" << std::endl;
	}
}

char FileSource::Escape(std::string s)
{
	if(s.length() == 1)
	{
		return s[0];
	}
	else if(s.length() == 2)
	{
		if(s[0] == '\\')
		{
			switch(s[1])
			{
				case 'n':
				{
					return '\n';
				}
				case 'r':
				{
					return '\r';
				}
				case 't':
				{
					return '\t';
				}
				case '0':
				{
					return '\0';
				}
				case 'v':
				{
					return '\v';
				}
				case 'b':
				{
					return '\b';
				}
				case 'f':
				{
					return '\f';
				}
				case 'a':
				{
					return '\a';
				}
				case '?':
				{
					return '\?';
				}
				case '\'':
				{
					return '\'';
				}
				case '\"':
				{
					return '\"';
				}

				default:
				{
					throw BaseThread::ConstructionException("Invalid configuration value: delimiter");
				}
			}
		}	
	}
	else
	{
		throw BaseThread::ConstructionException("Invalid configuration value: delimiter");
	}
	return -1;
}
