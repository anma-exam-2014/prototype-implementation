#ifndef DUMMY_SOURCE_H
#define DUMMY_SOURCE_H

#include "../../base/LogSource.h"

namespace Impl
{
	class DummySource : public Base::LogSource
	{
		REGISTER_SOURCE_CLASS(DummySource)

	public:

		DummySource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

	protected:

		void Main() override;

		void Stop() override;

	private:
		bool running;
		int interval=1000;
	};
}

#endif