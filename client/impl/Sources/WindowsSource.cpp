#ifdef _WIN32
#include "WindowsSource.h"
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <sstream>

using namespace Impl;
using namespace Base;
using namespace Core;

REGISTER_SOURCE_CLASS_IMPL(WindowsSource)

WindowsSource::WindowsSource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options) : LogSource(nextStep, options)
{
	for (auto i = options.begin(); i != options.end(); i++)
	{
		if (i->first == "channel")
		{
			if (i->second == "")
			{
				throw BaseThread::ConstructionException("Invalid configuration value: channel");
			}
			Channel = std::wstring(i->second.begin(), i->second.end());
		}
		else if (i->first == "query")
		{
			if (i->second == "")
			{
				throw BaseThread::ConstructionException("Invalid configuration value: query.");
			}
			Query = std::wstring(i->second.begin(), i->second.end());
		}
		else if (i->first == "retro-active")
		{
			if (i->second == "true" || i->second == "yes")
			{
				//mode = EvtSubscribeStartAtOldestRecord;
				//This is the default, it is already set.
			}
			else if (i->second == "false" || i->second == "no")
			{
				mode = EvtSubscribeToFutureEvents;
			}
			else
			{
				throw BaseThread::ConstructionException("Invalid configuration value: retro-active.");
			}
		}
		else
		{
			std::cout << "A unrecognized option was detected: " + i->first + "." << std::endl;
		}
	}

	// Init wait handle
	WaitHandle = CreateEvent(NULL, TRUE, TRUE, NULL);
	if (WaitHandle == NULL)
	{
		throw BaseThread::ConstructionException("CreateEvent failed with " + boost::lexical_cast<std::string>(static_cast<unsigned long>(GetLastError())) + ".");
	}

	// Subscribe to events.
	hSubscription = EvtSubscribe(NULL, WaitHandle, (LPCWSTR)Channel.c_str(), (LPCWSTR)Query.c_str(), NULL, NULL, NULL, mode);
	if (hSubscription == NULL)
	{
		DWORD status = GetLastError();

		if (ERROR_EVT_CHANNEL_NOT_FOUND == status)
		{
			throw BaseThread::ConstructionException("Channel '" + UTF8_Encode(Channel) + "' was not found.");
		}
		else if (ERROR_EVT_INVALID_QUERY == status)
		{
			throw BaseThread::ConstructionException("Query '" + UTF8_Encode(Query) + "' was not found.");
		}
		else
		{
			throw BaseThread::ConstructionException("EvtSubscribe failed with status " + boost::lexical_cast<std::string>(static_cast<unsigned long>(status)) + ".");
		}

	}

	running = true;
}

WindowsSource::~WindowsSource()
{
	CloseHandle(WaitHandle);
	EvtClose(hSubscription);
}

void WindowsSource::Main()
{
	DWORD status = ERROR_SUCCESS;
	DWORD WaitStatus = 0;

	while (running)
	{
		WaitStatus = WaitForSingleObject(WaitHandle, (DWORD)timeout);

		switch (WaitStatus)
		{
			case WAIT_OBJECT_0: //Signal received
			{
				status = ParseEvents(hSubscription);
				if (status != ERROR_NO_MORE_ITEMS)
				{
					throw std::runtime_error("ERROR: ParseEvents failed with code " + boost::lexical_cast<std::string>(static_cast<unsigned long>(GetLastError())));
				}
				ResetEvent(WaitHandle);
				break;
			}
			case WAIT_TIMEOUT:
			{
				//Timeout, do nothing
				break;
			}
			default: //Error
			{
				throw std::runtime_error("ERROR: WaitForSingleObject failed with code " + boost::lexical_cast<std::string>(static_cast<unsigned long>(GetLastError())));
				break;
			}
		}
	}	
}

void WindowsSource::Stop()
{
	running = false;
}

// Enumerate the events in the result set.
#define ARRAY_SIZE 100
DWORD WindowsSource::ParseEvents(EVT_HANDLE hResults)
{
	DWORD status = ERROR_SUCCESS;
	EVT_HANDLE hEvents[ARRAY_SIZE];
	DWORD dwReturned = 0;

	while (true)
	{
		// Get a block of events from the result set.
		if (!EvtNext(hResults, ARRAY_SIZE, hEvents, INFINITE, 0, &dwReturned))
		{
			if (ERROR_NO_MORE_ITEMS != (status = GetLastError()))
			{
				wprintf(L"EvtNext failed with code %lu\n", status);
			}

			goto cleanup;
		}

		// For each event, call the PrintEvent function which renders the
		// event for display.
		for (DWORD i = 0; i < dwReturned; i++)
		{
			status = ParseEvent(hEvents[i]);
			if (ERROR_SUCCESS == status)
			{
				EvtClose(hEvents[i]);
				hEvents[i] = NULL;
			}
			else
			{
				goto cleanup;
			}
		}
	}

cleanup:

	// Closes any events in case an error occurred above.
	for (DWORD i = 0; i < dwReturned; i++)
	{
		if (NULL != hEvents[i])
			EvtClose(hEvents[i]);
	}

	return status;
}

// Render the event as an XML string and print it.
DWORD WindowsSource::ParseEvent(EVT_HANDLE hEvent)
{
	DWORD status = ERROR_SUCCESS;
	DWORD dwBufferSize = 0;
	DWORD dwBufferUsed = 0;
	DWORD dwPropertyCount = 0;
	LPWSTR pRenderedContent = NULL;

	// The EvtRenderEventXml flag tells EvtRender to render the event as an XML string.
	if (!EvtRender(NULL, hEvent, EvtRenderEventXml, dwBufferSize, pRenderedContent, &dwBufferUsed, &dwPropertyCount))
	{
		if (ERROR_INSUFFICIENT_BUFFER == (status = GetLastError()))
		{
			dwBufferSize = dwBufferUsed;
			pRenderedContent = (LPWSTR)malloc(dwBufferSize);
			if (pRenderedContent)
			{
				EvtRender(NULL, hEvent, EvtRenderEventXml, dwBufferSize, pRenderedContent, &dwBufferUsed, &dwPropertyCount);
			}
			else
			{
				wprintf(L"malloc failed\n");
				status = ERROR_OUTOFMEMORY;
			}
		}

		if (ERROR_SUCCESS != (status = GetLastError()))
		{
			wprintf(L"EvtRender failed with %d\n", GetLastError());
		}
	}

	if (pRenderedContent)
	{
		std::string xml = UTF8_Encode(pRenderedContent);
		GetNextStep()->PutMessageOnQueue(CreateLogMessage(xml));
		free(pRenderedContent);
	}

	return status;
}

Common::LogMessage::Ptr WindowsSource::CreateLogMessage(std::string xml)
{
	boost::property_tree::ptree root;
	std::stringstream ss;
	ss << xml;
	read_xml(ss, root);
	root = root.get_child("Event").get_child("System");

	auto level = root.get<int>("Level");
	auto channel = root.get<std::string>("Channel");

	Common::LogMessage::SeverityValue sv;
	Common::LogMessage::FacilityNumber fn;

	if (level == 1)
	{
		sv = Common::LogMessage::Emergency;
	}
	if (level == 2)
	{
		sv = Common::LogMessage::Error;
	}
	if (level == 3)
	{
		sv = Common::LogMessage::Warning;
	}
	else
	{
		sv = Common::LogMessage::Informational;
	}

	
	if (channel == "System")
	{
		fn = Common::LogMessage::FN_KERN;
	}
	if (channel == "Security")
	{
		fn = Common::LogMessage::FN_KERN;
	}
	if (channel == "Setup")
	{
		fn = Common::LogMessage::FN_KERN;
	}
	else
	{
		fn = Common::LogMessage::FN_USER;
	}


	Common::LogMessage::Ptr msg(new Common::LogMessage(fn, sv));
	msg->SetContent(xml);
	return msg;
}

std::string WindowsSource::UTF8_Encode(const std::wstring &wstr)
{
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

/*std::wstring WindowsSource::UTF8_Decode(const std::string &str)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}*/

#endif
