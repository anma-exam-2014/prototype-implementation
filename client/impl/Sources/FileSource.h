#ifndef FILE_SOURCE_H
#define FILE_SOURCE_H

#include "../../base/LogSource.h"
#include <boost/filesystem.hpp>
#include <string>

#ifdef _WIN32
#include <Windows.h>
#else

#endif

namespace Impl
{
	class FileSource : public Base::LogSource
	{
		REGISTER_SOURCE_CLASS(FileSource)

	public:
		FileSource(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);
		~FileSource();

	protected:

		void Main() override;

		void Stop() override;

	private:
		boost::filesystem::path filepath;
		bool running = false;
		int timeout=1000;

		long currentFilePos=0;
		int maxRowLength=2048;
		char delimiter='\n';
		char* buffer;

	#ifdef _WIN32
		HANDLE ChangeHandle;

	#else
		int inotify_fd;
		int inotify_wd;
	#endif

		void ParseNewData();
		char Escape(std::string s);
	};
}

#endif
