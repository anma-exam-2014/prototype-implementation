#include "../../base/LogAggregator.h"

namespace Impl
{
	class SimpleTimestamper : public Base::LogAggregator
	{
		REGISTER_AGGREGATOR_CLASS(SimpleTimestamper)

	public:

		SimpleTimestamper(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		void HandleMessages(MsgPtrArr msg) override;

	protected:
		std::string InstanceName() override
		{
			return "SimpleTimestamper";
		}

	private:

		boost::posix_time::ptime Epoch;

		std::string GetCurrentDateTime();
		int utcOffset;
	};
}