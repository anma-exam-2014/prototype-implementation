#include "SimpleTimestamper.h"
#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>
//#include <boost/date_time/c_local_time_adjustor.hpp>

#ifdef _WIN32
#pragma warning(disable : 4244)
#pragma warning(disable : 4996)
#endif

using namespace Impl;

REGISTER_AGGREGATOR_CLASS_IMPL(SimpleTimestamper)

SimpleTimestamper::SimpleTimestamper(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: LogAggregator(nextStep, options)
{
	Epoch = boost::posix_time::ptime(boost::gregorian::date(1970, 1, 1));
}

void SimpleTimestamper::HandleMessages(MsgPtrArr msgs)
{
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
		msg->SetTimestamp((boost::posix_time::microsec_clock::universal_time() - Epoch).total_milliseconds());
		GetNextStep()->PutMessageOnQueue(msg);
	}
}