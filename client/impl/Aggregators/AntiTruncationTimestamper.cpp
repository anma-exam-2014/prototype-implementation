#include "AntiTruncationTimestamper.h"
#include <sha.h>
#include <osrng.h>
#include <base64.h>
#include <boost/lexical_cast.hpp>

using namespace Impl;

REGISTER_AGGREGATOR_CLASS_IMPL(AntiTruncationTimestamper)

AntiTruncationTimestamper::AntiTruncationTimestamper(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options)
: LogAggregator(nextStep, options)
{
    std::string wait = options["close-tag-wait-ms"];
    if (wait.empty())
    {
        throw ConstructionException("AntiTruncationTimestamper is missing required option \"close-tag-wait-ms\".");
    }
    waitTimeBeforeGenerateCloseTag = boost::lexical_cast<long>(wait);

	data = Core::PersistentStorageService::getInstance();

	Epoch = boost::posix_time::ptime(boost::gregorian::date(1970, 1, 1));

	if (!data->GetSync(ANTI_TRUNC_CURRENT_HASH).empty())
	{
		if (data->GetSync(ANTI_TRUNC_CURRENT_HASH, currentHash) != ANTI_TRUNC_HASH_LEN)
		{
			throw BaseThread::ConstructionException("Length of the loaded has does not match its specified length");
		}

        lastCloseTagSequenceNumber = boost::lexical_cast<long>(data->GetSync(ANTI_TRUNC_CURRENT_SEQUENCE_NUMBER));
	}
	else
	{
		CryptoPP::AutoSeededRandomPool rng;
		currentHash = new unsigned char[ANTI_TRUNC_HASH_LEN];
		rng.GenerateBlock(currentHash, ANTI_TRUNC_HASH_LEN);

		//Put first sync message on queue.
		Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FacilityNumber::FN_SYSLOG, Common::LogMessage::SeverityValue::Notice));
		Common::LogMessage::StructuredDataContent structuredData;
		structuredData[ANTI_TRUNC_START_HASH] = base64_encode(currentHash, ANTI_TRUNC_HASH_LEN);
		msg->GetStructuredData()[ANTI_TRUNCATION] = structuredData;
		msg->SetTimestamp((boost::posix_time::microsec_clock::universal_time() - Epoch).total_milliseconds());
		msg->SetContent(ANTI_TRUNC_START_MESSAGE);

		//Send to server and hope it gets deleted from local storage before this machine is compromised
		GetNextStep()->PutMessageOnQueue(msg);

        //1 because we just created a new message
        lastCloseTagSequenceNumber = 1;
        data->SetSync(ANTI_TRUNC_CURRENT_SEQUENCE_NUMBER, boost::lexical_cast<std::string>(lastCloseTagSequenceNumber));
	}
}

void AntiTruncationTimestamper::HandleMessages(MsgPtrArr msgs)
{
    boost::mutex::scoped_lock lock(mutexCurrentHash);
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
		msg->SetTimestamp((boost::posix_time::microsec_clock::universal_time() - Epoch).total_milliseconds());

		unsigned char* temp = new unsigned char[ANTI_TRUNC_HASH_LEN];
		ANTI_TRUNC_HASH_ALG().CalculateDigest(temp, currentHash, ANTI_TRUNC_HASH_LEN);
		memset(currentHash, zero, ANTI_TRUNC_HASH_LEN);
		delete[] currentHash;
		currentHash = temp;

		data->SetAsync(ANTI_TRUNC_CURRENT_HASH, currentHash, ANTI_TRUNC_HASH_LEN);

		GetNextStep()->PutMessageOnQueue(msg);
	}
}

void AntiTruncationTimestamper::HandleLastRecieverStatusUpdate(BaseTransmitter::ReceiverStatusInfo info)
{
    if (info.queueEmpty
        && info.timeEmptyQueue >= waitTimeBeforeGenerateCloseTag 
        && info.canSendImmediately 
        && info.lastMessageSequenceNumber > lastCloseTagSequenceNumber)
    {
        boost::mutex::scoped_lock lock(mutexCurrentHash);
        Common::LogMessage::Ptr msg(new Common::LogMessage(Common::LogMessage::FacilityNumber::FN_SYSLOG, Common::LogMessage::SeverityValue::Notice, false));

        Common::LogMessage::StructuredDataContent structuredData;
        structuredData[ANTI_TRUNC_CLOSE_HASH] = base64_encode(currentHash, ANTI_TRUNC_HASH_LEN);

        msg->GetStructuredData()[ANTI_TRUNCATION] = structuredData;
        msg->SetTimestamp((boost::posix_time::microsec_clock::universal_time() - Epoch).total_milliseconds());
        msg->SetContent(ANTI_TRUNC_CLOSE_MESSAGE);

        GetNextStep()->PutMessageOnQueue(msg);

        //+ 1 since we created a new message
        lastCloseTagSequenceNumber = info.lastMessageSequenceNumber + 1;
        data->SetAsync(ANTI_TRUNC_CURRENT_SEQUENCE_NUMBER , boost::lexical_cast<std::string>(lastCloseTagSequenceNumber));
    }
}

std::string AntiTruncationTimestamper::base64_encode(const byte* src, uint32_t len)
{
	std::string encoded;
	CryptoPP::StringSource ss(src, len, true,
		new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded), false, 0));
	return encoded;
}