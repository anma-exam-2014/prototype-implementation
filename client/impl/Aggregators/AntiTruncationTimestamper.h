#ifndef ANTI_TRUNCATION_TIMESTAMPER_H
#define ANTI_TRUNCATION_TIMESTAMPER_H

#include "../../base/LogAggregator.h"
#include "../../core/PersistentStorage.h"
#include "../../../common/DEFINITIONS.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <string>

namespace Impl
{
	class AntiTruncationTimestamper : public Base::LogAggregator
	{
		REGISTER_AGGREGATOR_CLASS(AntiTruncationTimestamper)

	public:

		AntiTruncationTimestamper(Base::BaseReceiver::Ptr nextStep, Core::ConfigOptions options);

		void HandleMessages(MsgPtrArr msg) override;

	protected:
		std::string InstanceName() override
		{
			return "AntiTruncationTimestamper";
		}

        void HandleLastRecieverStatusUpdate(BaseTransmitter::ReceiverStatusInfo info) override;

	private:
        uint64_t waitTimeBeforeGenerateCloseTag;
        uint64_t lastCloseTagSequenceNumber;

		std::string base64_encode(const unsigned char* src, uint32_t len);

		Core::PersistentStorageService* data;

		unsigned char* currentHash;

		volatile int zero = 0;
		boost::posix_time::ptime Epoch;

        boost::mutex mutexCurrentHash;
	};
}

#endif//ANTI_TRUNCATION_TIMESTAMPER_H