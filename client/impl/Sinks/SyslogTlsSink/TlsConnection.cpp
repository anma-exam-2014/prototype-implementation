#include "TlsConnection.h"

#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <thread>

using namespace Impl;
using namespace Impl::SyslogTlsSinkImpl;

class TlsConnectionImpl : public TlsConnection
{
public:

    std::string state;

	TlsConnectionImpl(std::string chainPemFile, 
		std::string privateKeyFile,
		std::string host, std::string port)
		: host(host),
		  port(port),
		  chainPemFile(chainPemFile),
		  privateKeyFile(privateKeyFile),
		  io_service_(),
		  context_(NULL),
		  socket_(NULL),
		  work_(NULL)
	{
		_canSend = false;
		_stop = false;
		firstAttempt = true;
		seconds_wait_for_reconnect = 1;
        state = "Constructor";
	}

	~TlsConnectionImpl()
    {
        state = "Destructor";
	};

	virtual bool CanSend()
	{
		return _canSend;
	}

	//this blocks caller until completed
	virtual void SendAsync(std::string& messages)
    {
        state = "SendAsync";
		boost::shared_ptr<std::string> msg(new std::string(messages));
		boost::asio::async_write(
			*socket_, 
			boost::asio::buffer(*msg),
			boost::bind(
				&TlsConnectionImpl::handle_write, 
				this, 
				boost::asio::placeholders::error,
				msg
			)
		);
		hasTriedToSend = true;
	}

protected:
	virtual void Main()
	{
		while (!_stop)
		{
			try{
                _start();
                state = "io_service.run()";
				io_service_.run();
				if (new_error)
					boost::asio::detail::throw_error(latestError, "handle_read");
			}
			catch (const boost::system::system_error& ex)
			{
                if (std::cout.good())
				    std::cout << "TlsConnection error: " << ex.what() << std::endl;
				new_error = false;
			}

            state = "Connection lost";
			_canSend = false;

			boost::system::error_code ec; //discard whatever error
			if (socket_)
				socket_->shutdown(ec);

			//discard existing socket
			if (socket_)
				socket_.reset();

			//reset io_service
			io_service_.reset();

			if (!_stop)
			{
				if (hasTriedToSend && connectionFailedCallback)
				{
					connectionFailedCallback();
				}
				hasTriedToSend = false;
				std::cout << "TlsConnection: Connection to remote server failed/closed. Waiting " << seconds_wait_for_reconnect << "s before reconnecting." << std::endl;
				//we keep trying until we succeed, but let's have a wiating period before then
                state = "Sleep";
				boost::this_thread::sleep_for(boost::chrono::seconds(seconds_wait_for_reconnect));
				seconds_wait_for_reconnect *= 2;
				if (seconds_wait_for_reconnect > 900)
					seconds_wait_for_reconnect = 900;
			}
		}
	}

	virtual void Stop()
	{
		_stop = true;
		io_service_.stop();
	}

	///////// HANDLERS //////////////
public:
	
	bool verify_certificate(bool preverified,
		boost::asio::ssl::verify_context& ctx)
    {
        state = "Cert verify";
		// The verify callback can be used to check whether the certificate that is
		// being presented is valid for the peer. For example, RFC 2818 describes
		// the steps involved in doing this for HTTPS. Consult the OpenSSL
		// documentation for more details. Note that the callback is called once
		// for each certificate in the certificate chain, starting from the root
		// certificate authority.
		int8_t subject_name[256];
		X509_STORE_CTX *cts = ctx.native_handle();
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		std::cout << "CTX ERROR : " << cts->error << std::endl;

		int32_t depth = X509_STORE_CTX_get_error_depth(cts);
		std::cout << "CTX DEPTH : " << depth << std::endl;

		switch (cts->error)
		{
		case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
			std::cout << "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT" << std::endl;
			break;
		case X509_V_ERR_CERT_NOT_YET_VALID:
		case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
			std::cout << "Certificate not yet valid!!" << std::endl;
			break;
		case X509_V_ERR_CERT_HAS_EXPIRED:
		case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
			std::cout << "Certificate expired.." << std::endl;
			break;
		case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:
			std::cout << "Self signed certificate in chain!!!" << std::endl;
			preverified = true;
			break;
		default:
			break;
		}
		const int32_t name_length = 256;
		X509_NAME_oneline(X509_get_subject_name(cert), reinterpret_cast<char*>(subject_name), name_length);
		std::cout << "Verifying " << subject_name << std::endl;
		std::cout << "Verification status : " << preverified << std::endl;
		return preverified;
	}

	void handle_connect(const boost::system::error_code& error)
	{
		if (!error)
        {
            state = "Connect Y";
			if (firstAttempt)
			{
				socket_->next_layer().set_option(boost::asio::ip::tcp::no_delay(false));
				firstAttempt = false;
			}

			socket_->async_handshake(boost::asio::ssl::stream_base::client,
				boost::bind(&TlsConnectionImpl::handle_handshake, this,
				boost::asio::placeholders::error));
		}
		else
        {
            state = "Connect N";
			_canSend = false;
			//store error
			latestError = error;
			new_error = true;
			//we stop io_service so we can try again.
			work_.reset();
			//cancel all current operations
			socket_->lowest_layer().cancel();
		}
	}

	void handle_handshake(const boost::system::error_code& error)
	{
		if (!error)
        {
            state = "Handshake Y";
			_canSend = true;

			boost::shared_ptr<boost::asio::streambuf> response(new boost::asio::streambuf());

			boost::asio::async_read_until(*socket_,
				*response,
				' ',
				boost::bind(&TlsConnectionImpl::handle_read, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred,
					response
				)
			);

			std::cout << "TlsConnection: Connection success!" << std::endl;
			//we reset the wait timer since we made a successfull connection
			seconds_wait_for_reconnect = 1;
		}
		else
        {
            state = "Handshake N";
			_canSend = false;
			//store error
			latestError = error;
			new_error = true;
			//we stop io_service so we can try again.
			work_.reset();
			//cancel all current operations
			socket_->lowest_layer().cancel();
		}
	}

	void handle_write(const boost::system::error_code& error, boost::shared_ptr<std::string> written)
	{
		if (!error)
        {
            state = "Write Y";
			if (writeSuccessCallback)
				writeSuccessCallback();
		}
		else
        {
            state = "Write N";
			_canSend = false;
			//store error
			latestError = error;
			new_error = true;
			//we stop io_service so we can try again.
			work_.reset();
			//cancel all current operations
			socket_->lowest_layer().cancel();
		}
	}

	//this should only be called if the server actualy sends a response
	//OR more likely of the connection closes
	void handle_read(const boost::system::error_code& error,
		size_t bytes_transferred, 
		boost::shared_ptr<boost::asio::streambuf> response)
	{
		if (!error)
        {
            state = "Read Y";
			//the server wrote to us!

			//acording to RFC 5425 this should not happen.
			//But the server which rests in the same code base as this client
			//DOES write back the number of log messages that has been succesfully
			//parsed and stored
			std::stringstream ss;
			std::string word;
			ss << response;
			ss >> word;

			if (wordReceivedCallback)
				wordReceivedCallback(word);

			boost::shared_ptr<boost::asio::streambuf> newResponse(new boost::asio::streambuf());

			//let's keep reading
			boost::asio::async_read_until(*socket_,
				*newResponse,
				' ',
				boost::bind(&TlsConnectionImpl::handle_read, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred,
					newResponse
				)
			);
		}
		else
        {
            state = "Read N";
			_canSend = false;
			//store error
			latestError = error;
			new_error = true;
			//we stop io_service so we can try again.
			work_.reset();
			//cancel all current operations
			socket_->lowest_layer().cancel();
		}
	}

	virtual void RestartConnection()
    {
        state = "Restart Connection";
		if (_canSend)
		{
			_canSend = false;
			//we stop io_service so we can try again.
			work_.reset();
			//cancel all current operations
			socket_->lowest_layer().cancel();
		}
	}

	//Send complete callback. the argument should be a callable with 
	//void as return type and no arguments
	virtual void SetSendCompleteCallback(std::function<void()> f)
	{
		writeSuccessCallback = f;
	}

	//Response received callback. The argument should be a callable that accepts
	// std::string as arguemnt (the received string)
	virtual void SetWordReceivedCallback(std::function<void(std::string&)> f)
	{
		wordReceivedCallback = f;
	}

	//Response received callback. The argument should be a callable that accepts
	// std::string as arguemnt (the received string)
	virtual void SetConnectionFailedCallback(std::function<void()> f)
	{
		connectionFailedCallback = f;
	}

private:
	std::string host;
	std::string port;
	std::string chainPemFile;
	std::string privateKeyFile;
	boost::asio::io_service io_service_;
	boost::shared_ptr<boost::asio::ssl::context> context_;
	boost::shared_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>> socket_;
	boost::shared_ptr<boost::asio::io_service::work> work_;
	bool _canSend;
	bool _stop;
	bool firstAttempt = true;
	bool hasTriedToSend = false;
	bool new_error = false;
	int seconds_wait_for_reconnect;
	std::function<void()> writeSuccessCallback;
	std::function<void(std::string&)> wordReceivedCallback;
	std::function<void()> connectionFailedCallback;
	boost::system::error_code latestError;

	void _start()
    {
        state = "Start";
		std::cout << "TlsConnection: Trying to (re)connect to " << host << ":" << port << std::endl;
		_canSend = false;
		firstAttempt = true;

		//create a new socket
		context_.reset(new boost::asio::ssl::context(boost::asio::ssl::context::tlsv12));
		context_->set_options(boost::asio::ssl::context_base::default_workarounds);
		context_->clear_options(boost::asio::ssl::context::no_compression);
		if (!chainPemFile.empty())
		{
			context_->use_certificate_chain_file(chainPemFile);
		}
		if (!privateKeyFile.empty())
		{
			context_->use_certificate_file(privateKeyFile, boost::asio::ssl::context_base::pem);
			context_->use_private_key_file(privateKeyFile, boost::asio::ssl::context_base::pem);
		}


		socket_.reset(new boost::asio::ssl::stream<boost::asio::ip::tcp::socket>(io_service_, *context_));

		//make sure the io_service does not run out of jobs
		work_.reset(new boost::asio::io_service::work(io_service_));

		boost::asio::ip::tcp::resolver resolver(io_service_);
		boost::asio::ip::tcp::resolver::query query(host, port);
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		socket_->set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert);
		socket_->set_verify_callback(
			boost::bind(&TlsConnectionImpl::verify_certificate, this, _1, _2));


		boost::asio::async_connect(socket_->lowest_layer(), endpoint_iterator,
			boost::bind(&TlsConnectionImpl::handle_connect, this,
			boost::asio::placeholders::error));
	}
};



TlsConnection::Ptr TlsConnection::CreateConnectionHandler(std::string host, std::string port, std::string chainPemFile, std::string privateKeyFile)
{
	return TlsConnection::Ptr(new TlsConnectionImpl(chainPemFile, privateKeyFile, host, port));
}
