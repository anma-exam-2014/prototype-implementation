#ifndef TLSCONNECTION_H
#define TLSCONNECTION_H

#include "../../../base/BaseThread.h"
#include "../../../core/BaseException.h"

namespace Impl
{
	namespace SyslogTlsSinkImpl
	{
		class TlsConnection : public Base::BaseThread
		{
		public:
			typedef boost::shared_ptr<TlsConnection> Ptr;

			class SendFailed : public Core::BaseException
			{
			public:
				SendFailed(const char* const& msg)
					: BaseException(msg)
				{}
			};

			static Ptr CreateConnectionHandler(std::string host, std::string port, std::string chainPemFile, std::string privateKeyFile);

			TlsConnection(const TlsConnection& obj) = delete;

			//reset connection (for example due to timed out reply)
			virtual void RestartConnection() = 0;
			
			//Equivalent to: 
			//return (is_connected && no_pending_writes)
			virtual bool CanSend() = 0;

			//this returns immediately
			virtual void SendAsync(std::string& messages) = 0;

			//Send complete callback. the argument should be a callable with 
			//void as return type and no arguments
			virtual void SetSendCompleteCallback(std::function<void()>) = 0;

			//Response received callback. The argument should be a callable that accepts
			// std::string as arguemnt (the received string)
			virtual void SetWordReceivedCallback(std::function<void(std::string&)>) = 0;

			//Send complete callback. the argument should be a callable with 
			//void as return type and no arguments
			virtual void SetConnectionFailedCallback(std::function<void()>) = 0;

		protected:
			std::string InstanceName() override
			{
				return "TlsConnection";
			}

			virtual void Main() = 0;
			virtual void Stop() = 0;

			TlsConnection(){};
		};
	}
}


#endif