#ifndef FILEBUFFEREDSTRINGQUEUE_H
#define FILEBUFFEREDSTRINGQUEUE_H

#include "../../../core/BaseException.h"
#include "../../../../common/sqlite3/sqlite3.h"
#include <boost/shared_ptr.hpp>
#include <fstream>

namespace Impl
{
	namespace SyslogTlsSinkImpl
	{

		class FileBufferedStringQueue
		{
		public:
			typedef boost::shared_ptr<FileBufferedStringQueue> Ptr;

			FileBufferedStringQueue();
			~FileBufferedStringQueue();

			void StartAppend();
			void AppendOne(std::string toAppend);
			void RevertAppends();
			void CommitAppends();

			int GetFirst(int nStrings, std::string& out);

			void DeleteFirst(int nStrings);

			int GetNrStrings();

			class IOException : public Core::BaseException
			{
			public:
				IOException(const std::string& msg) :BaseException(msg){};
			};

		private:
			::sqlite3* db;
			::sqlite3_stmt* preparedCountStmt;
			::sqlite3_stmt* preparedSelectStmt;
			::sqlite3_stmt* preparedInsertStmt;
			::sqlite3_stmt* preparedDeleteStmt;
		};
	}
}


#endif
