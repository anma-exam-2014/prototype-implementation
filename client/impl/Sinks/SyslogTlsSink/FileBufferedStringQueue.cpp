#include "FileBufferedStringQueue.h"
#include "../../../core/PersistentStorage.h"
#include <iostream>

using namespace Impl;
using namespace SyslogTlsSinkImpl;

FileBufferedStringQueue::FileBufferedStringQueue()
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    auto lock = pss->GetScopedSqlLock();
    //Check if library is thread_safe
    if (!sqlite3_threadsafe())
    {
        throw IOException("SQLite library is not compile with thread safe operation enabled");
    }

    if (sqlite3_open_v2(Core::PersistentStorageService::GetFilename().c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL) != SQLITE_OK)
    {
        throw IOException(std::string("Can't open log storage: ") + sqlite3_errmsg(db));
    }

    sqlite3_busy_timeout(db, 10000);

    char *zErrMsg = 0;
    if (sqlite3_exec(db, "PRAGMA auto_vacuum = FULL;", 0, 0, &zErrMsg) != SQLITE_OK)
    {
        throw IOException(std::string("SQL error: ") + zErrMsg);
    }
    if (sqlite3_exec(db, "VACUUM;", 0, 0, &zErrMsg) != SQLITE_OK)
    {
        throw IOException(std::string("SQL error: ") + zErrMsg);
    }

	const char* query =
		"CREATE TABLE IF NOT EXISTS BUFFER("  \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT," \
		"DATA TEXT NOT NULL); ";

	if (sqlite3_exec(db, query, 0, 0, &zErrMsg) != SQLITE_OK){
		throw IOException(std::string("SQL error: ") + zErrMsg);
	}

	if (sqlite3_prepare_v2(db, "INSERT INTO BUFFER (DATA) "\
		"VALUES (?);", -1, &preparedInsertStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare_v2(db, "SELECT COUNT(ID) FROM BUFFER ;", -1, &preparedCountStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare_v2(db, "SELECT DATA FROM BUFFER ORDER BY ID ASC LIMIT ? ;", -1, &preparedSelectStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare_v2(db, "DELETE FROM BUFFER WHERE ID IN (SELECT ID FROM BUFFER ORDER BY ID ASC LIMIT ?);", -1, &preparedDeleteStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}
}

FileBufferedStringQueue::~FileBufferedStringQueue()
{
    char *zErrMsg = 0;
    if (sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, &zErrMsg) != SQLITE_OK){
        //do nothing on error
        //throw IOException(std::string("SQL error: ") + zErrMsg);
    }
    if (sqlite3_finalize(preparedDeleteStmt) != SQLITE_OK)
    {
        throw IOException(std::string("Close preparedDeleteStmt failed: ") + sqlite3_errmsg(db));
    }
    if (sqlite3_finalize(preparedInsertStmt) != SQLITE_OK)
    {
        throw IOException(std::string("Close preparedInsertStmt failed: ") + sqlite3_errmsg(db));
    }
    if (sqlite3_finalize(preparedSelectStmt) != SQLITE_OK)
    {
        throw IOException(std::string("Close preparedSelectStmt failed: ") + sqlite3_errmsg(db));
    }
    if (sqlite3_finalize(preparedCountStmt) != SQLITE_OK)
    {
        throw IOException(std::string("Close preparedCountStmt failed: ") + sqlite3_errmsg(db));
    }
    if (sqlite3_close(db) != SQLITE_OK)
    {
        throw IOException(std::string("Close database failed: ") + sqlite3_errmsg(db));
    }
}

void FileBufferedStringQueue::StartAppend()
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    //wait for the storage file to be available
    pss->WaitForSqlSlot();
    //Start the commit
	char *zErrMsg = 0;
    int result;
	if ((result = sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", 0, 0, &zErrMsg)) != SQLITE_OK){
        if (result == SQLITE_BUSY)
        {
            //try again
            StartAppend();
            return;
        }
        else
        {
            //Allow persistent storage to write again
            pss->ReleaseSqlSlot();
            throw IOException(std::string("SQL error: ") + zErrMsg);
        }
	}
}

void FileBufferedStringQueue::RevertAppends()
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    char *zErrMsg = 0;
    int result;
    if ((result = sqlite3_exec(db, "ROLLBACK TRANSACTION", 0, 0, &zErrMsg)) != SQLITE_OK){
        if (result == SQLITE_BUSY)
        {
            //try again
            RevertAppends();
            return;
        }
        else
        {
            //Allow persistent storage to write again
            pss->ReleaseSqlSlot();
            throw IOException(std::string("SQL error: ") + zErrMsg);
        }
    }
    //wait for the storage file to be available
    pss->ReleaseSqlSlot();
}

void FileBufferedStringQueue::CommitAppends()
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    char *zErrMsg = 0;
    int result;
    if ((result = sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, &zErrMsg)) != SQLITE_OK){
        if (result == SQLITE_BUSY)
        {
            //try again
            CommitAppends();
            return;
        }
        else
        {
            //Allow persistent storage to write again
            pss->ReleaseSqlSlot();
            throw IOException(std::string("SQL error: ") + zErrMsg);
        }
    }

    //Allow persistent storage to write again
    pss->ReleaseSqlSlot();
}

void FileBufferedStringQueue::AppendOne(std::string toAppend)
{
    //locked by StartAppend
	if (sqlite3_bind_text(preparedInsertStmt, 1, toAppend.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}
    int result;
	if ((result = sqlite3_step(preparedInsertStmt)) != SQLITE_DONE)
	{
        if (result == SQLITE_BUSY)
        {
            //database is locked by thread/process, let's try again
            //Reset statement
            if (sqlite3_reset(preparedInsertStmt) != SQLITE_OK)
            {
                throw IOException(std::string("Write log message error: reset stmt failed: ") + sqlite3_errmsg(db));
            }
            //Do it!
            AppendOne(toAppend);
            return;
        }
        else
        {
            throw IOException(std::string("Write log message error: execute sqllite step error: ") + sqlite3_errmsg(db));
        }
	}
	if (sqlite3_reset(preparedInsertStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}
}

int FileBufferedStringQueue::GetFirst(int nStrings, std::string& out)
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    auto lock = pss->GetScopedSqlLock();
	// bind parameter
	if (sqlite3_bind_int(preparedSelectStmt, 1, nStrings) != SQLITE_OK)
	{
		throw IOException(std::string("Get log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}

	int rows = 0;

	//perform query
	while (true)
	{
		int s = sqlite3_step(preparedSelectStmt);
		if (s == SQLITE_ROW)
		{
			int bytes;
			char* text;
			text = (char*) sqlite3_column_text(preparedSelectStmt, 0);
			bytes = sqlite3_column_bytes(preparedSelectStmt, 0);
			out.append(text, bytes);
			rows++;
			// text is freed automatically by sqlite
		}
		else if (s == SQLITE_DONE) {
			break;
		}
        else if (s == SQLITE_BUSY)
        {
            //database is locked by thread/process, let's try again
            //Reset statement
            if (sqlite3_reset(preparedSelectStmt) != SQLITE_OK)
            {
                throw IOException(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
            }

            //do it!
            return GetFirst(nStrings, out);
        }
		else {
			throw IOException(std::string("Get log message error: ") + sqlite3_errmsg(db));
		}
	}

	//reset state
	if (sqlite3_reset(preparedSelectStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}

	return rows;
}

void FileBufferedStringQueue::DeleteFirst(int nStrings)
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    auto lock = pss->GetScopedSqlLock();
	if (sqlite3_bind_int(preparedDeleteStmt, 1, nStrings) != SQLITE_OK)
	{
		throw IOException(std::string("Delete log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}
    int result;
	if ((result = sqlite3_step(preparedDeleteStmt)) != SQLITE_DONE)
	{
        if (result == SQLITE_BUSY)
        {
            if (sqlite3_reset(preparedDeleteStmt) != SQLITE_OK)
            {
                throw IOException(std::string("Delete log message error: reset stmt failed: ") + sqlite3_errmsg(db));
            }
            DeleteFirst(nStrings);
            return;
        }
        else
        {
            throw IOException(std::string("Delete log message error: execute sqllite step error: ") + sqlite3_errmsg(db));
        }
	}
	if (sqlite3_reset(preparedDeleteStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Delete log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}
}

int FileBufferedStringQueue::GetNrStrings()
{
    static Core::PersistentStorageService* pss = Core::PersistentStorageService::getInstance();
    auto lock = pss->GetScopedSqlLock();
	int count = -1;
	int s;
	while (true)
	{
		s = sqlite3_step(preparedCountStmt);
		if (s == SQLITE_ROW) 
		{
			count = sqlite3_column_int(preparedCountStmt, 0);
		}
		else
		{
			break;
		}
	}

	if (count < 0 || s != SQLITE_DONE)
	{
		throw IOException(std::string("Count log message error: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_reset(preparedCountStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Count log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}

	return count;
}
