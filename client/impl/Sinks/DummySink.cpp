#include "DummySink.h"
#include <iostream>

using namespace Impl;

REGISTER_SINK_CLASS_IMPL(DummySink)

DummySink::DummySink(Core::ConfigOptions options)
: LogSink(options)
{
	for (auto i = options.begin(); i != options.end(); i++)
	{
		if (i->first == "verbose")
		{
			if (i->second == "false")
			{
				verbose = false;
			}
		}
		else
		{
			std::cout << "DummySink: Unknown option '" << i->first << "'" << std::endl;
		}
	}
}

void DummySink::HandleMessages(MsgPtrArr msgs)
{
	if (verbose)
	{
		BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
		{
			std::cout << msg->GenerateFrame() << std::endl;
		}
	}
    timeSinceLastMessage = 0;
}

Base::BaseTransmitter::ReceiverStatusInfo DummySink::LoopTickUpdate(uint64_t realTickLengtMs)
{
    timeSinceLastMessage = realTickLengtMs;

    Base::BaseTransmitter::ReceiverStatusInfo info;
    info.canSendImmediately = true;
    info.queueEmpty = true;
    info.timeEmptyQueue = timeSinceLastMessage;
    info.lastMessageSequenceNumber = ReceievedMessagesAllTime();
    return info;
}