#ifndef DUMMYSINK_H
#define DUMMYSINK_H

#include "../../base/LogSink.h"

namespace Impl
{
	class DummySink : public Base::LogSink
	{
		REGISTER_SINK_CLASS(DummySink);

	public:
		DummySink(Core::ConfigOptions options);

		void HandleMessages(MsgPtrArr msg);

        virtual Base::BaseTransmitter::ReceiverStatusInfo LoopTickUpdate(uint64_t realTickLengtMs) override;

	protected:
		std::string InstanceName() override
		{
			return "DummySink";
		}

	private:
		bool verbose = true;

        uint64_t timeSinceLastMessage = 0;
	};
}

#endif