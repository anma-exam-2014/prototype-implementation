#include "SyslogTlsSink.h"

#include <openssl/pem.h>
#include <openssl/x509v3.h>

#include <boost/lexical_cast.hpp>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/types.h>
#include <unistd.h>
#endif

using namespace Impl;
using namespace Impl::SyslogTlsSinkImpl;

REGISTER_SINK_CLASS_IMPL(SyslogTlsSink)

std::string getCommonNameFromCert(std::string key);
std::string getHostname();
std::string getPid();

SyslogTlsSink::SyslogTlsSink(Core::ConfigOptions options)
: LogSink(options)
{
	std::string host = options["host"];
	std::string port = options["port"];
	std::string pem = options["ca-chain-file"];
	std::string key = options["private-key"];
	std::string waitForAckStr = options["wait-for-ack"];
	
	bool waitForAck;
	try{
		waitForAck = waitForAckStr.empty() ? false : boost::lexical_cast<bool>(waitForAckStr);
	}
	catch (boost::bad_lexical_cast)
	{
		throw ConstructionException("Value of \"wait-for-ack\" in configuration file could not be parsed!");
	}

	if (host.empty() || port.empty())
	{
		throw ConstructionException("SyslogTlsSink need non-empty 'host' & 'port' options");
	}

	std::string hostname;
	if (!key.empty())
	{
		//get hostname from cert
		hostname = getCommonNameFromCert(key);
		std::cout << "Using Common Name \"" << hostname << "\" from client certificate as hostname for client." << std::endl;
	}
	else if (options.count("hostname") > 0)
	{
		hostname = options["hostname"];
		std::cout << "Using \"" << hostname << "\" from configuration file as hostname for client." << std::endl;
	}
	else
	{
		hostname = getHostname();
		std::cout << "Using machine name \"" << hostname << "\" as hostname for client." << std::endl;
	}

	Common::LogMessage::GlobalHostname(hostname);

	try{
		connection = TlsConnection::Ptr(TlsConnection::CreateConnectionHandler(host, port, pem, key));
	}
	catch (std::exception& e)
	{
		throw ConstructionException((std::string("Error while creating remote connection handler: ") + e.what()).c_str());
	}
	
	try{
		buffer = FileBufferedStringQueue::Ptr(new FileBufferedStringQueue());
	}
	catch (FileBufferedStringQueue::IOException &e)
	{
		throw ConstructionException(e.what());
	}

	if (waitForAck)
	{
		// this is our own extension of RFC 5425 where we wait for an application level
		// ack of each message before removing them from the buffer.
		// this is more reduntant in case of connection loss, but it is 
		// not conforming to the standard. It must be enabled on the server as well as
		// the client
		connection->SetWordReceivedCallback(
			[this](std::string& word)
		{
            int n = boost::lexical_cast<int>(word);
            this->buffer->DeleteFirst(n);
			this->nrPendingLogs -= n;
            this->timeSinceActivity = 0;
            this->timeEmptyQueue = 0;
		});
	}
	else
	{
		// this is the default and conforms to RFC 5425
		connection->SetSendCompleteCallback(
			[this]()
        {
            this->buffer->DeleteFirst(this->nrPendingLogs);
            this->nrPendingLogs = 0;
            this->timeSinceActivity = 0;
            this->timeEmptyQueue = 0;
		});
	}

	//either way we must be notified if something we send did not arrive
	// so that we may try to send the same information again
	connection->SetConnectionFailedCallback([this](){
		this->nrPendingLogs = 0;
	});
}

void SyslogTlsSink::Main()
{
	//Open connection
	connection->Start();

	//call parent method
	Base::LogSink::Main();

	//wait for connection thread to close
	connection->StopJoin(true);
}

void SyslogTlsSink::HandleMessages(MsgPtrArr msgs)
{
	buffer->StartAppend();
	BOOST_FOREACH(Common::LogMessage::Ptr msg, msgs)
	{
		try{
			std::string frame = msg->GenerateFrame();
			std::stringstream packet;
			packet << frame.length() << ' ' << frame;
			buffer->AppendOne(packet.str());
		}
		catch (std::exception &e)
		{
			std::cout << e.what();
		}
	}
    try{
        buffer->CommitAppends();
    }
    catch (FileBufferedStringQueue::IOException)
    {
        //could not commit all new messages
        buffer->RevertAppends();
        //let's try again
        HandleMessages(msgs);
    }
}

Base::BaseTransmitter::ReceiverStatusInfo SyslogTlsSink::LoopTickUpdate(uint64_t realTickLengtMs)
{
	//sanity checking
	if (connection->GetStatus() != RUNNING)
	{
		if (connection->GetStatus() == FINISHED)
		{
			//the thread has stopped permanently
			//we are probably in a shutdown state
		}
		else if (connection->GetStatus() == STOPPED_ERROR)
		{
			//the connection stop due to a error, let's 
			//abort everything and notify main thread
			throw connection->GetException();
		}
	}

	//try to send
    int available = buffer->GetNrStrings();
	try{
        if (connection->CanSend())
        {
            if (available)
            {
                timeEmptyQueue = 0;
                if (nrPendingLogs == 0)
                {
                    std::string logs;
                    const int MAX = 10000;
                    nrPendingLogs = buffer->GetFirst((available > MAX ? MAX : available), logs);
                    connection->SendAsync(logs);
                    timeSinceActivity = 0;
                }
                else
                {
                    timeSinceActivity += (int)realTickLengtMs;
                    if (timeSinceActivity > 30000)
                    {
                        //Some messages have been pending confirmation for a long time
                        //there must be something wrong with the connection
                        connection->RestartConnection();
                    }
                }
            }
        }
	}
	catch (TlsConnection::SendFailed& e)
	{
		std::cout << "Send failed: " << e.what() << std::endl;
    }

    //we update this, regardless if the queue is empty or not
    timeEmptyQueue += (int)realTickLengtMs;

    //update status
    Base::BaseTransmitter::ReceiverStatusInfo info;
    info.queueEmpty = (available == 0 && nrPendingLogs == 0);
    info.timeEmptyQueue = timeEmptyQueue;
    info.lastMessageSequenceNumber = ReceievedMessagesAllTime() - available;
    info.canSendImmediately = connection->CanSend();
    return info;
}

std::string getCN(X509 *cert);
void getSubjectAltName(X509 *cert);

std::string getCommonNameFromCert(std::string key)
{
	FILE *fpem;
	X509 *cert;

#ifdef _WIN32
#pragma warning (disable : 4996)
#endif
	if (!(fpem = fopen(key.c_str(), "r"))) {
		throw Base::BaseThread::ConstructionException("Couldn't open the PEM file: %s\n" + key);
	}

	if (!(cert = PEM_read_X509(fpem, NULL, NULL, NULL))) {
		fclose(fpem);
		throw Base::BaseThread::ConstructionException("Failed to read the PEM file: %s\n" + key);
	}

	std::string name = getCN(cert);

	fclose(fpem);
	OPENSSL_free(cert);

	return name;
}

std::string getCN(X509 *cert)
{
	auto subjName = X509_get_subject_name(cert);
	int idx;

	if (!subjName)
		throw Base::BaseThread::ConstructionException("X509_get_subject_name failed");

	idx = X509_NAME_get_index_by_NID(subjName, NID_commonName, -1);
	X509_NAME_ENTRY *entry = X509_NAME_get_entry(subjName, idx);
	ASN1_STRING *entryData = X509_NAME_ENTRY_get_data(entry);
	unsigned char *utf8;
	int length = ASN1_STRING_to_UTF8(&utf8, entryData);

	std::string CommonName((char*) utf8, length);

	OPENSSL_free(utf8);

	return CommonName;
}

std::string getPid()
{
#ifdef _WIN32
	DWORD pid = GetCurrentProcessId();
#else
	pid_t pid = getpid();
#endif

	return boost::lexical_cast<std::string>(pid);

}


std::string getHostname()
{
	std::string hostname;
	const int BUF_SIZE = 1024;
	char buf[BUF_SIZE];
#ifdef _WIN32
	DWORD dwCompNameLen = BUF_SIZE;

	if (0 != GetComputerName(buf, &dwCompNameLen)) {
		hostname = std::string(buf, dwCompNameLen);
	}
	else
	{
		Base::BaseThread::ConstructionException("Unable to get hostname of machine, please provide one in the configuration file or use a client certificate.");
	}
#else
	if (0 == gethostname(buf, BUF_SIZE))
	{
		hostname = std::string(buf);
	}
	else
	{
		Base::BaseThread::ConstructionException("Unable to get hostname of machine, please provide one in the configuration file or use a client certificate.");
	}
#endif

	return hostname;
}
