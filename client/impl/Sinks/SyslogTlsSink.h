#include "../../base/LogSink.h"
#include "SyslogTlsSink/TlsConnection.h"
#include "SyslogTlsSink/FileBufferedStringQueue.h"

namespace Impl
{
	class SyslogTlsSink : public Base::LogSink
	{
		REGISTER_SINK_CLASS(SyslogTlsSink)

	public:
		SyslogTlsSink(Core::ConfigOptions options);

		virtual void Main() override;

		virtual Base::BaseTransmitter::ReceiverStatusInfo LoopTickUpdate(uint64_t realTickLengtMs) override;

	protected:
		void HandleMessages(MsgPtrArr msg) override;

	protected:
		std::string InstanceName() override
		{
			return "SyslogTlsSink";
		}

	private:
		int nrPendingLogs = 0;
		int timeSinceActivity = 0;
		SyslogTlsSinkImpl::TlsConnection::Ptr connection;
		SyslogTlsSinkImpl::FileBufferedStringQueue::Ptr buffer;

        int timeEmptyQueue = 0;

	};
}
