#ifndef THREAD_FACTORY
#define THREAD_FACTORY

#include "ConfigParser.h"
#include "../base/LogSink.h"
#include "../base/LogSigner.h"
#include "../base/LogAggregator.h"
#include "../base/LogFilter.h"
#include "../base/LogSource.h"

#include <vector>
#include <map>

namespace Core
{
	class ThreadFactory
	{
	public:
		class ClassNotFoundException : public BaseException
		{
		public:
			ClassNotFoundException(const char* const& classname) 
				: BaseException(classname)	{}
		};

		static Base::LogSink::Ptr CreateSink(std::string name, ConfigOptions options);
		static Base::LogSigner::Ptr CreateSigner(std::string name, ConfigOptions options, Base::BaseReceiver::Ptr next);
		static Base::LogAggregator::Ptr CreateAggregator(std::string name, ConfigOptions options, Base::BaseReceiver::Ptr next);
		static Base::LogFilter::Ptr CreateFilter(std::string name, ConfigOptions options, Base::BaseReceiver::Ptr next);
		static Base::LogSource::Ptr CreateSource(std::string name, ConfigOptions options, Base::BaseReceiver::Ptr next);

		static void registerSink(const std::string& classname, Base::FactoryImpl::SinkCreator* creator);
		static void registerSigner(const std::string& classname, Base::FactoryImpl::SignerCreator* creator);
		static void registerAggregator(const std::string& classname, Base::FactoryImpl::AggregatorCreator* creator);
		static void registerFilter(const std::string& classname, Base::FactoryImpl::FilterCreator* creator);
		static void registerSource(const std::string& classname, Base::FactoryImpl::SourceCreator* creator);

		static std::vector<std::string> GetRegisteredSinks();
		static std::vector<std::string> GetRegisteredSigners();
		static std::vector<std::string> GetRegisteredAggregators();
		static std::vector<std::string> GetRegisteredFilters();
		static std::vector<std::string> GetRegisteredSources();

	private:
		static std::map<std::string, Base::FactoryImpl::SinkCreator*>& getSinkTable();
		static std::map<std::string, Base::FactoryImpl::SignerCreator*>& getSignerTable();
		static std::map<std::string, Base::FactoryImpl::AggregatorCreator*>& getAggregatorTable();
		static std::map<std::string, Base::FactoryImpl::FilterCreator*>& getFilterTable();
		static std::map<std::string, Base::FactoryImpl::SourceCreator*>& getSourceTable();
	};
}

#endif