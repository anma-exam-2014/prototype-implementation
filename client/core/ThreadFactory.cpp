#include "ThreadFactory.h"

using namespace Core;
using namespace Base;
using namespace Base::FactoryImpl;

void ThreadFactory::registerSink(const std::string& classname, Base::FactoryImpl::SinkCreator* creator)
{
	getSinkTable()[classname] = creator;
}

LogSink::Ptr ThreadFactory::CreateSink(std::string classname, ConfigOptions options)
{
	std::map<std::string, SinkCreator*>::iterator i;
	i = getSinkTable().find(classname);

	if (i != getSinkTable().end())
		return LogSink::Ptr(i->second->create(options));
	else
		throw ThreadFactory::ClassNotFoundException(classname.c_str());
}

std::map<std::string, SinkCreator*>& ThreadFactory::getSinkTable()
{
	static std::map<std::string, SinkCreator*> table;
	return table;
}



void ThreadFactory::registerSigner(const std::string& classname, Base::FactoryImpl::SignerCreator* creator)
{
	getSignerTable()[classname] = creator;
}

LogSigner::Ptr ThreadFactory::CreateSigner(std::string classname, ConfigOptions options, Base::BaseReceiver::Ptr next)
{
	std::map<std::string, SignerCreator*>::iterator i;
	i = getSignerTable().find(classname);

	if (i != getSignerTable().end())
		return LogSigner::Ptr(i->second->create(next, options));
	else
		throw ThreadFactory::ClassNotFoundException(classname.c_str());
}

std::map<std::string, SignerCreator*>& ThreadFactory::getSignerTable()
{
	static std::map<std::string, SignerCreator*> table;
	return table;
}




void ThreadFactory::registerAggregator(const std::string& classname, Base::FactoryImpl::AggregatorCreator* creator)
{
	getAggregatorTable()[classname] = creator;
}

LogAggregator::Ptr ThreadFactory::CreateAggregator(std::string classname, ConfigOptions options, Base::BaseReceiver::Ptr next)
{
	std::map<std::string, AggregatorCreator*>::iterator i;
	i = getAggregatorTable().find(classname);

	if (i != getAggregatorTable().end())
		return LogAggregator::Ptr(i->second->create(next, options));
	else
		throw ThreadFactory::ClassNotFoundException(classname.c_str());
}

std::map<std::string, AggregatorCreator*>& ThreadFactory::getAggregatorTable()
{
	static std::map<std::string, AggregatorCreator*> table;
	return table;
}



void ThreadFactory::registerFilter(const std::string& classname, Base::FactoryImpl::FilterCreator* creator)
{
	getFilterTable()[classname] = creator;
}

LogFilter::Ptr ThreadFactory::CreateFilter(std::string classname, ConfigOptions options, Base::BaseReceiver::Ptr next)
{
	std::map<std::string, FilterCreator*>::iterator i;
	i = getFilterTable().find(classname);

	if (i != getFilterTable().end())
		return LogFilter::Ptr(i->second->create(next, options));
	else
		throw ThreadFactory::ClassNotFoundException(classname.c_str());
}

std::map<std::string, FilterCreator*>& ThreadFactory::getFilterTable()
{
	static std::map<std::string, FilterCreator*> table;
	return table;
}



void ThreadFactory::registerSource(const std::string& classname, Base::FactoryImpl::SourceCreator* creator)
{
	getSourceTable()[classname] = creator;
}

LogSource::Ptr ThreadFactory::CreateSource(std::string classname, ConfigOptions options, Base::BaseReceiver::Ptr next)
{
	std::map<std::string, SourceCreator*>::iterator i;
	i = getSourceTable().find(classname);

	if (i != getSourceTable().end())
		return LogSource::Ptr(i->second->create(next, options));
	else
		throw ThreadFactory::ClassNotFoundException(classname.c_str());
}

std::map<std::string, SourceCreator*>& ThreadFactory::getSourceTable()
{
	static std::map<std::string, SourceCreator*> table;
	return table;
}

template<class T>
std::vector<std::string> getKeys(std::map<std::string, T*> table)
{
	typedef typename std::map<std::string, T*> table_type;
	std::vector<std::string> keys;
	
	for (typename table_type::iterator it = table.begin(); it != table.end(); it++)
	{
		keys.push_back(it->first);
	}

	return keys;
}

std::vector<std::string> ThreadFactory::GetRegisteredSinks()
{
	return getKeys(getSinkTable());
}
std::vector<std::string> ThreadFactory::GetRegisteredSigners()
{
	return getKeys(getSignerTable());
}
std::vector<std::string> ThreadFactory::GetRegisteredAggregators()
{
	return getKeys(getAggregatorTable());
}
std::vector<std::string> ThreadFactory::GetRegisteredFilters()
{
	return getKeys(getFilterTable());
}
std::vector<std::string> ThreadFactory::GetRegisteredSources()
{
	return getKeys(getSourceTable());
}
