#ifndef PERSISTANT_STORAGE_H
#define PERSISTANT_STORAGE_H

#include "BaseException.h"
#include "../base/BaseReceiver.h"
#include "../../common/sqlite3/sqlite3.h"
#include <boost/shared_ptr.hpp>
#include <fstream>
#include <string>
#include <set>
#include <queue>

namespace Core
{
	class PersistentStorageService : public Base::BaseThread
	{
	public:

        //Get singelton instance
		static PersistentStorageService* getInstance()
		{
			static PersistentStorageService instance;
			return &instance;
		}

        //Get value from storage syncronously
		std::string GetSync(std::string key);
        //Get value from storage syncronously
		int GetSync(std::string key, unsigned char* &ptr);
        //Write value to storage syncronously
		void SetSync(std::string key, std::string data);
        //Write value to storage syncronously
		void SetSync(std::string key, unsigned char* data, int length);
        //Write value to storage, asyncronously, fails silently
		void SetAsync(std::string key, std::string data);
        //Write value to storage, asyncronously, fails silently
		void SetAsync(std::string key, unsigned char* data, int length);
        //Remove a value from storage syncronously
		void DeleteSync(std::string key);

		class IOException : public Core::BaseException
		{
		public:
			IOException(const std::string& msg) :BaseException(msg){};
		};

		//this can only be set _before_ getInstance is called for the first times
		static void SetFilename(std::string);
        //Get the name/path of the storage file
        static std::string GetFilename();


        class __ScopedSqlLock
        {
        public:
            __ScopedSqlLock()
            {
                lock();
            }

            ~__ScopedSqlLock()
            {
                unlock();
            }

            void unlock()
            {
                static PersistentStorageService* pss = getInstance();
                if (isLocked)
                {
                    pss->ReleaseSqlSlot();
                    isLocked = false;
                }
            }

            void lock()
            {
                static PersistentStorageService* pss = getInstance();
                if (!isLocked)
                {
                    pss->WaitForSqlSlot();
                    isLocked = true;
                }
            }
        private:
            bool isLocked = false;
        };
        typedef boost::shared_ptr<__ScopedSqlLock> ScopedSqlLock;

        ScopedSqlLock GetScopedSqlLock()
        {
            return ScopedSqlLock(new __ScopedSqlLock());
        }

        void WaitForSqlSlot();
        void ReleaseSqlSlot();

	protected:
		std::string InstanceName() override
		{
			return "PersistentStorage";
		}

	private:
		PersistentStorageService();
		PersistentStorageService(PersistentStorageService const&) = delete;
		void operator=(PersistentStorageService const&) = delete;
		~PersistentStorageService();

		void init();

		class WriteThis{
		public:
			WriteThis(std::string key, unsigned char* data, int length);
			~WriteThis();
			std::string key;
			unsigned char* data;
			int length;
		};

		typedef boost::shared_ptr<WriteThis> WriteThisPtr;
		typedef std::vector<boost::shared_ptr<WriteThis> > WriteThisPtrArr;

		//needed to remove duplicates
		struct WriteThisCompare
		{
			bool operator()(const WriteThisPtr& a, const WriteThisPtr& b) const;
		};

		void PutOnQueue(WriteThisPtr msg);

		void HandleMessages(WriteThisPtrArr msgs);
		void Set_internal(std::string key, unsigned char* data, int length);

		virtual void Main() override;
		virtual void Stop() override;

		sqlite3_stmt* preparedInsertStmt;
		sqlite3_stmt* preparedSelectStmt;
		sqlite3_stmt* preparedDeleteStmt;
		static std::string filename;
		sqlite3* db;
		bool isdb = false;


		boost::posix_time::ptime lastTick;

		WriteThisPtrArr WaitForMessage();
		int MessageCount();

		bool shouldStop = false;

		//Direct access to queue is forbidden except by 
		// mutex protected member functions
		WriteThisPtrArr queue;
		mutable boost::mutex queueMutex;
		boost::condition_variable queueCondition;
        
        //All sql operations must occur one at a time
        std::queue<boost::shared_ptr<boost::condition_variable>> sqlSlotQueue;
        boost::mutex sqlSlotMutex;
        bool sqlSlotAvailable = true;


		//this is needed by Benchmarking
		friend class Benchmark;
		void DeleteFileAndRestartService(bool startagain=true);
	};
}

#endif
