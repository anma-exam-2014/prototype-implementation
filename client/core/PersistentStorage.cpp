#include "PersistentStorage.h"

using namespace Core;

#include <string.h>
#include <stdio.h>

std::string PersistentStorageService::filename;

PersistentStorageService::WriteThis::WriteThis(std::string key, unsigned char* data, int length)
{
	this->key = key;
	this->data = new unsigned char[length];
	this->length = length;
	memcpy(this->data, data, length);
}

PersistentStorageService::WriteThis::~WriteThis()
{
	delete[] this->data;
}


bool PersistentStorageService::WriteThisCompare::operator()(const PersistentStorageService::WriteThisPtr& a, const PersistentStorageService::WriteThisPtr& b) const {
	return a->key < b->key;
}

PersistentStorageService::PersistentStorageService()
{
    try{
        //auto lock = GetScopedSqlLock();
        //we cannot use GetScopedSqlLock() in construction phase since it calls getInstance
        WaitForSqlSlot();

        //do init
        init();

        //release lock
        ReleaseSqlSlot();
    }
    catch (...)
    {
        ReleaseSqlSlot();
        throw;//rethrow
    }
}

void PersistentStorageService::init()
{
	//Check if library is thread_safe
	if (!sqlite3_threadsafe())
	{
		throw IOException("SQLite library is not compiled with thread safe operation enabled");
	}

    if (sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL) != SQLITE_OK)
	{
		throw IOException(std::string("Can't open log storage: ") + sqlite3_errmsg(db));
    }

    //wait 1 second at a time, but we always keep trying
    sqlite3_busy_timeout(db, 1000);

	char *zErrMsg = 0;
	if (sqlite3_exec(db, "PRAGMA auto_vacuum = FULL;", 0, 0, &zErrMsg) != SQLITE_OK)
	{
		throw IOException(std::string("SQL error: ") + zErrMsg);
	}
	if (sqlite3_exec(db, "VACUUM;", 0, 0, &zErrMsg) != SQLITE_OK)
	{
		throw IOException(std::string("SQL error: ") + zErrMsg);
	}
	const char* query =
		"CREATE TABLE IF NOT EXISTS PERSISTENTDATA("  \
		"KEY TEXT PRIMARY KEY," \
		"DATA BLOB NOT NULL); ";

	if (sqlite3_exec(db, query, 0, 0, &zErrMsg) != SQLITE_OK){
		throw IOException(std::string("SQL error: ") + zErrMsg);
	}

	if (sqlite3_prepare_v2(db, "INSERT OR REPLACE INTO PERSISTENTDATA (KEY, DATA) VALUES (?, ?);", -1, &preparedInsertStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqlite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare_v2(db, "SELECT DATA FROM PERSISTENTDATA WHERE KEY = ? ;", -1, &preparedSelectStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqlite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare_v2(db, "DELETE FROM PERSISTENTDATA WHERE KEY = ?;", -1, &preparedDeleteStmt, 0) != SQLITE_OK)
	{
		throw IOException(std::string("Could not create sqlite3 prepared stmt: ") + sqlite3_errmsg(db));
	}
	isdb = true;
}

class StopException{};

void PersistentStorageService::Main()
{
	try{
		lastTick = boost::get_system_time();
		while (true)
		{
			HandleMessages(WaitForMessage());
		}
	}
	catch (StopException)
	{
		//do nothing just quit the thread
    }
    ReleaseSqlSlot();
}

void PersistentStorageService::Stop()
{
	shouldStop = true;
}

PersistentStorageService::~PersistentStorageService()
{
	if (isdb)
    {
        auto lock = GetScopedSqlLock();
		char *zErrMsg = 0;
		if (sqlite3_exec(db, "COMMIT TRANSACTION", 0, 0, &zErrMsg) != SQLITE_OK){
			//do nothing on error
			//throw IOException(std::string("SQL error: ") + zErrMsg);
		}
		if (sqlite3_finalize(preparedDeleteStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedDeleteStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_finalize(preparedInsertStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedInsertStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_finalize(preparedSelectStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedSelectStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_close(db) != SQLITE_OK)
		{
			throw IOException(std::string("Close database failed: ") + sqlite3_errmsg(db));
		}
		isdb = false;
	}
}

void PersistentStorageService::PutOnQueue(PersistentStorageService::WriteThisPtr msg)
{
    boost::mutex::scoped_lock lock(queueMutex);
	bool const was_empty = queue.empty();
	queue.push_back(msg);

	lock.unlock(); // unlock the mutex BEFORE notify_one()

	if (was_empty)
	{
        queueCondition.notify_one();
	}
}

void PersistentStorageService::HandleMessages(WriteThisPtrArr msgs)
{
	typedef std::set<WriteThisPtr, WriteThisCompare> toWriteType;
	toWriteType toWrite;
	BOOST_REVERSE_FOREACH(WriteThisPtr msg, msgs)
	{
		//This will remove any duplicates, the reverse foreach loop ensures
		//that only the newest values are inserted
		toWrite.insert(msg);
	}

	BOOST_FOREACH(WriteThisPtr msg, toWrite)
	{
		Set_internal(msg->key, msg->data, msg->length);
	}
}

void PersistentStorageService::WaitForSqlSlot()
{
    boost::mutex::scoped_lock lock(sqlSlotMutex);
    if (sqlSlotAvailable)
    {
        sqlSlotAvailable = false;
    }
    else
    {
        boost::shared_ptr<boost::condition_variable> cond(new boost::condition_variable());
        sqlSlotQueue.push(cond);
        cond->wait(lock);
        sqlSlotQueue.pop();
    }
}

void PersistentStorageService::ReleaseSqlSlot()
{
    boost::mutex::scoped_lock lock(sqlSlotMutex);
    if (sqlSlotQueue.size() == 0)
    {
        sqlSlotAvailable = true;
    }
    else
    {
        sqlSlotQueue.front()->notify_one();
    }
}

PersistentStorageService::WriteThisPtrArr PersistentStorageService::WaitForMessage()
{
    ReleaseSqlSlot();
	boost::mutex::scoped_lock lock(queueMutex);
	while (queue.empty())
	{
		if (shouldStop)
			throw StopException();
		boost::system_time timeout = lastTick + boost::posix_time::milliseconds(100);
		queueCondition.timed_wait(lock, timeout);
		auto currentTime = boost::get_system_time();
		lastTick = currentTime;
	}

    //allow new items to the queue while we wait
    lock.unlock();
    //wait for an availabel time slot
    WaitForSqlSlot();
    //lock the queue again while copy it
    lock.lock();

	WriteThisPtrArr copy(queue.begin(), queue.end());
	queue.clear();
	return copy;
}

int PersistentStorageService::GetSync(std::string key, unsigned char* &ptr)
{
    auto lock = GetScopedSqlLock();
	int bytes = 0;

	// Bind parameter
	if (sqlite3_bind_text(preparedSelectStmt, 1, key.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}

	//Perform query
	while (true)
	{
		int s = sqlite3_step(preparedSelectStmt);
		if (s == SQLITE_ROW)
		{
			unsigned char* tmpptr = (unsigned char*)sqlite3_column_blob(preparedSelectStmt, 0);
			bytes = sqlite3_column_bytes(preparedSelectStmt, 0);
			ptr = new unsigned char[bytes];
			memcpy(ptr, tmpptr, bytes);
			memset(tmpptr, 0, bytes);
		}
		else if (s == SQLITE_DONE)
		{
			break;
		}
        else if (s == SQLITE_BUSY)
        {
            //database is locked by thread/process, let's try again
            //Reset statement
            if (sqlite3_reset(preparedSelectStmt) != SQLITE_OK)
            {
                throw IOException(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
            }

            //do it!
            return GetSync(key, ptr);
        }
		else
		{
			throw IOException(std::string("Get log message error: ") + sqlite3_errmsg(db));
		}
	}

	//Reset statement
	if (sqlite3_reset(preparedSelectStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}

	return bytes;
}

std::string PersistentStorageService::GetSync(std::string key)
{
	unsigned char* tmp = NULL;
	int len = GetSync(key, tmp);
	return std::string(reinterpret_cast<const char*>(tmp), len);
}


void PersistentStorageService::SetSync(std::string key, std::string data)
{
    auto lock = GetScopedSqlLock();
	Set_internal(key, (unsigned char*)data.data(), data.length());
}

void PersistentStorageService::SetSync(std::string key, unsigned char* data, int length)
{
    auto lock = GetScopedSqlLock();
	Set_internal(key, data, length);
}


void PersistentStorageService::SetAsync(std::string key, std::string data)
{
	WriteThisPtr ptr(new WriteThis(key, (unsigned char*)data.data(), data.length()));
	PutOnQueue(ptr);
}

void PersistentStorageService::SetAsync(std::string key, unsigned char* data, int length)
{
	WriteThisPtr ptr(new WriteThis(key, data, length));
	PutOnQueue(ptr);
}


void PersistentStorageService::Set_internal(std::string key, unsigned char* data, int length)
{
	//Bind parameters
	if (sqlite3_bind_text(preparedInsertStmt, 1, key.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}
	if (sqlite3_bind_blob(preparedInsertStmt, 2, data, length, SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}

	//Perform query
    int result;
	while ((result = sqlite3_step(preparedInsertStmt)) != SQLITE_DONE)
	{
        if (result != SQLITE_BUSY)
        {
		    throw IOException(std::string("Write log message error: execute sqllite step error: ") + sqlite3_errmsg(db));
        }
    }

	//Reset statement
	if (sqlite3_reset(preparedInsertStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}
}

void PersistentStorageService::DeleteSync(std::string key)
{
    auto lock = GetScopedSqlLock();
	//Bind parameters
	if (sqlite3_bind_text(preparedDeleteStmt, 1, key.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw IOException(std::string("Write log message error: could not bind parameter: ") + sqlite3_errmsg(db));
	}

	//Perform query
    int result;
	if ((result = sqlite3_step(preparedDeleteStmt)) != SQLITE_DONE)
	{
        if (result == SQLITE_BUSY)
        {
            //database is locked by other thread/process, let's try again
            //Reset statement
            if (sqlite3_reset(preparedDeleteStmt) != SQLITE_OK)
            {
                throw IOException(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
            }

            //do it!
            DeleteSync(key);
            return;
        }
        else
        {
            throw IOException(std::string("Delete log message error: execute sqllite step error: ") + sqlite3_errmsg(db));
        }
    }

	//Reset statement
	if (sqlite3_reset(preparedDeleteStmt) != SQLITE_OK)
	{
		throw IOException(std::string("Delete log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}
}

void PersistentStorageService::SetFilename(std::string s)
{
	filename = s;
}

std::string PersistentStorageService::GetFilename()
{
    return filename;
}

void PersistentStorageService::DeleteFileAndRestartService(bool startagain)
{
    auto lock = GetScopedSqlLock();
	if (isdb)
	{
		if (sqlite3_finalize(preparedDeleteStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedDeleteStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_finalize(preparedInsertStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedInsertStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_finalize(preparedSelectStmt) != SQLITE_OK)
		{
			throw IOException(std::string("Close preparedSelectStmt failed: ") + sqlite3_errmsg(db));
		}
		if (sqlite3_close(db) != SQLITE_OK)
		{
			throw IOException(std::string("Close database failed: ") + sqlite3_errmsg(db));
		}
		isdb = false;
	}
	if (remove(filename.c_str()) != 0)
		throw std::runtime_error("Error deleting persitent storage file");

	if (startagain)
	{
		//start again
		init();
	}
}