#ifndef BASE_EXCEPTION_H
#define BASE_EXCEPTION_H

#include <stdexcept>

namespace Core
{
	class BaseException : public std::runtime_error
	{
	public:
		BaseException(const char* msg)
		: std::runtime_error(msg)
		{}

		BaseException(const std::string& msg)
		: std::runtime_error(msg)
		{}
	};
}

#endif
