#ifndef BENCHMARK_H
#define BENCHMARK_H

#include "ConfigParser.h"
#include "ThreadFactory.h"

//pretty print tables
#include <bprinter/table_printer.h>

#include <vector>
#include <map>
#include <boost/scoped_ptr.hpp>

namespace Core
{
	namespace Internal{
		typedef std::vector<std::string> SignArr;
	}

	class Benchmark
	{
	public:
		Benchmark(int argc, char* argv[]);
		~Benchmark();

		bool shouldRun()
		{
			return mShouldRun;
		}

		int run();

	private:
		//some usefull typdefs
		typedef Common::LogMessage::Ptr Message_p;
		typedef Base::BaseReceiver::MsgPtrArr Batch_t;
		typedef std::map<std::string, double> Time_t;
		typedef std::map<std::string, Time_t> TimeCollection_t;
		typedef std::vector<TimeCollection_t> Results_t;

		Message_p PrepareMessage();
		Batch_t PrepareBatch();

		std::string getParameters();
		std::string calculateAnalysis(Results_t results);

		TimeCollection_t doRun(bprinter::TablePrinter& tPrinter, Base::LogSink::Ptr sink, int runNr);

		//Some options
		bool mShouldRun;
		bool mVerbose;
		std::string mOutput;
		Internal::SignArr mSigners;
		int mRuns;
		int mBatches;
		int mMessages;
		std::string mMessageContent;
		std::map<std::string, ConfigOptions> mSignerOptions;
		bool mDeletePersistentStorage;
	};


}


#endif