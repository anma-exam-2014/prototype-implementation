#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H

#include <string>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree.hpp>
#include "BaseException.h"


namespace Core
{
	typedef std::map<std::string, std::string> ConfigOptions;

	class ConfigParser
	{
	private:
		void ParseXML(std::string fileName);
	public:
		typedef boost::shared_ptr<ConfigParser> Ptr;

		ConfigParser(std::string fileName);
		~ConfigParser();

		std::string GetSinkName();
		ConfigOptions GetSinkOptions();

		std::string GetSignerName();
		ConfigOptions GetSignerOptions();

		std::string GetAggregatorName();
		ConfigOptions GetAggregatorOptions();

		ConfigOptions GetOptions();

		int GetNumberOfSources();
		std::string GetSourceName(int sourceId);
		ConfigOptions GetSourceOptions(int sourceId);

		int GetNumberOfFilters(int sourceId);
		std::string GetFilterName(int sourceId, int filterId);
		ConfigOptions GetFilterOptions(int sourceId, int filterId);

		static void PrintExampleConfig();

		class FileNotFoundException : public BaseException
		{
		public:
			FileNotFoundException(const char* const& msg) :BaseException(msg){};
		};

		class ParserException : public BaseException
		{
		public:
			ParserException(const char* const& msg) :BaseException(msg){};
		};
		
	private:
		std::string filename;

		struct Component
		{
			std::string name;
			std::map<std::string, std::string> options;
		};

		struct Source
		{
			Component source;
			std::vector<boost::shared_ptr<Component> > filters;
		};

		struct Configuration
		{
			std::vector<boost::shared_ptr<Source> > sources;
			boost::shared_ptr<Component> aggregator;
			boost::shared_ptr<Component> signer;
			boost::shared_ptr<Component> sink;
		};

		Configuration config;
		ConfigOptions options;
		boost::shared_ptr<Component> ParseComponent(std::string elementName, boost::property_tree::ptree parent);
		boost::shared_ptr<Source> ParseSource(boost::property_tree::ptree element);
		std::vector<boost::shared_ptr<Source> > ParseSources(boost::property_tree::ptree root);
		boost::shared_ptr<Component> ParseFilter(boost::property_tree::ptree element);
		Component ParseFilter();
		void ParseOptions(std::string elementName, boost::property_tree::ptree parent);
	};
}

#endif
