#include "ConfigParser.h"
#include <fstream>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <iostream>

#if 0
bool debug = true;
#else
bool debug = false;
#endif

#define DEBUG_PRINT(x) do { if(debug) { std::cerr << x << std::endl; } } while(0)

using namespace Core;
using boost::property_tree::ptree;

class MissingClassException : public BaseException
{
public:
	MissingClassException(const char* const& msg)
		:BaseException(msg){};
};

class UnknownXmlElementException : public BaseException
{
public:
	UnknownXmlElementException(const char* const& msg) 
		:BaseException(msg){};
};

ConfigParser::ConfigParser(std::string fileName)
{
	try
	{
		ParseXML(fileName);
	}
	catch (MissingClassException &e)
	{
		throw ParserException((std::string("ERROR: A ") + e.what() + " element does not specify any class.").c_str());
	}
	catch (UnknownXmlElementException &e)
	{
		throw ParserException((std::string("ERROR: ") + e.what() + " is an unknown XML element.").c_str());
	}
	catch (boost::property_tree::xml_parser_error &e)
	{
		throw FileNotFoundException((std::string("ERROR: Error reading config file: ") + e.what()).c_str());
	}
	catch (boost::property_tree::ptree_error)
	{
		throw ParserException("ERROR: Missing XML element.");
	}
	catch (...)
	{
		throw ParserException("ERROR: An unknown error occured.");
	}
}
ConfigParser::~ConfigParser()
{

}

void ConfigParser::PrintExampleConfig()
{
	std::cout << "<?xml version=\"1.0\" encoding=\"utf - 8\"?>" << std::endl;
	std::cout << "<sources>" << std::endl;
	std::cout << "\t<!-- An arbitrary amount of sources can be specified (at least 1) -->" << std::endl;
	std::cout << "\t<source class=\"SourceImplementationClass\" option1=\"x\" option2=\"y\">" << std::endl;
	std::cout << "\t\t<!-- An arbitrary amount of filters can be specified for each source (note that specified order matters) -->" << std::endl;
	std::cout << "\t\t<filter class=\"FilterImplementationClass\" option1=\"z\" />" << std::endl;
	std::cout << "\t\t<filter class=\"FilterImplementationClass\"/>" << std::endl;
	std::cout << "\t</source>" << std::endl;
	std::cout << "</sources>" << std::endl << std::endl;
	std::cout << "<!-- One and only one aggregator element must be specified. -->" << std::endl;
	std::cout << "<aggregator class=\"AggregatorImplementationClass\"/>" << std::endl << std::endl;
	std::cout << "<!-- One and only one signer element must be specified. -->" << std::endl;
	std::cout << "<signer class=\"SignerImplementationClass\"/>" << std::endl << std::endl;
	std::cout << "<!-- One and only one sink element must be specified. -->" << std::endl;
	std::cout << "<sink class=\"SinkImplementationClass\"/>" << std::endl << std::endl;
}

void ConfigParser::ParseXML(std::string fileName)
{
	//Load xml structure into a tree
	ptree root;
	read_xml(fileName, root);
	root = root.get_child("config");
	
	//Parse required elements
	config.sources = ParseSources(root);
	config.aggregator = ParseComponent("aggregator", root);
	config.signer = ParseComponent("signer", root);
	config.sink = ParseComponent("sink", root);
	
	ParseOptions("options", root);
}

std::vector<boost::shared_ptr<ConfigParser::Source> > ConfigParser::ParseSources(ptree root)
{
	ptree sourceTree = root.get_child("sources");
	std::vector<boost::shared_ptr<ConfigParser::Source> > sources;

	for (auto i = sourceTree.begin(); i != sourceTree.end(); i++)
	{
		if (i->first == "source")
		{
			DEBUG_PRINT("source element");
			boost::shared_ptr<Source> source = ParseSource(i->second);
			sources.push_back(source);
		}
		else if (i->first == "<xmlcomment>")
		{
			//Skip this
		}
		else
		{
			throw UnknownXmlElementException(i->first.c_str());
		}
	}

	return sources;
}

boost::shared_ptr<ConfigParser::Source> ConfigParser::ParseSource(ptree element)
{
	boost::shared_ptr<Source> source(new Source());
	
	//Parse source attributes
	auto attributes = element.get_child("<xmlattr>");
	for (auto i = attributes.begin(); i != attributes.end(); i++)
	{
		DEBUG_PRINT("\t" << i->first << "=\"" << i->second.data() << "\"");
		if (i->first == "class")
		{
			source->source.name = i->second.data();
		}
		else
		{
			source->source.options[i->first] = i->second.data();
		}
	}

	if (source->source.name.empty())
	{
		throw MissingClassException("<source>");
	}

	//Parse filters
	auto i = element.begin();
	i++;
	for (; i != element.end(); i++)
	{
		if (i->first == "filter")
		{
			DEBUG_PRINT("filter element");
			boost::shared_ptr<Component> filter = ParseFilter(i->second);
			source->filters.push_back(filter);
		}
		else if (i->first == "<xmlcomment>")
		{
			//Skip this
		}
		else
		{
			throw UnknownXmlElementException(i->first.c_str());
		}
	}

	return source;
}

boost::shared_ptr<ConfigParser::Component> ConfigParser::ParseFilter(ptree element)
{
	boost::shared_ptr<Component> filter(new Component());

	//Parse filter attributes
	auto attributes = element.get_child("<xmlattr>");
	for (auto i = attributes.begin(); i != attributes.end(); i++)
	{
		DEBUG_PRINT("\t" << i->first << "=\"" << i->second.data() << "\"");
		if (i->first == "class")
		{
			filter->name = i->second.data();
		}
		else
		{
			filter->options[i->first] = i->second.data();
		}
	}

	if (filter->name.empty())
	{
		throw MissingClassException("<filter>");
	}

	return filter;
}

boost::shared_ptr<ConfigParser::Component> ConfigParser::ParseComponent(std::string elementName, ptree parent)
{
	boost::shared_ptr<Component> component(new Component());
	ptree element = parent.get_child(elementName);

	DEBUG_PRINT(elementName << " element");
	auto attributes = element.get_child("<xmlattr>");
	for (auto i = attributes.begin(); i != attributes.end(); i++)
	{
		DEBUG_PRINT("\t" << i->first << "=\"" << i->second.data() << "\"");
		if (i->first == "class")
		{
			component->name = i->second.data();
		}
		else
		{
			component->options[i->first] = i->second.data();
		}
	}

	if (component->name.empty())
	{
		throw MissingClassException(("<" + elementName +">").c_str());
	}

	return component;
}
void ConfigParser::ParseOptions(std::string elementName, boost::property_tree::ptree parent)
{
	try
	{
		ptree element = parent.get_child(elementName);
		DEBUG_PRINT(elementName << " element");
		auto attributes = element.get_child("<xmlattr>");
		for (auto i = attributes.begin(); i != attributes.end(); i++)
		{
			DEBUG_PRINT("\t" << i->first << "=\"" << i->second.data() << "\"");
			options[i->first] = i->second.data();
		}
	}
	catch (...)
	{
		//Nothing
	}
	
}


std::string ConfigParser::GetSinkName()
{
	return config.sink->name;
}

ConfigOptions ConfigParser::GetSinkOptions()
{
	return config.sink->options;
}

std::string ConfigParser::GetSignerName()
{
	return config.signer->name;
}

ConfigOptions ConfigParser::GetSignerOptions()
{
	return config.signer->options;
}

std::string ConfigParser::GetAggregatorName()
{
	return config.aggregator->name;
}

ConfigOptions ConfigParser::GetAggregatorOptions()
{
	return config.aggregator->options;
}

ConfigOptions ConfigParser::GetOptions()
{
	return options;
}

int ConfigParser::GetNumberOfSources()
{
	return config.sources.size();
}

std::string ConfigParser::GetSourceName(int sourceId)
{
	return config.sources.at(sourceId)->source.name;
}

ConfigOptions ConfigParser::GetSourceOptions(int sourceId)
{
	return config.sources.at(sourceId)->source.options;
}

int ConfigParser::GetNumberOfFilters(int sourceId)
{
	return config.sources.at(sourceId)->filters.size();
}

std::string ConfigParser::GetFilterName(int sourceId, int filterId)
{
	return config.sources.at(sourceId)->filters.at(filterId)->name;
}

ConfigOptions ConfigParser::GetFilterOptions(int sourceId, int filterId)
{
	return config.sources.at(sourceId)->filters.at(filterId)->options;
}
