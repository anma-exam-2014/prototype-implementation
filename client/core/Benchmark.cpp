#include "Benchmark.h"

#include "PersistentStorage.h"

#include <iostream>
#include <fstream>
#include <omp.h>
#include <boost/program_options.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/predicate.hpp>

using namespace Core;
using namespace Common;
using namespace std;

#define MESSAGE_MAX 200
#define MESSAGE_MIN 50

string availableSignerString()
{
	auto signers = ThreadFactory::GetRegisteredSigners();
	string all;
	BOOST_FOREACH(string signer, signers)
	{
		all += signer + ", ";
	}

	//remove last ", "
	return all.substr(0, all.length() - 2);
}

boost::program_options::options_description buildPrgOptions()
{
	boost::program_options::options_description desc("Benchmarking options");
	auto callMeToAddAnOption = desc.add_options()
		("help", "Produce this help message.")
		("benchmark", "Activates benchmarking mode, may be used together with \"--help\" to show all available benchmarking commands.")
		("verbose", "Enable verbose output.")
		("output", boost::program_options::value<std::string>()->default_value("-"), "Print results to a file. If '-' is specified the result is printed to stdout.")
		("signer", boost::program_options::value<Internal::SignArr>(), ("Set signer to benchmark (sepparated by comma (,)), available values: " + availableSignerString() + ". This option can be specified multiple times to benchmark more than one signer at a time.").c_str())
		("runs", boost::program_options::value<int>()->default_value(1), "Set the number of times each benchmark is run for this session.")
		("batches", boost::program_options::value<int>()->default_value(100), "Set the number of batches in each run.")
		("messages", boost::program_options::value<int>()->default_value(100), "Set the number of log meassages in each batch.")
		("message-content", boost::program_options::value<std::string>()->default_value("random"), "Set the content of each log message, available values: random|<string>.")
		("persistent-storage", boost::program_options::value<std::string>()->default_value("test.dat"), "Set the filename that is to be used as persistent storage between runs.")
		("delete-persistent-storage", "Deletes the persitent storage after each run.")
		;

	auto signers = ThreadFactory::GetRegisteredSigners();
	BOOST_FOREACH(string signer, signers)
	{
		callMeToAddAnOption(
			(signer + "-<option>").c_str(), 
			boost::program_options::value<string>(), 
			("Any specific option for " + signer + " is specified in this manner.").c_str()
		);
	}

	return desc;
}

template<typename T>
T GetAs(boost::program_options::variables_map vm, string key)
{
	auto it = vm.find(key);
	if (it == vm.end())
		throw std::runtime_error("Required parameter \"" + key + "\" is missing!");
	try{
		return it->second.as<T>();
	}
	catch (boost::bad_any_cast)
	{
		throw std::runtime_error("Could not convert value of \"" + key + "\" to " + typeid(T).name() + "!");
	}
}

typedef std::vector< boost::program_options::basic_option<char> > vecBasicOpt;

ConfigOptions GetOptions(vecBasicOpt vm, string substring)
{
	vecBasicOpt::const_iterator it = vm.begin();
	vecBasicOpt::const_iterator end = vm.end();
	ConfigOptions result;

	for (; it != end; ++it)
	{
		string key = it->string_key;
		if (boost::starts_with(key, substring))
		{
			key.erase(0, substring.length());
			result[key] = it->value[0]; //There will only ever be one value
		}
	}

	return result;
}

Benchmark::Benchmark(int argc, char* argv[])
{	
	//default is to not run benchmarking
	mShouldRun = false;

	// defines comand line arguments
	boost::program_options::options_description desc = buildPrgOptions();

	// read command line parameters
	boost::program_options::variables_map vm;
	boost::program_options::command_line_parser parser(argc, argv);
	boost::program_options::parsed_options parsed = parser.options(desc).allow_unregistered().run();
	boost::program_options::store(parsed, vm);
	boost::program_options::notify(vm);

	if (!vm.count("benchmark"))
	{
		//no benchmarking, return to main program!
		return;
	}

	//Okay, let's do some benchmarking
	mShouldRun = true;

	// check command line parameters
	if (vm.count("help"))
	{
		cout << desc;
		mRuns = 0;
		return;
	}

	//read in some options
	mVerbose = vm.count("verbose") != 0;
	mOutput = GetAs<string>(vm, "output");
	mSigners = GetAs<Internal::SignArr>(vm, "signer");
	mRuns = GetAs<int>(vm, "runs");
	mBatches = GetAs<int>(vm, "batches");
	mMessages = GetAs<int>(vm, "messages");
	mMessageContent = GetAs<string>(vm, "message-content");
	PersistentStorageService::SetFilename(GetAs<string>(vm, "persistent-storage"));
	mDeletePersistentStorage = vm.count("delete-persistent-storage") != 0;

	//signer specific options
	BOOST_FOREACH(string signer, mSigners)
	{
		mSignerOptions[signer] = GetOptions(parsed.options, signer + "-");
	}
}

Benchmark::~Benchmark()
{
}

int Benchmark::run()
{
	if (!mRuns)
		return 0;

	if (mVerbose) cout << "Initializing Benchmarker ... " << std::flush;

	stringstream table;

	table << "-----BEGIN INPUT-----" << endl;
	table << getParameters();
	table << "-----END INPUT-----" << endl;
	table << "-----BEGIN RESULTS-----" << endl;

	//set up table_printer with headers
	bprinter::TablePrinter tPrinter(&table);

	tPrinter.AddColumn("Phase", 12);
	BOOST_FOREACH(string signerName, mSigners)
	{
		tPrinter.AddColumn(signerName, 30);
	}
	
	//start persistentstorage service
	PersistentStorageService::getInstance()->Start();

	//DummySink used as a null device endpoint
	ConfigOptions dummyOpts;
	dummyOpts["verbose"] = "false";
	Base::LogSink::Ptr sink = ThreadFactory::CreateSink("DummySink", dummyOpts);
	sink->Start();

	//print table header!
	tPrinter.PrintHeader();

	if (mDeletePersistentStorage)
		PersistentStorageService::getInstance()->DeleteFileAndRestartService();

	Results_t results;

	if (mVerbose) cout << "Done!" << endl;
	try{
		//begin benchmarking!
		for (int runNr = 0; runNr < mRuns; runNr++)
		{
			results.push_back(doRun(tPrinter, sink, runNr + 1));
			if (runNr + 1 < mRuns)
			{
				//Add as a sepparator
				tPrinter.PrintFooter();
			}
		}
		if (mVerbose) cout << "Benchmarking complete!" << endl;
	}
	catch (...)
	{
		//stop other running threads
		sink->StopJoin();
		PersistentStorageService::getInstance()->Stop();
		if (mDeletePersistentStorage)
			PersistentStorageService::getInstance()->DeleteFileAndRestartService(false);

		//rethrow to notify user
		throw;
	}

	//print table footer
	tPrinter.PrintFooter();
	table << "-----END RESULTS-----" << endl;
	table << "-----BEGIN ANALYSIS-----" << endl;
	table << calculateAnalysis(results);
	table << "-----END ANALYSIS-----" << endl;

	sink->StopJoin();

	//stop persistentstorage service
	PersistentStorageService::getInstance()->Stop();
	if (mDeletePersistentStorage)
		PersistentStorageService::getInstance()->DeleteFileAndRestartService(false);

	//alright lets output our results!
	if (mOutput != "-" && !mOutput.empty())
	{
		ofstream file(mOutput, std::ofstream::out);
		file << table.str();
		file.close();
		cout << "Results written to file: " << mOutput << endl;
	}
	else
	{
		cout << table.str();
	}

	return 0;
}

//Returns the time of each phase for each signer
Benchmark::TimeCollection_t Benchmark::doRun(bprinter::TablePrinter& tPrinter, Base::LogSink::Ptr sink, int runNr)
{
	//place to store results
	TimeCollection_t results;

	if (mVerbose) cout << "Beginning run " << runNr << ":" << endl;
	Batch_t logBatch = PrepareBatch();
	vector<Base::LogSigner::Ptr> signers;

	results["Setup"] = Time_t();
	tPrinter << "Setup-" + boost::lexical_cast<string>(runNr);
	if (mVerbose) cout << "  Setup phase:" << endl;
	BOOST_FOREACH(string signerName, mSigners)
	{
		if (mVerbose) cout << "    Creating " << signerName << " ... " << std::flush;
		double start = omp_get_wtime();
		signers.push_back(ThreadFactory::CreateSigner(signerName, mSignerOptions[signerName], sink));
		double end = omp_get_wtime();
		string time = boost::lexical_cast<string>(end - start) + " s";
		tPrinter << time;
		if (mVerbose) cout << "Done!" << endl;

		//Store results
		results["Setup"][signerName] = end - start;
	}

	results["Signing"] = Time_t();
	tPrinter << "Signing-" + boost::lexical_cast<string>(runNr);
	if (mVerbose) cout << "  Signing phase:" << endl;
	int j = 0;
	BOOST_FOREACH(Base::LogSigner::Ptr signer, signers)
	{
		string signerName = mSigners[j++];
		if (mVerbose) cout << "    Signing with " << signerName << " ... " << std::flush;
		//start clock
		double start = omp_get_wtime();
		for (int batchNr = 0; batchNr < mBatches; batchNr++)
		{
			signer->HandleMessages(logBatch);
		}
		//show handle message time
		double end = omp_get_wtime();
		string time = boost::lexical_cast<string>(end - start) + " s";
		tPrinter << time;
		if (mVerbose) cout << "Done!" << endl;

		//Store results
		results["Signing"][signerName] = end - start;
	}

	results["KeyUpdate"] = Time_t();
	tPrinter << "KeyUpdate-" + boost::lexical_cast<string>(runNr);
	if (mVerbose) cout << "  KeyUpdate phase:" << endl;
	j = 0;
	BOOST_FOREACH(Base::LogSigner::Ptr signer, signers)
	{
		string signerName = mSigners[j++];
		if (mVerbose) cout << "    KeyUpdate with " << signerName << " ... " << std::flush;
		//start clock
		double start = omp_get_wtime();

		bool ret = signer->BenchmarkUpdateKey();

		//show handle message time
		double end = omp_get_wtime();
		if (ret)
		{
			string time = boost::lexical_cast<string>(end - start) + " s";
			tPrinter << time;

			//Store results
			results["KeyUpdate"][signerName] = end - start;
		}
		else
		{
			//Store results
			results["KeyUpdate"][signerName] = 0;
			tPrinter << "N/A";
		}
		if (mVerbose) cout << "Done!" << endl;
	}

	if (mDeletePersistentStorage)
		PersistentStorageService::getInstance()->DeleteFileAndRestartService();

	return results;
}

string Benchmark::getParameters()
{
	stringstream ss;
	ss << "Number of runs in session: " << mRuns << endl;
	ss << "Number of batches in each run: " << mBatches << endl;
	ss << "Number of messages in each batch: " << mMessages << endl;
	ss << "Message content paramter: " << mMessageContent << endl;

	BOOST_FOREACH(string signerName, mSigners)
	{
		string options;
		options += "Options for " + signerName + ": ";
		if (mSignerOptions[signerName].size() > 0)
		{
			typedef pair<string, string> key_value;
			BOOST_FOREACH(key_value p, mSignerOptions[signerName])
			{
				options + p.first + "=\"" + p.second + "\", ";
			}
			options = options.substr(0, options.length() - 2);
		}
		ss << options << endl;
	}

	return ss.str();
}

string Benchmark::calculateAnalysis(Results_t results)
{
	typedef pair<string, Time_t> level1;
	typedef pair<string, double> level2;

	TimeCollection_t totalTime;
	stringstream table;
	//set up table_printer with headers
	bprinter::TablePrinter tPrinter(&table);

	tPrinter.AddColumn("", 30);
	BOOST_FOREACH(string signerName, mSigners)
	{
		tPrinter.AddColumn(signerName, 30);
	}
	tPrinter.PrintHeader();

	//calculate total time
	BOOST_FOREACH(TimeCollection_t run, results)
	{
		BOOST_FOREACH(level1 phase, run)
		{
			BOOST_FOREACH(level2 signer, phase.second)
			{
				totalTime[phase.first][signer.first] += signer.second;
			}
		}
	}

	//calculate average times
	BOOST_FOREACH(level1 phase, totalTime)
	{
		tPrinter << phase.first + " average:";
		BOOST_FOREACH(string signerName, mSigners)
		{
			double time = phase.second[signerName];
			tPrinter << boost::lexical_cast<string>((time/ mRuns)) + " s";
		}
	}

	//calculate signing throughput
	tPrinter << "Throughput:";
	BOOST_FOREACH(string signerName, mSigners)
	{
		double time = totalTime["Signing"][signerName];
		tPrinter << boost::lexical_cast<string>(((mBatches * mMessages) / (time / mRuns))) + " 1/s";
	}

	//calculate average signing latency
	tPrinter << "Sign 1 message:";
	BOOST_FOREACH(string signerName, mSigners)
	{
		double time = totalTime["Signing"][signerName];
		tPrinter << boost::lexical_cast<string>((time / mRuns) / (mBatches * mMessages)) + " s";
	}

	tPrinter.PrintFooter();

	return table.str();
}


static const char alphanum[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

int stringLength = sizeof(alphanum)-1;

char getRandomChar()
{
	return alphanum[rand() % stringLength];
}

Benchmark::Message_p Benchmark::PrepareMessage()
{
	//create message
	Message_p message;
	message.reset(
		new LogMessage(
		LogMessage::FacilityNumber::FN_LOCAL7,
		LogMessage::SeverityValue::Debug
		)
		);

	//set timestamp
	boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
	uint64_t millis = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
	message->SetTimestamp(millis);

	//set content
	if (mMessageContent == "random")
	{
		int length = rand() % (MESSAGE_MAX-MESSAGE_MIN) + MESSAGE_MIN;
		string content;
		for (int i = 0; i < length; i++)
		{
			content += getRandomChar();
		}
	}
	else
	{
		message->SetContent(mMessageContent);
	}
	return message;
}
Benchmark::Batch_t Benchmark::PrepareBatch()
{
	Batch_t messages;
	for (int i = 0; i < mMessages; i++)
	{
		messages.push_back(PrepareMessage());
	}
	return messages;
}
