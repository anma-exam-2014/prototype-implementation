#ifdef _WIN32
#include <windows.h>
#include <tchar.h>
#else
#include <sys/types.h>
#include <unistd.h>
//we need this for std::this_thread::sleep_for
#define _GLIBCXX_USE_NANOSLEEP
#endif

#include <string>
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <csignal>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/foreach.hpp>
#include "ConfigParser.h"
#include "ThreadFactory.h"
#include "BaseException.h"
#include "PersistentStorage.h"
#include "Benchmark.h"

using namespace std;
using namespace Core;
using namespace Base;

boost::shared_ptr<ConfigParser> parseCommandLineAndCreateConfigParser(int argc, char* argv[]);
vector<BaseThread::Ptr> createAllThreads(ConfigParser::Ptr Config);
void JoinAllThreads(vector<BaseThread::Ptr> threadList);

bool isStopSignalRecieved();
void subscribeStopSignals();
void SendControlC(int pid);

int readPid();
void writePid();
void removePid();

std::function<void(void)> niceShutdownHandler = [](){};

class QuitQuietly : public BaseException
{
public:
	QuitQuietly(int code)
		: BaseException("")
	{ returnCode = code; }
	int returnCode;
};
class MainOperationException : public BaseException
{
public:
	MainOperationException(const char* const & msg)
		: BaseException(msg)
	{  }
};

int main(int argc, char* argv[])
{
	try
	{
		///////////////////
		//RUN BENCHMARKS?//
		///////////////////
		{
			Benchmark benchmarker(argc, argv);
			if (benchmarker.shouldRun())
				return benchmarker.run();
		}
		//////////////////
		//Run Client.exe//
		//////////////////
		auto Config = parseCommandLineAndCreateConfigParser(argc, argv);
		if (Config->GetOptions().count("persistent-storage-path") != 0)
		{
			PersistentStorageService::SetFilename(Config->GetOptions()["persistent-storage-path"]);
		}
		else
		{
			PersistentStorageService::SetFilename("persistentstorage");
		}

		//subscribe to SIGINT & SIGKILL
		subscribeStopSignals();

		//create a thread for each component in the
		// logging pipeline
		vector<BaseThread::Ptr> threadList = createAllThreads(Config);

		//all threads are now created, install niceShutdownHandler
		niceShutdownHandler = [threadList]()
		{
			cout << "STOP SIGNAL RECEIVED! WAITING FOR ALL THREADS TO EXIT!" << endl;
			JoinAllThreads(threadList);
			removePid();
		};

		//lets start main loop
		std::chrono::milliseconds loopPauseDuration(100);
		bool contd = true;
		int returnCode = 0;
		while (contd)
		{
			//pause
			std::this_thread::sleep_for(loopPauseDuration);

			//check for error on any thread
			BOOST_FOREACH(BaseThread::Ptr thread, threadList)
			{
				if (thread->GetStatus() == BaseThread::STOPPED_ERROR)
				{
					cout << "FATAL ERROR IN PIPELINE: " << thread->GetException().what() << endl;
					cout << "APPLICATION WILL NOW EXIT" << endl;
					contd = false;
					returnCode = -1;
					break;
				}
			}
		}

		JoinAllThreads(threadList);

		return returnCode;
	}
	catch (ConfigParser::FileNotFoundException &e)
	{
		cout << e.what() << endl;
	}
	catch (ConfigParser::ParserException &e)
	{
		cout << e.what() << endl;
	}
	catch (QuitQuietly &e){
		//do nothing, just quit quietly
		return e.returnCode;
	}
	catch (ThreadFactory::ClassNotFoundException& e)
	{
		cout << "Class " << e.what() << " not found!" << endl;
		cout << "Check that your configuration is correct." << endl;
	}
	catch (std::exception &e)
	{
		cout << "Error: " << e.what() << endl;
		cout << "Program will now exit" << endl;
	}
	catch (...)
	{
		cout << "Unknown error occured, program will now exit" << endl;
	}
	return -1; //error occured
}

//global variable
std::string pidfile;

boost::shared_ptr<ConfigParser> parseCommandLineAndCreateConfigParser(int argc, char* argv[])
{
	// defines comand line arguments
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
		("help", "Produce this help message.")
		("benchmark", "Activates benchmarking mode, may be used together with \"--help\" to show all available benchmarking commands.")
		("config", boost::program_options::value<std::string>(), "Set configuration file. Default: \"./config.xml\"")
		("config-template", "Outputs template xml for the configuration file, to stdout.")
		("stop", "Sends stop signal to process specified by --pid-file.")
		("pid-file", boost::program_options::value<std::string>(), "Path to file containing process id. Default: \"./prototype.pid\"")
		;

	// read command line parameters
	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	// check command line parameters
	if (vm.count("help"))
	{
		cout << desc;
		throw QuitQuietly(1);
	}

	if (vm.count("config-template"))
	{
		ConfigParser::PrintExampleConfig();
		throw QuitQuietly(0);
	}

	pidfile = "prototype.pid";

	if (vm.count("pid-file"))
	{
		pidfile = vm["pid-file"].as<string>();
	}

	if (vm.count("stop"))
	{
		cout << "Attempting to stop process with pid pid-file: " << pidfile << endl;
		int pid = readPid();
		SendControlC(pid);
		cout << "Sent stop signal to pid: " << pid << " please give it a few moments to exit." << endl;
		throw QuitQuietly(0);
	}
	else
	{
		writePid();
		//delete the pid file when program exits
		atexit(removePid);
	}

	std::string configfile = "config.xml";

	if (vm.count("config"))
	{
		configfile = vm["config"].as<string>();
	}
	else
	{
		cout << "Configuration file not specified, trying default: " << configfile << endl;
	}

	//create configuration file parser
	return boost::shared_ptr<ConfigParser> (
		new ConfigParser(configfile)
	);
}

vector<BaseThread::Ptr> createAllThreads(ConfigParser::Ptr Config)
{
	//create storage
	vector<BaseThread::Ptr> threadList;

	//start persistent storage service
	PersistentStorageService::getInstance()->Start();

	//create sink
	auto Sink = ThreadFactory::CreateSink(
		Config->GetSinkName(), 
		Config->GetSinkOptions());

	//create signer
	auto Signer = ThreadFactory::CreateSigner(
		Config->GetSignerName(), 
		Config->GetSignerOptions(), 
		Sink);

	//create Timestamper
	auto Aggregator = ThreadFactory::CreateAggregator(
		Config->GetAggregatorName(), 
		Config->GetAggregatorOptions(), 
		Signer);

	//add them to threadList
	threadList.push_back(Sink);
	threadList.push_back(Signer);
	threadList.push_back(Aggregator);

	//we want sources to be added last therefore
	//so we use a temporary list for them
	vector<BaseThread::Ptr> sources;

	//create sources and their filters
	int nSources = Config->GetNumberOfSources();
	for (int s = nSources - 1; s >= 0; s--)
	{
		BaseReceiver::Ptr nextStep(Aggregator);
		//for each Source
		int nFilters = Config->GetNumberOfFilters(s);
		for (int f = nFilters - 1; f >= 0; f--)
		{

			//for each filter
			BaseReceiver::Ptr newFilter = ThreadFactory::CreateFilter(
				Config->GetFilterName(s, f),
				Config->GetFilterOptions(s, f),
				nextStep);

			//store filter
			threadList.push_back(newFilter);

			//the next created filter or current source should use this as the next step.
			nextStep = newFilter;
		}

		//create source
		BaseThread::Ptr newSource = ThreadFactory::CreateSource(
			Config->GetSourceName(s),
			Config->GetSourceOptions(s),
			nextStep);

		//store source
		sources.push_back(newSource);
	}

	//now we add the sources to the thread list
	threadList.insert(threadList.end(), sources.begin(), sources.end());

	//creation appears to be a success, so let's start all threads
	BOOST_FOREACH(BaseThread::Ptr thread, threadList)
	{
		thread->Start();
	}

	return threadList;
}


void JoinAllThreads(vector<BaseThread::Ptr> threadList)
{
	//We go in reverse order so that the 
	//pipeline may have time to finish all remaining items
	int i = 0;
	cout << threadList.size() << " pipeline stages created and assumed to be still running." << endl;
	BOOST_REVERSE_FOREACH(BaseThread::Ptr thread, threadList)
	{
		i++;
		thread->StopJoin();
		cout << "Stage " << i << " has stopped." << endl;
	}

	//persistent storage service must also stop
	PersistentStorageService::getInstance()->StopJoin();
	cout << "Persistent Storage Service has stopped." << endl;

	cout << "All stages have now stopped." << endl;
}


int readPid()
{
	ifstream in(pidfile);
	if (in.is_open())
	{
		int pid;
		in >> pid;
		return pid;
	}
	else
	{
		throw MainOperationException("Could not read from pid file!");
	}
}


void writePid()
{
	ofstream out(pidfile);
	if (out.is_open())
	{
		int pid;
#ifdef _WIN32
		pid = GetCurrentProcessId();
#else
		pid = getpid();
#endif
		out << pid;
	}
	else
	{
		throw MainOperationException("Could not write to pid file!");
	}
}

void removePid()
{
	remove(pidfile.c_str());
}


#ifdef _WIN32
BOOL WINAPI ConsoleHandler(DWORD CEvent)
{
	//switch (CEvent)
	//{
	//case CTRL_C_EVENT:
	//case CTRL_BREAK_EVENT:
	//case CTRL_CLOSE_EVENT:
	//case CTRL_LOGOFF_EVENT:
	//case CTRL_SHUTDOWN_EVENT:
	//	break;
	//}
	niceShutdownHandler();

	return TRUE;
}
#endif

void unixStopSignalHandler(int)
{
	niceShutdownHandler();
	exit(0);
}

void subscribeStopSignals()
{
#ifdef _WIN32
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, TRUE) == FALSE)
	{
		// unable to install handler... 
		// display message to the user
		cout << "Unable to install console control-C handler (used to detect exit signals from user)!\n";
		return throw QuitQuietly(-1);
	}
#endif
	if (signal(SIGINT, unixStopSignalHandler) == SIG_ERR)
	{
		// unable to install handler... 
		// display message to the user
		cout << "Unable to install SIGINT handler (used to detect exit signals from user)!\n";
		return throw QuitQuietly(-1);
	}
	if (signal(SIGTERM, unixStopSignalHandler) == SIG_ERR)
	{
		// unable to install handler... 
		// display message to the user
		cout << "Unable to install SIGTERM handler (used to detect exit signals from user)!\n";
		return throw QuitQuietly(-1);
	}
}

void SendControlC(int pid)
{
#ifdef _WIN32
	if (!FreeConsole()) //detach from current console
	{
		cout << "ERROR: FreeConsole() failed!";
	}
	if (!AttachConsole(pid)) // attach to process console
	{
		cout << "ERROR: AttachConsole() failed!";
	}
	if (!SetConsoleCtrlHandler(NULL, TRUE)) // disable Control+C handling for our app
	{
		cout << "ERROR: SetConsoleCtrlHandler() failed!";
	}
	if (!GenerateConsoleCtrlEvent(CTRL_C_EVENT, 0)) // generate Control+C event
	{
		cout << "ERROR: GenerateConsoleCtrlEvent() failed!";
	}
#else
	kill(pid, SIGINT);
#endif
}
