#include "RequestHandler.h"
#include "../../common/DEFINITIONS.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/scoped_array.hpp>
#include <boost/algorithm/string/predicate.hpp>


using namespace Core;

sqlite3* myOpen(std::string filename)
{
	sqlite3* dbTmpPtr;
	int rc = sqlite3_open(filename.c_str(), &dbTmpPtr);

	if (rc)
	{
		throw std::runtime_error(std::string("Can't open log storage: ") + sqlite3_errmsg(dbTmpPtr));
	}
	return dbTmpPtr;
}

void RequestHandler::myExecute(const char* query)
{
	char *zErrMsg = 0;
	int rc = sqlite3_exec(db, query, 0, 0, &zErrMsg);
	if (rc != SQLITE_OK){
		throw std::runtime_error(std::string("SQL error: ") + zErrMsg);
	}
}

RequestHandler::RequestHandler(std::string filename)
{
	db = myOpen(filename);

	//check if library is thread_safe
	if (!sqlite3_threadsafe())
		throw StorageError("SQLITE lib is not compile with thread safe operation enabled");

	/* Execute SQL statement */
	myExecute(
		"CREATE TABLE IF NOT EXISTS LOG_MESSAGE("  \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT," \
		"FACILITY_NUMBER INT NOT NULL," \
		"SEVERITY_VALUE INT NOT NULL," \
		"TIMESTAMP TEXT NOT NULL," \
		"HOSTNAME TEXT NOT NULL," \
		"APPNAME TEXT NOT NULL," \
		"PROCID TEXT NOT NULL," \
		"MSG_TYPE TEXT NOT NULL," \
		"STRUCTURED_DATA TEXT NOT NULL," \
		"CONTENT TEXT NOT NULL); "
	);

	myExecute(
		"CREATE TABLE IF NOT EXISTS SECRETS("  \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT," \
		"TYPE TEXT NOT NULL," \
		"CONTENT TEXT NOT NULL,"\
		"TIMESTAMP DATETIME DEFAULT CURRENT_TIMESTAMP); "
	);

	if (sqlite3_prepare(db, "INSERT INTO LOG_MESSAGE ("\
		"FACILITY_NUMBER,"\
		"SEVERITY_VALUE,"\
		"TIMESTAMP,"\
		"HOSTNAME,"\
		"APPNAME,"\
		"PROCID,"\
		"MSG_TYPE,"\
		"STRUCTURED_DATA,"\
		"CONTENT)"\
		"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);", -1, &preparedInsertStmt, 0) != SQLITE_OK)
	{
		throw StorageError(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

	if (sqlite3_prepare(db, "INSERT INTO SECRETS (TYPE, CONTENT) VALUES (?, ?);", -1, &preparedInsertSecretStmt, 0) != SQLITE_OK)
	{
		throw StorageError(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
	}

    if (sqlite3_prepare(db, "SELECT TIMESTAMP FROM LOG_MESSAGE ORDER BY ID DESC LIMIT 1;", -1, &preparedSelectLastTimestamp, 0) != SQLITE_OK)
    {
        throw StorageError(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
    }

    if (sqlite3_prepare(db, "SELECT COUNT(*) FROM LOG_MESSAGE WHERE "
        "FACILITY_NUMBER = ? AND "
        "SEVERITY_VALUE= ? AND "
        "TIMESTAMP= ? AND "
        "HOSTNAME= ? AND "
        "APPNAME= ? AND "
        "PROCID= ? AND "
        "MSG_TYPE= ? AND "
        "STRUCTURED_DATA= ? AND "
        "CONTENT= ? ;", -1, &preparedCountDuplicates_very_slow, 0) != SQLITE_OK)
    {
        throw StorageError(std::string("Could not create sqllite3 prepared stmt: ") + sqlite3_errmsg(db));
    }
}

RequestHandler::~RequestHandler()
{
	sqlite3_close(db);
}

/// Handle a request and produce a reply.
std::string RequestHandler::handle_request(const char* data, size_t size)
{
	try{

		//Are we continuing a read on a previous request?
		if (previousMessagePart.empty())
		{
			//no apperently not, so lets start a new transaction
			nrParsedMessagesInCurrentBatch = 0;
            writtenMessagesCurrentBatch = 0;
		}

		std::stringstream ss;
		ss << previousMessagePart << std::string(data, size);
		previousMessagePart.clear();

		while (true)
		{
			int length = -1;
			ss >> length;
			if (ss.eof())
			{
				try{
					previousMessagePart.append(ss.str().substr((unsigned int)ss.tellg()));
				}
				catch (...)
				{
					if (length > 0)
					{
						previousMessagePart.append(boost::lexical_cast<std::string>(length));
					}
				}
				break;
			}

			char c;
			ss.get(c); //get space
			if (c != ' ')
			{
				throw ParseError("Space must follow packet length");
			}

			boost::scoped_array<char> buf(new char[length + 1]);
			ss.read(buf.get(), length);
			int realLength = (int)ss.gcount();

			buf[realLength] = 0;

			std::string message(buf.get(), realLength);

			if (realLength < length)
			{
				previousMessagePart.append(boost::lexical_cast<std::string>(length));
				previousMessagePart.append(" ");
				previousMessagePart.append(message);
				break;
			}

			handle_message(message);
			
			nrParsedMessagesInCurrentBatch++;		
		}

		//are we waiting for more?
		if (previousMessagePart.empty())
		{
			//no we have everything in this batch, let's commit everything!
			myExecute("COMMIT TRANSACTION");
			return boost::lexical_cast<std::string>(nrParsedMessagesInCurrentBatch)+" ";
		}
		else
		{
			// yes we are waiting for more! empty reply
			return "";
		}
	}
	catch (ParseError)
	{
		try{ myExecute("ROLLBACK TRANSACTION"); }
		catch (...){}
		previousMessagePart = "";
		nrParsedMessagesInCurrentBatch = 0;
		//simply rethrow
		throw;
	}
	catch (StorageError)
	{
		try{ myExecute("ROLLBACK TRANSACTION"); }
		catch (...){}
		previousMessagePart = "";
		nrParsedMessagesInCurrentBatch = 0;
		//simply rethrow
		throw;
	}
	catch (std::exception& e)
	{
		try{ myExecute("ROLLBACK TRANSACTION"); }
		catch (...){}
		previousMessagePart = "";
		nrParsedMessagesInCurrentBatch = 0;
		throw StorageError(e.what());
	}
	catch (...)
	{
		try{ myExecute("ROLLBACK TRANSACTION"); }
		catch (...){}
		previousMessagePart = "";
		nrParsedMessagesInCurrentBatch = 0;
		throw StorageError("Some unknown error happened while attempting to store received data.");
	}
}


void RequestHandler::handle_message(std::string message)
{
    //Parse message
    auto data = Common::LogMessage::ParseString(message);

    //special message hook
    bool writeToFile = check_for_and_handle_special_messages(data);

    //should this file be written to file?
	if (writeToFile)
	{
        //but check for duplicates first
        if (!check_for_duplicate_messages(data))
        {
            write_message_to_file(data);
        }
	}
}

RequestHandler::SPECIAL_MESSAGE_TYPE RequestHandler::determine_special_message(Common::LogMessage::ParsedData& message)
{
    if (boost::algorithm::contains(message.structuredData, ANTI_TRUNCATION))
    {
        if (message.content == ANTI_TRUNC_START_MESSAGE)
        {
            return ANTI_TRUNC_START;
        }
        else if (message.content == ANTI_TRUNC_CLOSE_MESSAGE)
        {
            return ANTI_TRUNC_END;
        }
        else
        {
            throw ParseError("Could not determine type of " ANTI_TRUNCATION " message!");
        }
    }

    //add more types here

    return NOT_SPECIAL;
}

bool RequestHandler::check_for_and_handle_special_messages(Common::LogMessage::ParsedData& message)
{
    auto type = determine_special_message(message);

    switch (type)
    {
    case Core::RequestHandler::NOT_SPECIAL:
        return true;
        break;
    case Core::RequestHandler::ANTI_TRUNC_START:
        return handle_trunc_start_message(message);
        break;
    case Core::RequestHandler::ANTI_TRUNC_END:
        return handle_trunc_end_message(message);
        break;
    default:
        throw ParseError("Unrecognized message type");
        return false;
        break;
    }
}

void bind_log_message_to_prepared_statement(Common::LogMessage::ParsedData& message, sqlite3_stmt* stmt)
{
    if (sqlite3_bind_int(stmt, 1, message.facilityNumber) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_int(stmt, 2, message.severityValues) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 3, message.timestamp.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 4, message.hostname.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 5, message.appName.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 6, message.procId.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 7, message.messageType.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 8, message.structuredData.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
    if (sqlite3_bind_text(stmt, 9, message.content.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
        throw RequestHandler::StorageError("Could not bind parameter");
}

boost::posix_time::ptime timeStringToComparable(std::string str)
{
    static std::locale format(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%dT%H:%M:%s"));
    boost::posix_time::ptime pt;
    std::istringstream is(str);
    is.imbue(format);
    is >> pt;
    if (pt == boost::posix_time::not_a_date_time)
        throw RequestHandler::ParseError("Unable to parse timestring");
    return pt;
}

std::string RequestHandler::fetchLastWrittenTimeFromLogFile()
{
    std::string str = ""; //EPOCH
    int rows = 0;
    //perform query
    while (true)
    {
        int s = sqlite3_step(preparedSelectLastTimestamp);
        if (s == SQLITE_ROW)
        {
            int bytes;
            char* text;
            text = (char*)sqlite3_column_text(preparedSelectLastTimestamp, 0);
            bytes = sqlite3_column_bytes(preparedSelectLastTimestamp, 0);
            str.append(text, bytes);
            rows++;
            // text is freed automatically by sqlite
        }
        else if (s == SQLITE_DONE) {
            break;
        }
        else {
            throw ParseError(std::string("Get last log message timestamp error: ") + sqlite3_errmsg(db));
        }
    }

    //reset state
    if (sqlite3_reset(preparedSelectLastTimestamp) != SQLITE_OK)
    {
        throw ParseError(std::string("Get log message error: reset stmt failed: ") + sqlite3_errmsg(db));
    }

    return str;
}

//returns true if the massage has already been written to log file
bool RequestHandler::check_for_duplicate_messages(Common::LogMessage::ParsedData& message)
{
    if (writtenMessagesCurrentBatch > 0)
    {
        //Previous messages were apparently not duplicates
        //so no need to check again
        return false;
    }

    //Do we need to fetch the last written timestamp from the file?
    if (lastWrittenTimestamp == boost::date_time::not_a_date_time)
    {
        std::string str = fetchLastWrittenTimeFromLogFile();
        
        if (str.empty())
            return false; //there is no last message so there can be no duplicate

        lastWrittenTimestamp = timeStringToComparable( str );
    }

    //parse timestamp
    auto nTime = timeStringToComparable( message.timestamp );

    if (nTime > lastWrittenTimestamp)
    {
        //we are safe
        return false;
    }

    //okay so we need to really make sure that there are no duplicates
    //this is very slow, and that is the reason for the quick checks above
    bind_log_message_to_prepared_statement(message, preparedCountDuplicates_very_slow);

    int duplicates = 0;
    int rows = 0;
    //perform query
    while (true)
    {
        int s = sqlite3_step(preparedCountDuplicates_very_slow);
        if (s == SQLITE_ROW)
        {
            duplicates = sqlite3_column_int(preparedCountDuplicates_very_slow, 0);
            rows++;
            // text is freed automatically by sqlite
        }
        else if (s == SQLITE_DONE) {
            break;
        }
        else {
            throw ParseError(std::string("Count duplicates error: ") + sqlite3_errmsg(db));
        }
    }

    //reset state
    if (sqlite3_reset(preparedCountDuplicates_very_slow) != SQLITE_OK)
    {
        throw ParseError(std::string("Count duplicates error: reset stmt failed: ") + sqlite3_errmsg(db));
    }

    if (rows == 0)
    {
        throw ParseError("Count duplicates error: no result from sqlite");
    }

    //check for messages
    return duplicates > 0;
}

void RequestHandler::write_message_to_file(Common::LogMessage::ParsedData& message)
{
    if (writtenMessagesCurrentBatch == 0)
    {
        myExecute("BEGIN TRANSACTION");
    }

    bind_log_message_to_prepared_statement(message, preparedInsertStmt);

	if (sqlite3_step(preparedInsertStmt) != SQLITE_DONE)
	{
		throw StorageError(std::string("Write log message error: execute sqllite step error: ") + sqlite3_errmsg(db));
	}
	if (sqlite3_reset(preparedInsertStmt) != SQLITE_OK)
	{
		throw StorageError(std::string("Write log message error: reset stmt failed: ") + sqlite3_errmsg(db));
	}

    lastWrittenTimestamp = timeStringToComparable(message.timestamp);
    writtenMessagesCurrentBatch++;
}

bool RequestHandler::handle_trunc_start_message(Common::LogMessage::ParsedData& message)
{
    //we don't really have to do anything to handle this,
    //this is just here for completenes sake
    return true;
}

bool RequestHandler::handle_trunc_end_message(Common::LogMessage::ParsedData& message)
{
    //We only remove one, and we search from the bottom of the table due to speed considerations
    std::string query = "DELETE FROM LOG_MESSAGE WHERE ID in (SELECT ID FROM LOG_MESSAGE WHERE CONTENT = '";
    query += ANTI_TRUNC_CLOSE_MESSAGE;
    query += "' AND LOG_MESSAGE.STRUCTURED_DATA LIKE '%";
    query += ANTI_TRUNCATION;
    query += "%";
    query += ANTI_TRUNC_CLOSE_HASH;
    query += "%' ORDER BY LOG_MESSAGE.ID DESC LIMIT 1 )";

    myExecute(query.c_str());

    //we want this message to be written to file
    return true;
}