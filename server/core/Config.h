#ifndef CONFIG_H
#define CONFIG_H

#include <boost/lexical_cast.hpp>
#include <map>
#include <stdexcept>

namespace Core
{

	class Config
	{
	public:

		enum KEYS
		{
			K_STORAGE_DIR,
			K_BIND_ADDRESS,
			K_BIND_PORT,
			K_CERTIFICATE_CHAIN_PEM,
			K_PRIVATE_KEY_PEM,
			K_PRIVATE_KEY_PASSWORD,
			K_TMP_DH_PEM,
			K_TMP_DH_GENERATE,
			K_NR_THREADS,
			NR_OF_KEYS
		};

		class QuitQuietly : public std::runtime_error
		{
		public:
			QuitQuietly(int code)
				: std::runtime_error("")
			{
				returnCode = code;
			}
			int returnCode;
		};
		
		static void Initialize(int argc, char* argv[]);

		//Get a value as a string
		static std::string Get(KEYS key);

		template<class T>
		static T Get(KEYS key)
		{
			try
			{
				return boost::lexical_cast<T>(Get(key));
			}
			catch (const boost::bad_lexical_cast)
			{
				return boost::lexical_cast<T>(GetDefault(key));
			}
		}

	private:

		typedef std::map<KEYS, std::string> ConfigValues;

		static std::string GetDefault(KEYS key);

		static ConfigValues keyValues;

	};

}

#endif