#ifndef SERVER_H
#define SERVER_H

#include "Connection.h"
#include "RequestHandler.h"

#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace Core
{

	class Server : private boost::noncopyable
	{
	public:
		Server();

		void run();

	private:

		/// Initiate an asynchronous accept operation.
		void start_accept();

		/// Handle completion of an asynchronous accept operation.
		void handle_accept(const boost::system::error_code& e);

		/// Handle a request to stop the server.
		void handle_stop();

		/// The io_service used to perform asynchronous operations.
		boost::asio::io_service io_service_;

		/// The signal_set is used to register for process termination notifications.
		boost::asio::signal_set signals_;

		/// Acceptor used to listen for incoming connections.
		boost::asio::ip::tcp::acceptor acceptor_;

		/// The next connection to be accepted.
		Connection::Ptr new_connection_;

		/// The context for ssl options
		boost::asio::ssl::context context_;

	};
}

#endif