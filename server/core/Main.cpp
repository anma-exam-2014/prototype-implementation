#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#include "Config.h"
#include "Server.h"

int main(int argc, char* argv[])
{
	try
	{
		//Read configurations
		Core::Config::Initialize(argc, argv);

		// Initialise the server.
		Core::Server s;

		// Run the server until stopped.
		s.run();
	}
	catch (Core::Config::QuitQuietly& e)
	{
		return e.returnCode;
	}
	catch (std::exception& e)
	{
		std::cerr << "Error occured: " << e.what() << "\n";
	}

	return 0;
}