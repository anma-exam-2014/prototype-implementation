#ifndef CONNECTION_H
#define CONNECTION_H

#include "RequestHandler.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace Core
{

	class Connection
		: public boost::enable_shared_from_this<Connection>,
		  private boost::noncopyable
	{
	public:
		typedef boost::shared_ptr<Connection> Ptr;

		/// Construct a connection with the given io_service.
		explicit Connection(boost::asio::io_service& io_service, boost::asio::ssl::context& context);

		~Connection();

		/// Get the socket associated with the connection.
		boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& socket();

		/// Start the first asynchronous operation for the connection.
		void start();

		/// Shutdown the connection
		void shutdown();

	private:
		/// Handle completion of a read operation.
		void handle_read(const boost::system::error_code& e,
			std::size_t bytes_transferred);

		/// Handle completion of a write operation.
		void handle_write(const boost::system::error_code& e);

		/// Handle completion of ssl handshake
		void handle_handshake(const boost::system::error_code& error);

		/// Handle completion of a write operation.
		void handle_shutdown(const boost::system::error_code& e);

		/// Verify client certificate
		bool verify_certificate(bool preverified,
			boost::asio::ssl::verify_context& ctx);

		/// Strand to ensure the connection's handlers are not called concurrently.
		boost::asio::io_service::strand strand_;

		/// Socket for the connection.
		boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket_;

		/// The handler used to process the incoming request.
		boost::scoped_ptr<RequestHandler> request_handler_;

		/// Buffer for incoming data.
		boost::array<char, 8192> buffer_;

		/// Name of connecting machine (taken from CN of client certificate)
		std::string remoteName;
	};

}


#endif
