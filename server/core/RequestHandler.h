#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <stdexcept>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/date_time.hpp>
#include "../../common/sqlite3/sqlite3.h"
#include "../../common/LogMessage.h"

namespace Core
{
	// One object will be shared accross multiple threads, 
	// be sure to make methods and sub objects thread safe
	class RequestHandler
		: private boost::noncopyable
	{
	public:
		class ParseError : public std::runtime_error
		{
		public:
			ParseError(const std::string& msg)
				: std::runtime_error(msg)
			{}
		};
		class StorageError : public std::runtime_error
		{
		public:
			StorageError(const std::string& msg)
				: std::runtime_error(msg)
			{}
		};

		RequestHandler(std::string filename);
		~RequestHandler();

		/// Handle a request and produce a reply.
		std::string handle_request(const char* data, size_t size);

    private:

        enum SPECIAL_MESSAGE_TYPE
        {
            NOT_SPECIAL = 0,
            ANTI_TRUNC_START,
            ANTI_TRUNC_END
        };

		void handle_message(std::string message);
        void write_message_to_file(Common::LogMessage::ParsedData& message);
        SPECIAL_MESSAGE_TYPE determine_special_message(Common::LogMessage::ParsedData& message);
        bool check_for_duplicate_messages(Common::LogMessage::ParsedData& message);
        bool check_for_and_handle_special_messages(Common::LogMessage::ParsedData& message);

        //special message handlers
        bool handle_trunc_start_message(Common::LogMessage::ParsedData& message);
        bool handle_trunc_end_message(Common::LogMessage::ParsedData& message);

        //helper functions
        std::string fetchLastWrittenTimeFromLogFile();

		std::string previousMessagePart;
        boost::posix_time::ptime lastWrittenTimestamp;
		int nrParsedMessagesInCurrentBatch = 0;
        int writtenMessagesCurrentBatch = 0;

		sqlite3* db = NULL;
		sqlite3_stmt* preparedInsertStmt;
		sqlite3_stmt* preparedInsertSecretStmt;
        sqlite3_stmt* preparedSelectLastTimestamp;
        sqlite3_stmt* preparedCountDuplicates_very_slow;

		void myExecute(const char* query);

	};

}

#endif