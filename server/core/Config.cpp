#include "Config.h"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

using namespace Core;

Config::ConfigValues Config::keyValues;

#ifdef _WIN32

#include <windows.h>

void echo(bool on = true)
{
	DWORD  mode;
	HANDLE hConIn = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleMode(hConIn, &mode);
	mode = on
		? (mode | ENABLE_ECHO_INPUT)
		: (mode & ~(ENABLE_ECHO_INPUT));
	SetConsoleMode(hConIn, mode);
}

#else

#include <termios.h>
#include <unistd.h>

void echo(bool on = true)
{
	struct termios settings;
	tcgetattr(STDIN_FILENO, &settings);
	settings.c_lflag = on
		? (settings.c_lflag | ECHO)
		: (settings.c_lflag & ~(ECHO));
	tcsetattr(STDIN_FILENO, TCSANOW, &settings);
}

#endif

void Config::Initialize(int argc, char* argv[])
{
	// defines comand line arguments
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "Produce this help message.")
		("storage-dir,s", boost::program_options::value<std::string>(), ("Store all recieved logs in specified folder (WITHOUT last trailing slash). Default: " + GetDefault(K_STORAGE_DIR)).c_str())
		("bind,b", boost::program_options::value<std::string>(), ("Bind server to address. Default: " + GetDefault(K_BIND_ADDRESS)).c_str())
		("port,p", boost::program_options::value<std::string>(), ("Bind server to port. Default: " + GetDefault(K_BIND_PORT)).c_str())
		("cert,c", boost::program_options::value<std::string>(), ("File containing certificate chain for server key (PEM format). Default: " + GetDefault(K_CERTIFICATE_CHAIN_PEM)).c_str())
		("private-key,k", boost::program_options::value<std::string>(), ("File containing server private key (PEM format). Default: " + GetDefault(K_PRIVATE_KEY_PEM)).c_str())
		("passphrase", boost::program_options::value<std::string>(), "The passphrse used to decrypt the server private key. Default: read from standard input.")
		("tmp-dh,d", boost::program_options::value<std::string>(), ("Use the specified file to obtain the temporary Diffie-Hellman parameters. See OpenSSL documentation for details. Default: " + GetDefault(K_TMP_DH_PEM)).c_str())
		("generate-tmp-dh,g", (std::string("Generate temporary Diffie-Hellman parameters on startup. Default: ") + (Get<bool>(K_TMP_DH_GENERATE) ? "yes" : "no")).c_str())
		;

	// read command line parameters
	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	// check command line parameters
	if (vm.count("help"))
	{
		std::cout << desc;
		throw QuitQuietly(0);
	}
	if (vm.count("storage-dir"))
	{
		keyValues[K_STORAGE_DIR] = vm["storage-dir"].as<std::string>();
	}
	if (vm.count("bind"))
	{
		keyValues[K_BIND_ADDRESS] = vm["bind"].as<std::string>();
	}
	if (vm.count("port"))
	{
		keyValues[K_BIND_PORT] = vm["port"].as<std::string>();
	}
	if (vm.count("cert"))
	{
		keyValues[K_CERTIFICATE_CHAIN_PEM] = vm["cert"].as<std::string>();
	}
	if (vm.count("private-key"))
	{
		keyValues[K_PRIVATE_KEY_PEM] = vm["private-key"].as<std::string>();
	}
	if (vm.count("passphrase"))
	{
		keyValues[K_PRIVATE_KEY_PASSWORD] = vm["passphrase"].as<std::string>();
	}
	if (vm.count("tmp-dh"))
	{
		keyValues[K_TMP_DH_PEM] = vm["tmp-dh"].as<std::string>();
	}
	if (vm.count("generate-tmp-dh"))
	{
		keyValues[K_TMP_DH_GENERATE] = "1";
	}

	//do some validation
	if (!boost::filesystem::is_directory(Get(K_STORAGE_DIR)))
	{
		throw std::runtime_error("The folder \"" + Get(K_STORAGE_DIR) + "\" does not exist.");
	}
}

//Get a value as a string
std::string Config::Get(KEYS key)
{
	if (keyValues.count(key))
	{
		return keyValues[key];
	}
	else if (key == K_PRIVATE_KEY_PASSWORD)
	{
		//special case
		std::string pwd;
		std::cout << "Please enter the passcode for server private key: ";

		echo(false);
		std::getline(std::cin, pwd);
		echo(true);

		std::cout << std::endl;

		return pwd;
	}
	else
	{
		return GetDefault(key);
	}
}



std::string Config::GetDefault(KEYS key)
{
	switch (key)
	{
	case KEYS::K_STORAGE_DIR:
		return "./logs";
	case KEYS::K_BIND_ADDRESS:
		return "0.0.0.0";
	case KEYS::K_BIND_PORT:
		return "6514";
	case KEYS::K_CERTIFICATE_CHAIN_PEM:
		return "./chain.pem";
	case KEYS::K_PRIVATE_KEY_PEM:
		return "./server.key";
	case KEYS::K_TMP_DH_PEM:
		return "./tmp-dh.pem";
	case KEYS::K_TMP_DH_GENERATE:
		return "0";
	case KEYS::K_NR_THREADS:
		return "1";
	default:
		throw std::logic_error("Unkown key specified for Config::GetDefault()");
	}
}