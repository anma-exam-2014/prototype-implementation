#include "Server.h"
#include "Config.h"

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>


using namespace Core;

void GenerateTmpDHFile(std::string filename);

Server::Server()
	: io_service_(),
	signals_(io_service_),
	acceptor_(io_service_),
	new_connection_(),
	context_(boost::asio::ssl::context::tlsv12)
{
	// Register to handle the signals that indicate when the server should exit.
	// It is safe to register for the same signal multiple times in a program,
	// provided all registration for the specified signal is made through Asio.
	signals_.add(SIGINT);
	signals_.add(SIGTERM);
#if defined(SIGQUIT)
	signals_.add(SIGQUIT);
#endif // defined(SIGQUIT)
	signals_.async_wait(boost::bind(&Server::handle_stop, this));

	// Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
	boost::asio::ip::tcp::resolver resolver(io_service_);
	boost::asio::ip::tcp::resolver::query query(Config::Get(Config::K_BIND_ADDRESS), Config::Get(Config::K_BIND_PORT));
	boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
	acceptor_.open(endpoint.protocol());
	acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	acceptor_.bind(endpoint);
	acceptor_.listen();
	

	context_.set_options(
		boost::asio::ssl::context::default_workarounds
		| boost::asio::ssl::context::no_tlsv1
		| boost::asio::ssl::context::single_dh_use);
	context_.clear_options(boost::asio::ssl::context::no_compression);

	context_.set_verify_mode(
		boost::asio::ssl::context::verify_peer
		| boost::asio::ssl::context::verify_fail_if_no_peer_cert);

	context_.set_password_callback(boost::bind(&Config::Get<std::string>, Config::K_PRIVATE_KEY_PASSWORD));

	if (Config::Get<bool>(Config::K_TMP_DH_GENERATE))
	{
		//we ned to generate DH parameters
		GenerateTmpDHFile(Config::Get(Config::K_TMP_DH_PEM));
	}

	try{
		context_.use_tmp_dh_file(Config::Get(Config::K_TMP_DH_PEM));
	}
	catch (boost::system::system_error &e)
	{
		throw std::runtime_error("Error while reading in temporary Diffie-Hellman prameters from file: \"" + Config::Get(Config::K_TMP_DH_PEM) + "\". Error was: " + e.what());
	}
	try{
		context_.use_certificate_chain_file(Config::Get(Config::K_CERTIFICATE_CHAIN_PEM));
	}
	catch (boost::system::system_error &e)
	{
		throw std::runtime_error("Error while reading in certificate chain from file: \"" + Config::Get(Config::K_CERTIFICATE_CHAIN_PEM) + "\". Error was: " + e.what());
	}
	try{
		context_.use_private_key_file(Config::Get(Config::K_PRIVATE_KEY_PEM), boost::asio::ssl::context::file_format::pem);
	}
	catch (boost::system::system_error &e)
	{
		throw std::runtime_error("Error while reading in server private key from file: \"" + Config::Get(Config::K_PRIVATE_KEY_PEM) + "\". Error was: " + e.what());
	}


	start_accept();

	std::cout << "Initialization complete!" << std::endl;
}

void Server::run()
{
	// Create a pool of threads to run all of the io_services.
	std::vector<boost::shared_ptr<boost::thread> > threads;
	std::size_t thread_pool_size_ = Config::Get<size_t>(Config::K_NR_THREADS);

	for (std::size_t i = 0; i < thread_pool_size_; ++i)
	{
		boost::shared_ptr<boost::thread> thread(new boost::thread(
			boost::bind(&boost::asio::io_service::run, &io_service_)));
		threads.push_back(thread);
	}

	std::cout << "Waiting for connections on " << Config::Get(Config::K_BIND_ADDRESS) << ":" << Config::Get(Config::K_BIND_PORT) << "..." << std::endl;

	// Wait for all threads in the pool to exit.
	for (std::size_t i = 0; i < threads.size(); ++i)
		threads[i]->join();
}

void Server::start_accept()
{
	new_connection_.reset(new Connection(io_service_, context_));
	acceptor_.async_accept(new_connection_->socket().lowest_layer(),
		boost::bind(&Server::handle_accept, this,
		boost::asio::placeholders::error));
}

void Server::handle_accept(const boost::system::error_code& e)
{
	if (!e)
	{
		new_connection_->start();
	}

	start_accept();
}

void Server::handle_stop()
{
	io_service_.stop();
}

#include <openssl/dh.h>
#include <stdio.h>

void GenerateTmpDHFile(std::string filename)
{
	std::cout << "Generating temporary Diffie-Hellman parameters:" << std::endl;

	DH* dh = DH_new();
	if (!dh)
		throw std::runtime_error("DH_new failed");

	std::cout << "\tGenerating DH parameters ... ";
	if (!DH_generate_parameters_ex(dh, 2048, DH_GENERATOR_2, 0))
		throw std::runtime_error("DH_generate_parameters_ex failed");
	std::cout << "done\n";

	std::cout << "\tChecking DH parameters ... ";
	int codes = 0;
	if (!DH_check(dh, &codes))
		throw std::runtime_error("DH_check failed");
	std::cout << "done\n";

	std::cout << "\tGenerating DH keys ... ";
	if (!DH_generate_key(dh))
		throw std::runtime_error("DH_generate_key failed");
	std::cout << "done\n";

	std::cout << "\tWriting to file \"" << filename << "\" ... ";
#pragma warning (disable : 4996)
	FILE* fp = fopen(filename.c_str(), "w");
	if (!fp)
		throw std::runtime_error("Could not open file for writing");
	int write_ok = PEM_write_DHparams(fp, dh);
	fclose(fp);
	if (!write_ok)
		throw std::runtime_error("PEM_write_DHparams failed");
	std::cout << "done\n";

	std::cout << "Done!" << std::endl;
}
