#include "Connection.h"
#include "Config.h"

#include <vector>
#include <boost/bind.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>
#include <iostream>
#include "RequestHandler.h"

using namespace Core;

Connection::Connection(boost::asio::io_service& io_service, boost::asio::ssl::context& context)
	: strand_(io_service),
	socket_(io_service, context),
	request_handler_(NULL)
{
}

Connection::~Connection()
{
}

boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& Connection::socket()
{
	return socket_;
}

void Connection::start()
{
	socket_.set_verify_callback(boost::bind(&Connection::verify_certificate, this, _1, _2));
	socket_.async_handshake(boost::asio::ssl::stream_base::server,
		boost::bind(&Connection::handle_handshake, shared_from_this(),
		boost::asio::placeholders::error));
}

void Connection::shutdown()
{
	socket_.async_shutdown(
		boost::bind(&Connection::handle_shutdown, shared_from_this(),
		boost::asio::placeholders::error));
}

void Connection::handle_handshake(const boost::system::error_code& e)
{
	if (!e)
	{
		request_handler_.reset(new RequestHandler(Config::Get(Config::K_STORAGE_DIR) + "/" + remoteName + ".sql"));

		std::cout << std::endl << "Handshake with \"" << remoteName
			<< "\" completed, ready to recieve content from " 
			<< socket_.lowest_layer().remote_endpoint().address().to_string() 
			<< std::endl;

		socket_.async_read_some(boost::asio::buffer(buffer_),
			strand_.wrap(
			boost::bind(&Connection::handle_read, shared_from_this(),
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred)));
	}
	else
	{
		std::cout << e << ":" << e.message() << std::endl;
		shutdown();
	}
}

void Connection::handle_read(const boost::system::error_code& e,
	std::size_t bytes_transferred)
{
	if (!e)
	{
		try{
			std::string response = request_handler_->handle_request(buffer_.data(), bytes_transferred);
			buffer_[0] = 0;

			std::cout << response << std::flush;

			boost::asio::async_write(socket_, boost::asio::buffer(response),
				strand_.wrap(
				boost::bind(&Connection::handle_write, shared_from_this(),
				boost::asio::placeholders::error)));
		}
		catch (RequestHandler::ParseError& e)
		{
			std::cout << std::endl << e.what() << std::endl;
			shutdown();
		}
		catch (RequestHandler::StorageError &e)
		{
			std::cout << std::endl << e.what() << std::endl;
			shutdown();
		}
	}
	else
	{
		std::cout << e << ":" << e.message() << std::endl;
		shutdown();
	}
}

void Connection::handle_write(const boost::system::error_code& e)
{
	if (!e)
	{
		// wait for more content
		socket_.async_read_some(boost::asio::buffer(buffer_),
			strand_.wrap(
			boost::bind(&Connection::handle_read, shared_from_this(),
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred)));
	}
	else
	{
		std::cout << e << ":" << e.message() << std::endl;
		shutdown();
	}
}

void Connection::handle_shutdown(const boost::system::error_code& e)
{
	// do nothing regardless of success or error

	// No new asynchronous operations are started. This means that all shared_ptr
	// references to the connection object will disappear and the object will be
	// destroyed automatically after this handler returns. The connection class's
	// destructor closes the socket.
}

std::string getCN(X509 *cert);

bool Connection::verify_certificate(bool preverified,
	boost::asio::ssl::verify_context& ctx)
{
	// The verify callback can be used to check whether the certificate that is
	// being presented is valid for the peer. For example, RFC 2818 describes
	// the steps involved in doing this for HTTPS. Consult the OpenSSL
	// documentation for more details. Note that the callback is called once
	// for each certificate in the certificate chain, starting from the root
	// certificate authority.


	//int8_t subject_name[256];
	X509_STORE_CTX *cts = ctx.native_handle();
	//int32_t length = 0;
	X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
	//std::cout << "CTX ERROR : " << cts->error << std::endl;

	//int32_t depth = X509_STORE_CTX_get_error_depth(cts);
	//std::cout << "CTX DEPTH : " << depth << std::endl;

	switch (cts->error)
	{
	case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
		//std::cout << "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT" << std::endl;
		break;
	case X509_V_ERR_CERT_NOT_YET_VALID:
	case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
		//std::cout << "Certificate not yet valid!!" << std::endl;
		break;
	case X509_V_ERR_CERT_HAS_EXPIRED:
	case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
		//std::cout << "Certificate expired.." << std::endl;
		break;
	case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:
		//std::cout << "Self signed certificate in chain!!!" << std::endl;
		preverified = true;
		break;
	default:
		break;
	}
	//const int32_t name_length = 256;
	//X509_NAME_oneline(X509_get_subject_name(cert), reinterpret_cast<char*>(subject_name), name_length);
	//std::cout << "Verifying " << subject_name << std::endl;
	//std::cout << "Verification status : " << preverified << std::endl;
	try{
		remoteName = getCN(cert);
	}
	catch (...)
	{
		return false;
	}
	return preverified;
}

std::string getCN(X509 *cert)
{
	auto subjName = X509_get_subject_name(cert);
	int idx;

	if (!subjName)
		throw std::runtime_error("X509_get_subject_name failed");

	idx = X509_NAME_get_index_by_NID(subjName, NID_commonName, -1);
	X509_NAME_ENTRY *entry = X509_NAME_get_entry(subjName, idx);
	ASN1_STRING *entryData = X509_NAME_ENTRY_get_data(entry);
	unsigned char *utf8;
	int length = ASN1_STRING_to_UTF8(&utf8, entryData);

	std::string CommonName((char*)utf8, length);

	OPENSSL_free(utf8);

	return CommonName;
}
