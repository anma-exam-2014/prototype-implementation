﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{
    class Utilities
    {
        public static UInt16 Decode_UINT16(byte[] bytes, ref int offset)
        {
            UInt16 temp = 0;
            temp = (ushort)(bytes[offset+1] & 0xFF);
            temp |= (ushort)(bytes[offset] << 8);

            offset += 2;
	        return temp;
        }


        public static UInt32 Decode_UINT32(byte[] y, ref int offset)
        {
            UInt32 x = 0;

            uint FF = 0xFF;

            x = y[offset];
            x = ((x << 8) | (y[offset + 1] & FF));
            x = ((x << 8) | (y[offset + 2] & FF));
            x = ((x << 8) | (y[offset + 3] & FF));

            offset += 4;

            return x;
        }

        public static UInt64 Decode_UINT64(byte[] y, ref int offset)
        {
            //offset += 8;
            //return BitConverter.ToUInt64(y, offset-8);
            UInt64 x = 0;

            ulong FF = 0xFF;

            x = y[offset];
            x = ((x << 8) | (y[offset + 1] & FF));
            x = ((x << 8) | (y[offset + 2] & FF));
            x = ((x << 8) | (y[offset + 3] & FF));
            x = ((x << 8) | (y[offset + 4] & FF));
            x = ((x << 8) | (y[offset + 5] & FF));
            x = ((x << 8) | (y[offset + 6] & FF));
            x = ((x << 8) | (y[offset + 7] & FF));

            offset += 8;

            return x;
        }

    }

    public static class MyExtensionMethods
    {
        public static byte[] GetBytes(this byte[] buffer, ref int offset, int number)
        {
            byte[] result = buffer.Skip(offset).Take(number).ToArray();
            offset += number;
            return result;
        }

        public static string ToMyFormat(this TimeSpan t)
        {
            string ret = "";
            if (t.Days > 0)
                ret += t.Days + " days ";
            if (t.Hours > 0)
                ret += t.Hours + " hours ";
            if (t.Minutes > 0)
                ret += t.Minutes + " minutes ";

            ret += t.Seconds + "." + t.Milliseconds.ToString("D3") + " seconds ";

            return ret;
        }
    }
}
