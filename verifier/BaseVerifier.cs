﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{
    public abstract class BaseVerifier
    {
        public abstract void Verify(LogItem entry);
    }
}
