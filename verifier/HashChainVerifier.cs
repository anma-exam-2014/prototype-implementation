﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace verifier
{
    public class HashChainVerifier : BaseVerifier
    {
        private string previousHash;
        private bool hasKey;

        string Key;

        public HashChainVerifier()
        {
            hasKey = false;
        }

        public override void Verify(LogItem entry)
        {
            entry.ResetVerificationStatus();
            var details = new SigInfoStruct() { Message = "" };
            entry.SigInfoStruct = details;
            var data = entry.StructuredDataDict["HASHCHAINSIGNATURE@41717"];
            if(hasKey)
            {
                //let's verify stream integrity
                if (previousHash == null || !data.ContainsKey("PREVIOUS"))
                {
                    entry.VerificationWarning = "Data neccessary for verification of previous message is missing. Stream integrity may be compromised! ";
                }
                else if (previousHash != data["PREVIOUS"])
                {
                    entry.VerificationWarning = "Hash of previous message failed to verify. Stream integrity may be compromised! ";
                }

                //lets verify the actual signature 
                doVerifySignature(entry);
            }
            else
            {
                if (data.ContainsKey("KEY_HASH") && entry.Content == "New HashChainSigner key synchronized")
                {
                    Key = data["KEY_HASH"];
                    hasKey = true;
                }
                else
                {
                    //Error
                }
            }
        }

        private bool doVerifySignature(LogItem entry)
        {
            var data = entry.StructuredDataDict["HASHCHAINSIGNATURE@41717"];
            string frameAsString = entry.ToString();
            byte[] frameWithSignature = Encoding.ASCII.GetBytes(frameAsString);
            byte[] frameWithoutSignature = Encoding.ASCII.GetBytes(frameAsString.Replace(data["SIGNATURE"], ""));

            //we will used this hash on the next message to verify stream integrity on the next message
            previousHash = BitConverter.ToString(new SHA256Managed().ComputeHash(frameWithSignature)).Replace("-", "").ToLower();

            //verify signature
            byte[] hash = new SHA256Managed().ComputeHash(frameWithoutSignature);
            byte[] keyTmp = StringToByteArray(Key);
            for (int i = 0; i < 1000; i++)
            {
                using (var hmac = new HMACSHA256(keyTmp))
                {
                    byte[] mac = hmac.ComputeHash(hash);
                    string hexMac = BitConverter.ToString(mac).Replace("-", "").ToLower();

                    keyTmp = new SHA256Managed().ComputeHash(keyTmp);

                    if (hexMac.Equals(data["SIGNATURE"]))
                    {
                        //success!
                        entry.SetVerificationOk();

                        if (i > 0)
                        {
                            entry.VerificationWarning += "Signature was succesfully validated only when compensating for " + i + " missing messages!";
                        }

                        //save next key
                        Key = BitConverter.ToString(keyTmp).Replace("-", "").ToLower();
                        return true;
                    }
                }
            }

            //we update the key once to not fall out of sync
            keyTmp = new SHA256Managed().ComputeHash(StringToByteArray(Key));
            Key = BitConverter.ToString(keyTmp).Replace("-", "").ToLower();

            //failure
            entry.SetVerificationError("Signature verification failed!");
            return false;
        }

        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        private class SigInfoStruct : LogItem.BaseSigInfoStruct
        {
            public string Message;

            public override string ToString()
            {
                return Message;
            }
        }
    }
}
