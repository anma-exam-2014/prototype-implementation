﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{
    public class LogItem
    {
        public FACILITY_NUMBER FacilityNumber { get; set; }
        public SEVERITY_NUMBER SeverityNumber { get; set; }
        public string Timestamp { get; set; }
        public string Hostname { get; set; }
        public string Appname { get; set; }
        public string ProcId { get; set; }
        public string MessageType { get; set; }
        public string StructuredData { get; set; }
        public string StructuredDataOriginal { get; private set; }
        public Dictionary<string, Dictionary<string, string>> StructuredDataDict { get; private set; }
        public string Content { get; set; }
        public VerificationStatusType VerificationStatus { get; set; }
        public BaseSigInfoStruct SigInfoStruct { get; set; }
        public string SigInfo
        {
            get
            {
                if (SigInfoStruct == null)
                    return "Signature information will appear here if found during the verification process.";

                string ret = "### Verification status ###" + Environment.NewLine;
                ret += VerificationStatusString + Environment.NewLine;
                if (!String.IsNullOrEmpty(VerificationWarning))
                {
                    ret += "Warning: " + VerificationWarning + Environment.NewLine;
                }
                return ret + SigInfoStruct.ToString();
            }
        }

        public class BaseSigInfoStruct
        {
            //We only use this for it's overriden ToString method
        }

        private string errorMsg;
        public string VerificationStatusString
        {
            get
            {
                string ret = "";
                if (VerificationStatus == VerificationStatusType.Failed)
                    ret = errorMsg;
                else
                    ret = VerificationStatus.ToString();

                if (!String.IsNullOrEmpty(VerificationWarning))
                {
                    ret += " + Warning";
                }

                return ret;
            }
        }
        public string VerificationWarning { get; set; }

        public void SetVerificationError(string msg)
        {
            errorMsg = msg;
            VerificationStatus = VerificationStatusType.Failed;
        }

        public void SetVerificationOk()
        {
            VerificationStatus = VerificationStatusType.Ok;
        }

        public void ResetVerificationStatus()
        {
            VerificationWarning = null;
            VerificationStatus = VerificationStatusType.Unverified;
        }

        public LogItem(FACILITY_NUMBER fc, SEVERITY_NUMBER sn, string timestamp, string hostname, string appname, string procid, string message_type, string structured_data, string content)
        {
            this.VerificationStatus = VerificationStatusType.Unverified;
            this.FacilityNumber = fc;
            this.SeverityNumber = sn;
            this.Timestamp = timestamp;
            this.Hostname = hostname;
            this.Appname = appname;
            this.ProcId = procid;
            this.MessageType = message_type;
            this.StructuredData = structured_data;
            this.StructuredDataOriginal = structured_data;
            this.Content = content;

            StructuredDataDict = new Dictionary<string, Dictionary<string, string>>();
            ParseStructuredData();
        }

        private enum PARSE_STATE{
            BEGIN,
            NAME,
            KEY,
            QUOTE,
            VALUE,
            VALUE_ESCAPE,
            VALUE_FINISHED
        };

        public enum SignatureType
        {
            HashChains,
            PublicKey,
            Rsa,
            TpmRsa
        }

        public enum VerificationStatusType
        {
            Unverified,
            Ok,
            Failed
        }

        public SignatureType? GetSignatureType()
        {
            if (StructuredDataDict.ContainsKey("HASHCHAINSIGNATURE@41717"))
            {
                return SignatureType.HashChains;
            }
            else if (StructuredDataDict.ContainsKey("RSASIGNATURE@41717"))
            {
                return SignatureType.Rsa;
            }
            else if (StructuredDataDict.ContainsKey("TPMRSASIGNATURE@41717"))
            {
                return SignatureType.TpmRsa;
            }
            else if (StructuredDataDict.ContainsKey("ITKISREYZINSIGNATURE@41717"))
            {
                return SignatureType.PublicKey;
            }

            return null;
        }

        public class ParseError : Exception { }

        private void ParseStructuredData()
        {
            PARSE_STATE state = PARSE_STATE.BEGIN;
            string currentName = "";
            string currentKey = "";
            string currentValue = "";

            if (StructuredData == "-")
                return;

            //for each data structure
            foreach (char c in StructuredData)
            {
                switch (state)
                {
                    case PARSE_STATE.BEGIN:
                        if (c != '[')
                            throw new ArgumentException("expected '[' but got '" + c + "'");
                        state = PARSE_STATE.NAME;
                        currentName = "";
                        currentKey = "";
                        currentValue = "";
                        break;
                    case PARSE_STATE.NAME:
                        if(c == ' ')
                        {
                            state = PARSE_STATE.VALUE_FINISHED;
                            StructuredDataDict[currentName] = new Dictionary<string, string>();
                        }
                        else
                        {
                            currentName += c;
                        }
                        break;
                    case PARSE_STATE.KEY:
                        if(c == '=')
                        {
                            state = PARSE_STATE.QUOTE;
                        }
                        else if(c != ' ')
                        {
                            currentKey += c;
                        }
                        else if(currentKey.Count() != 0)
                        {
                            throw new ArgumentException("Encountered space while reading key name");
                        }
                        break;
                    case PARSE_STATE.QUOTE:
                        if (c != '"')
                            throw new ArgumentException("excpected '=' but got '" + c + "'");
                        state = PARSE_STATE.VALUE;
                        break;
                    case PARSE_STATE.VALUE:
                        if(c == '\\')
                        {
                            state = PARSE_STATE.VALUE_ESCAPE;
                        }
                        else if(c == '"')
                        {
                            state = PARSE_STATE.VALUE_FINISHED;
                            StructuredDataDict[currentName][currentKey] = currentValue;
                            currentKey = "";
                            currentValue = "";
                        }
                        else
                        {
                            currentValue += c;
                        }
                        break;
                    case PARSE_STATE.VALUE_ESCAPE:
                        if (c != '\\' && c != '"' && c != ']')
                            currentValue += '\\';
                        currentValue += c;
                        state = PARSE_STATE.VALUE;
                        break;
                    case PARSE_STATE.VALUE_FINISHED:
                        switch (c)
                        {
                            case ' ':
                                state = PARSE_STATE.KEY;
                                break;
                            case ']':
                                currentName = "";
                                state = PARSE_STATE.BEGIN;
                                break;
                            default:
                                state = PARSE_STATE.KEY;
                                currentKey += c;
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public string[] ToStringArray()
        {
            //return new string[] { FacilityNumber.ToString(), SeverityNumber.ToString(), Timestamp, Hostname, Appname, ProcId, MessageType, Content, StructuredData };
            return new string[] { VerificationStatusString, FacilityNumber.ToString(), SeverityNumber.ToString(), Timestamp, Content };
        }

        public byte[] GenerateFrame(bool originalData = false)
        {
            StringBuilder sb = new StringBuilder(300);
            sb.Append("<" + (((int)FacilityNumber) * 8 + ((int)SeverityNumber)) + ">");
            sb.Append("1");
            sb.Append(" ");
            sb.Append(Timestamp);
            sb.Append(" ");
            sb.Append(Hostname);
            sb.Append(" ");
            sb.Append(Appname);
            sb.Append(" ");
            sb.Append(ProcId);
            sb.Append(" ");
            sb.Append(MessageType);
            sb.Append(" ");

            if (originalData)
                sb.Append(StructuredDataOriginal);
            else
                sb.Append(StructuredData);

            sb.Append(" ");
            sb.Append(Content);

            string tmp = sb.ToString();
            byte[] bytes = new byte[tmp.Length * sizeof(char)];
            System.Buffer.BlockCopy(tmp.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public string ToString(bool originalData = false)
        {
            StringBuilder sb = new StringBuilder(300);
            sb.Append("<" + (((int)FacilityNumber) * 8 + ((int)SeverityNumber)) + ">");
            sb.Append("1");
            sb.Append(" ");
            sb.Append(Timestamp);
            sb.Append(" ");
            sb.Append(Hostname);
            sb.Append(" ");
            sb.Append(Appname);
            sb.Append(" ");
            sb.Append(ProcId);
            sb.Append(" ");
            sb.Append(MessageType);
            sb.Append(" ");

            if (originalData)
                sb.Append(StructuredDataOriginal);
            else
                sb.Append(StructuredData);

            sb.Append(" ");
            sb.Append(Content);

            return sb.ToString();
        }

        public enum FACILITY_NUMBER
        {
            //Kernel messages
            FN_KERN = 0,
            //User-level messages
            FN_USER,
            //mail system
            FN_MAIL,
            //system daemons
            FN_DAEMON,
            //security/authorization messages
            FN_AUTH,
            //Messages generated internally by syslogd
            FN_SYSLOG,
            //line printer subsystem
            FN_LPR,
            //network news subsystem
            FN_NEWS,
            //UUCP subsystem
            FN_UUCP,
            //clock daemon
            FN_CLOCK,
            //security/authorization messages
            FN_AUTHPRIV,
            //FTP daemon
            FN_FTP,
            //NTP subsystem
            FN_NTP,
            //log audit
            FN_LOG_AUDIT,
            //log alert
            FN_LOG_ALERT,
            //clock daemon
            FN_CRON,
            //local use 0
            FN_LOCAL0,
            //local use 1
            FN_LOCAL1,
            //local use 2
            FN_LOCAL2,
            //local use 3
            FN_LOCAL3,
            //local use 4
            FN_LOCAL4,
            //local use 5
            FN_LOCAL5,
            //local use 6
            FN_LOCAL6,
            //local use 7
            FN_LOCAL7
        }

        public enum SEVERITY_NUMBER
        {
            Emergency = 0,
            Alert,
            Critical,
            Error,
            Warning,
            Notice,
            Informational,
            Debug
        }
    }
}
