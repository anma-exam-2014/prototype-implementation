﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{

    class TpmRsaVerifier : BaseVerifier
    {
        private bool hasInitMessage = false;
        private RSAParameters signKey;
        private long? previousBatchId = null;
        private long? previousSuccessfullBatchId = null;
        private const int NONCE_LENGTH = 20;
        private const int DIGEST_LENGTH = 20;

        private string aggregatedMessageString = "";
        private byte[] hashToMatch;
        private int batchsize;
        SigInfoStruct signInfo;

        static string DebugTickRAte = "";

        public TpmRsaVerifier()
        {

        }

        public override void Verify(LogItem entry)
        {
            long? processedBatchId = _Verify(entry);
            if (processedBatchId != null)
                previousBatchId = processedBatchId;
        }

        private long? _Verify(LogItem entry)
        {
            entry.ResetVerificationStatus();
            long? batchid = null;
            try
            {
                var data = entry.StructuredDataDict["TPMRSASIGNATURE@41717"];
                if (data == null)
                {
                    entry.SetVerificationError("No signature data!");
                    return null;
                }

                batchid = Int64.Parse(data["BATCH"]);

                if (batchid == 0)
                {
                    //special init message!

                    if (data["VERSION"] != "1")
                    {
                        entry.SetVerificationError("unsupported version detected!");
                        return batchid;
                    }

                    signKey = new RSAParameters();
                    signKey.Modulus = System.Convert.FromBase64String(data["SIGN_KEY_MODULUS"]);
                    signKey.Exponent = System.Convert.FromBase64String(data["SIGN_KEY_EXPONENT"]);

                    //we cannot fail verifying message zero since it contains information needed by all other 
                    //messages.
                    hasInitMessage = true;
                }
                else
                {
                    if(!hasInitMessage)
                    {
                        entry.SetVerificationError("Missing message with batch id 0");
                        return batchid;
                    }

                    //normal log message
                    int index = Int16.Parse(data["INDEX"]);

                    //first message in batch
                    if(index == 0)
                    {
                        //do some resets
                        aggregatedMessageString = "";
                        hashToMatch = new byte[0];

                        batchsize = Int16.Parse(data["BATCH_SIZE"]);

                        //check stream status
                        if (previousBatchId + 1 != batchid)
                        {
                            entry.VerificationWarning = (batchid - previousBatchId - 1) + " previous message(s) in database is missing!";
                        }
                        //if (previousSuccessfullBatchId + 1 != batchid)
                        //{
                        //    entry.SetVerificationError((batchid - previousSuccessfullBatchId - 1) + " previous message(s) in database failed to validate!");
                        //    return batchid;
                        //}

                        SigInfoStruct newSignInfo = new SigInfoStruct(System.Convert.FromBase64String(data["SIG_INFO"]));
                        newSignInfo.previousSigInfoStruct = signInfo;
                        newSignInfo.system_time = DateTime.Parse(entry.Timestamp, CultureInfo.InvariantCulture);
                        signInfo = newSignInfo;

                        SigStruct signature = new SigStruct(System.Convert.FromBase64String(data["SIG"]));

                        //example from http://msdn.microsoft.com/en-us/library/201yh4c4(v=vs.90).aspx
                        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                        RSA.ImportParameters(signKey);
                        RSAPKCS1SignatureDeformatter RSADeformatter = new RSAPKCS1SignatureDeformatter(RSA);
                        RSADeformatter.SetHashAlgorithm("SHA1");
                        if (!RSADeformatter.VerifySignature(signInfo.total_hash, signature.raw_data))
                        {
                            entry.SetVerificationError("Signature could not be verified.");
                            return batchid;
                        }

                        //the signature is correct, now we must check if the rest of the message corresponds to the included hash message

                        //remove the signature information
                        entry.StructuredData = entry.StructuredData.Replace(data["SIG_INFO"], "");
                        entry.StructuredData = entry.StructuredData.Replace(data["SIG"], "");

                        hashToMatch = signInfo.data_hash;
                    }

                    //do this for all messages
                    aggregatedMessageString += entry.ToString();
                    entry.SigInfoStruct = signInfo;

                    //last message in batch
                    if(index == batchsize-1)
                    {
                        byte[] tt = Encoding.ASCII.GetBytes(aggregatedMessageString);
                        //get hash of log message
                        byte[] hash = new SHA1CryptoServiceProvider().ComputeHash(tt);

                        if (!hash.SequenceEqual(hashToMatch))
                        {
                            entry.SetVerificationError("Log message data failed verification, but tick-stamp, tick-rate and tick-nonce data succeeded verification.");
                            return batchid;
                        }

                        //Success!
                        previousBatchId = batchid;
                    }

                }

                //verification successfull
                entry.SetVerificationOk();
                previousSuccessfullBatchId = batchid;
                return batchid;
            }
            catch(KeyNotFoundException)
            {
                entry.SetVerificationError("Missing data in signature!");
                return batchid;
            }
            catch(Exception e)
            {
                entry.SetVerificationError(e.Message);
                return batchid;
            }
        }

        class SigStruct
        {
            public byte[] raw_data;

            public SigStruct(byte[] sig)
            {
                raw_data = sig;
            }
        }

        class SigInfoStruct : verifier.LogItem.BaseSigInfoStruct
        {
            public UInt16 tpm_sign_info_tag;
            public string tpm_sign_info_fixed;
            public byte[] tpm_sign_info_replay;
            public UInt32 tpm_sign_info_data_length;
            public byte[] data_hash;
            public UInt16 tpm_current_ticks_tag;
            public UInt64 tpm_current_ticks_currentTicks;
            public byte[] tpm_current_ticks_currentTicksBytes;
            public UInt16 tpm_current_ticks_tickRate;
            public byte[] tpm_current_ticks_nonce;
            public byte[] total_hash;

            public byte[] raw_data;
            public DateTime? system_time = null;

            public SigInfoStruct previousSigInfoStruct = null;

            public SigInfoStruct(byte[] signInfo)
            {
                raw_data = signInfo;
                int offset = 0;

                //check TPM_TAG_SIGNINFO
                tpm_sign_info_tag = Utilities.Decode_UINT16(signInfo, ref offset);
                if (tpm_sign_info_tag != 0x0005)
                    throw new ArgumentException("SIG_INFO is corrupt: Expected value 0x0005, in bytes 0-1!");

                //Check fixed
                tpm_sign_info_fixed = System.Text.Encoding.ASCII.GetString(signInfo.GetBytes(ref offset, 4));
                if (tpm_sign_info_fixed != "TSTP")
                    throw new ArgumentException("SIG_INFO is corrupt: Expected value 'TSTP' in bytes 2-5!");

                tpm_sign_info_replay = signInfo.GetBytes(ref offset, NONCE_LENGTH);
                tpm_sign_info_data_length = Utilities.Decode_UINT32(signInfo, ref offset);

                //only data left, do simple sanity check
                if (signInfo.Length - offset - tpm_sign_info_data_length != 0)
                    throw new ArgumentException("SIG_INFO is corrupt: data length does not match actual value!");

                //get hash value
                data_hash = signInfo.GetBytes(ref offset, DIGEST_LENGTH);

                //get ticks
                tpm_current_ticks_tag = Utilities.Decode_UINT16(signInfo, ref offset);
                tpm_current_ticks_currentTicks = Utilities.Decode_UINT64(signInfo, ref offset); offset -= 8;
                tpm_current_ticks_currentTicksBytes = signInfo.GetBytes(ref offset, 8);
                tpm_current_ticks_tickRate = (ushort)(Utilities.Decode_UINT16(signInfo, ref offset));
                tpm_current_ticks_nonce = signInfo.GetBytes(ref offset, NONCE_LENGTH);

                if (offset != signInfo.Length)
                    throw new ArgumentException("SIG_INFO is corrupt: data length does not match actual value!");

                SHA1 sha = new SHA1CryptoServiceProvider();
                total_hash = sha.ComputeHash(raw_data);
            }

            public string timeSinceBoot()
            {
                UInt64 microSeconds = tpm_current_ticks_currentTicks / tpm_current_ticks_tickRate;
                var timeSpan = TimeSpan.FromMilliseconds(microSeconds / 1000);
                return String.Format(
                    "{0} days, {1} hours, {2} minutes and {3}.{4} seconds", 
                    timeSpan.Days, 
                    timeSpan.Hours, 
                    timeSpan.Minutes, 
                    timeSpan.Seconds,
                    timeSpan.Milliseconds);
            }

            //this is to provide more information when the item is double clicked
            public override string ToString()
            {
                string tostring = String.Format(
                    "### Parsed data from SIG_INFO ###" + Environment.NewLine +
                    "TPM Signature anti replay Nonce: {0}" + Environment.NewLine +
                    "TPM Tick Count: {1}" + Environment.NewLine +
                    "TPM Tick Rate: {2} tick per µs" + Environment.NewLine +
                    "TPM System Restart Nonce: {3}" + Environment.NewLine, 
                    BitConverter.ToString(tpm_sign_info_replay),
                    tpm_current_ticks_currentTicks,
                    tpm_current_ticks_tickRate,
                    BitConverter.ToString(tpm_current_ticks_nonce));

                if (previousSigInfoStruct != null)
                {
                    tostring += "### Calculated values ###" + Environment.NewLine;
                    TimeSpan sysTimeDiff = (system_time - previousSigInfoStruct.system_time).GetValueOrDefault();
                    var diffMicro = sysTimeDiff.TotalMilliseconds * 1000;
                    var diffTicks = tpm_current_ticks_currentTicks - previousSigInfoStruct.tpm_current_ticks_currentTicks;
                    var CalculatedTickRate = diffTicks / diffMicro;
                    TimeSpan ExpectedSysTimeDiff = TimeSpan.FromMilliseconds(diffTicks / (UInt64)(tpm_current_ticks_tickRate * 1000));
                    var TickRateErrorRatio = ExpectedSysTimeDiff.TotalMilliseconds / sysTimeDiff.TotalMilliseconds;
                    tostring += String.Format("Time difference of this and previous message (System Time): {0}" + Environment.NewLine, sysTimeDiff.ToMyFormat());
                    tostring += String.Format("Expected time difference (based on TPM Tick Count and TPM Tick Rate): {0}" + Environment.NewLine, ExpectedSysTimeDiff.ToMyFormat());
                    tostring += String.Format("Caclulated Tick Rate (based on TPM Tick Count and system time): {0}" + Environment.NewLine, CalculatedTickRate);
                    tostring += String.Format("Caclulated error ratio of TPM Tick Rate (based on real and expected System Time Difference): {0}" + Environment.NewLine, TickRateErrorRatio);

                    DebugTickRAte += String.Format("{0} {1} {2}" + Environment.NewLine, tpm_current_ticks_tickRate, diffTicks, diffMicro);
                }

                return tostring;
            }
        }
    }
}
