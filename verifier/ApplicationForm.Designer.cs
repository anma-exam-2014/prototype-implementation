﻿namespace verifier
{
    partial class SecureLogVerifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecureLogVerifier));
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.MenuStripFile = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.verifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findNextErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findPreviousErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.ToolStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStrip = new System.Windows.Forms.ToolStrip();
            this.ToolStripOpenFile = new System.Windows.Forms.ToolStripButton();
            this.ToolStripVerify = new System.Windows.Forms.ToolStripButton();
            this.ToolStripTruncationCheck = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.findPreviousError = new System.Windows.Forms.ToolStripButton();
            this.findNextError = new System.Windows.Forms.ToolStripButton();
            this.MainListView = new System.Windows.Forms.ListView();
            this.Signature = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Facility = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Severity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Timestamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Content = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.MenuStrip.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.ToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStripFile,
            this.MenuStripEdit,
            this.MenuStripHelp});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(873, 24);
            this.MenuStrip.TabIndex = 0;
            this.MenuStrip.Text = "menuStrip";
            // 
            // MenuStripFile
            // 
            this.MenuStripFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.MenuStripFile.Name = "MenuStripFile";
            this.MenuStripFile.Size = new System.Drawing.Size(37, 20);
            this.MenuStripFile.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.ToolStripOpenFile_Click);
            // 
            // MenuStripEdit
            // 
            this.MenuStripEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verifyToolStripMenuItem,
            this.findNextErrorToolStripMenuItem,
            this.findPreviousErrorToolStripMenuItem});
            this.MenuStripEdit.Name = "MenuStripEdit";
            this.MenuStripEdit.Size = new System.Drawing.Size(39, 20);
            this.MenuStripEdit.Text = "Edit";
            // 
            // verifyToolStripMenuItem
            // 
            this.verifyToolStripMenuItem.Name = "verifyToolStripMenuItem";
            this.verifyToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.verifyToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.verifyToolStripMenuItem.Text = "Verify";
            this.verifyToolStripMenuItem.Click += new System.EventHandler(this.ToolStripVerify_Click);
            // 
            // findNextErrorToolStripMenuItem
            // 
            this.findNextErrorToolStripMenuItem.Name = "findNextErrorToolStripMenuItem";
            this.findNextErrorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.findNextErrorToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.findNextErrorToolStripMenuItem.Text = "Find Next Error";
            this.findNextErrorToolStripMenuItem.Click += new System.EventHandler(this.findNextError_Click);
            // 
            // findPreviousErrorToolStripMenuItem
            // 
            this.findPreviousErrorToolStripMenuItem.Name = "findPreviousErrorToolStripMenuItem";
            this.findPreviousErrorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.findPreviousErrorToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.findPreviousErrorToolStripMenuItem.Text = "Find Previous Error";
            this.findPreviousErrorToolStripMenuItem.Click += new System.EventHandler(this.findPreviousError_Click);
            // 
            // MenuStripHelp
            // 
            this.MenuStripHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.MenuStripHelp.Name = "MenuStripHelp";
            this.MenuStripHelp.Size = new System.Drawing.Size(44, 20);
            this.MenuStripHelp.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel});
            this.StatusStrip.Location = new System.Drawing.Point(0, 548);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(873, 22);
            this.StatusStrip.TabIndex = 1;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // ToolStripLabel
            // 
            this.ToolStripLabel.Name = "ToolStripLabel";
            this.ToolStripLabel.Size = new System.Drawing.Size(106, 17);
            this.ToolStripLabel.Text = "<Status text here!>";
            // 
            // ToolStrip
            // 
            this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripOpenFile,
            this.ToolStripVerify,
            this.ToolStripTruncationCheck,
            this.toolStripSeparator1,
            this.findPreviousError,
            this.findNextError});
            this.ToolStrip.Location = new System.Drawing.Point(0, 24);
            this.ToolStrip.Name = "ToolStrip";
            this.ToolStrip.Size = new System.Drawing.Size(873, 25);
            this.ToolStrip.TabIndex = 2;
            this.ToolStrip.Text = "toolStrip1";
            // 
            // ToolStripOpenFile
            // 
            this.ToolStripOpenFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripOpenFile.Image")));
            this.ToolStripOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripOpenFile.Name = "ToolStripOpenFile";
            this.ToolStripOpenFile.Size = new System.Drawing.Size(23, 22);
            this.ToolStripOpenFile.Text = "Open";
            this.ToolStripOpenFile.Click += new System.EventHandler(this.ToolStripOpenFile_Click);
            // 
            // ToolStripVerify
            // 
            this.ToolStripVerify.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripVerify.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripVerify.Image")));
            this.ToolStripVerify.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripVerify.Name = "ToolStripVerify";
            this.ToolStripVerify.Size = new System.Drawing.Size(23, 22);
            this.ToolStripVerify.Text = "Verify";
            this.ToolStripVerify.Click += new System.EventHandler(this.ToolStripVerify_Click);
            // 
            // ToolStripTruncationCheck
            // 
            this.ToolStripTruncationCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripTruncationCheck.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripTruncationCheck.Image")));
            this.ToolStripTruncationCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripTruncationCheck.Name = "ToolStripTruncationCheck";
            this.ToolStripTruncationCheck.Size = new System.Drawing.Size(23, 22);
            this.ToolStripTruncationCheck.Text = "Check truncation";
            this.ToolStripTruncationCheck.Click += new System.EventHandler(this.ToolStripTruncationCheck_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // findPreviousError
            // 
            this.findPreviousError.Name = "findPreviousError";
            this.findPreviousError.Size = new System.Drawing.Size(133, 22);
            this.findPreviousError.Text = "Find Prevoius Error (F2)";
            this.findPreviousError.Click += new System.EventHandler(this.findPreviousError_Click);
            // 
            // findNextError
            // 
            this.findNextError.Name = "findNextError";
            this.findNextError.Size = new System.Drawing.Size(112, 22);
            this.findNextError.Text = "Find Next Error (F3)";
            this.findNextError.Click += new System.EventHandler(this.findNextError_Click);
            // 
            // MainListView
            // 
            this.MainListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Signature,
            this.Facility,
            this.Severity,
            this.Timestamp,
            this.Content});
            this.MainListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainListView.FullRowSelect = true;
            this.MainListView.GridLines = true;
            this.MainListView.Location = new System.Drawing.Point(0, 49);
            this.MainListView.Name = "MainListView";
            this.MainListView.Size = new System.Drawing.Size(873, 499);
            this.MainListView.TabIndex = 3;
            this.MainListView.UseCompatibleStateImageBehavior = false;
            this.MainListView.View = System.Windows.Forms.View.Details;
            this.MainListView.DoubleClick += new System.EventHandler(this.MainListView_MouseDoubleClick);
            this.MainListView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainListView_KeyPress);
            // 
            // Signature
            // 
            this.Signature.Text = "Signature";
            this.Signature.Width = 94;
            // 
            // Facility
            // 
            this.Facility.Text = "Facility";
            this.Facility.Width = 90;
            // 
            // Severity
            // 
            this.Severity.Text = "Severity";
            this.Severity.Width = 96;
            // 
            // Timestamp
            // 
            this.Timestamp.Text = "Timestamp";
            this.Timestamp.Width = 148;
            // 
            // Content
            // 
            this.Content.Text = "Content";
            this.Content.Width = 441;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // SecureLogVerifier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 570);
            this.Controls.Add(this.MainListView);
            this.Controls.Add(this.ToolStrip);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.MenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.Name = "SecureLogVerifier";
            this.Text = "Log Verifier";
            this.Load += new System.EventHandler(this.SecureLogVerifier_Load);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ToolStrip.ResumeLayout(false);
            this.ToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuStripFile;
        private System.Windows.Forms.ToolStripMenuItem MenuStripEdit;
        private System.Windows.Forms.ToolStripMenuItem MenuStripHelp;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel ToolStripLabel;
        private System.Windows.Forms.ToolStrip ToolStrip;
        private System.Windows.Forms.ToolStripButton ToolStripOpenFile;
        private System.Windows.Forms.ToolStripButton ToolStripVerify;
        private System.Windows.Forms.ListView MainListView;
        private System.Windows.Forms.ColumnHeader Signature;
        private System.Windows.Forms.ColumnHeader Facility;
        private System.Windows.Forms.ColumnHeader Severity;
        private System.Windows.Forms.ColumnHeader Timestamp;
        private System.Windows.Forms.ColumnHeader Content;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton findNextError;
        private System.Windows.Forms.ToolStripButton findPreviousError;
        private System.Windows.Forms.ToolStripMenuItem findNextErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findPreviousErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ToolStripTruncationCheck;
    }
}