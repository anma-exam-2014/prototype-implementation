﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using ProgressTest;

namespace verifier
{
    public partial class SecureLogVerifier : Form
    {
        private LogItem[] CurrentLog = null;
        //private VerificationKey[] Keys = null;
        private HashChainVerifier mHashChainVerifier;
        private RsaVerifier mRsaVerifier;
        private TpmRsaVerifier mTpmRsaVerifier;
        private ItkisReyzinVerifier mPublicKeyVerifier;

        public SecureLogVerifier()
        {
            InitializeComponent();
        }

        private void SecureLogVerifier_Load(object sender, EventArgs e)
        {
            UpdateToolbarButtons();
        }

        private void ToolStripOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                CurrentLog = LoadLogDataFromSqlFile(ofd.FileName);
                //Keys = LoadSecretsKeyFromSqlFile(ofd.FileName);
                UpdateToolbarButtons();
                MainListView.Items.Clear();
                foreach (LogItem entry in CurrentLog)
                {
                    ListViewItem item = new ListViewItem(entry.ToStringArray());
                    MainListView.Items.Add(item);
                }
            }
        }

        private void ShowProgressFormWhileBackgroundWorkerRuns()
        {
        }

        private void ToolStripVerify_Click(object sender, EventArgs e)
        {
            mHashChainVerifier = new HashChainVerifier();
            mRsaVerifier = new RsaVerifier();
            mPublicKeyVerifier = new ItkisReyzinVerifier();
            mTpmRsaVerifier = new TpmRsaVerifier();

            ProgressForm form = new ProgressForm();
            form.DoWork += new ProgressForm.DoWorkEventHandler(doVerifyInBackground);

            DialogResult result = form.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                //the user clicked cancel
            }
            else if (result == DialogResult.Abort)
            {
                //an unhandled exception occured in user function
                //you may get the exception information:
                MessageBox.Show(form.Result.Error.Message);
            }
            else if (result == DialogResult.OK)
            {
                //the background worker finished normally
                //the result of the background worker is stored in form.Result
            }

            MainListView.SelectedIndices.Clear();
            //update view based on result
            int i = 0;
            foreach (var item in CurrentLog)
            {
                MainListView.Items[i].Text = item.VerificationStatusString;
                if (item.VerificationStatus == LogItem.VerificationStatusType.Ok) //null means no error
                {
                    if (String.IsNullOrEmpty(item.VerificationWarning))
                    {
                        MainListView.Items[i].ForeColor = Color.Green;
                    }
                    else
                    {
                        MainListView.Items[i].ForeColor = Color.Orange;
                    }   
                }
                else if(item.VerificationStatus == LogItem.VerificationStatusType.Unverified)
                {
                    MainListView.Items[i].ForeColor = Color.Black;
                }
                else
                {
                    MainListView.Items[i].ForeColor = Color.Red;
                    MainListView.Items[i].Selected = true;
                    MainListView.Select();
                }
                i++;
            }
        }

        private void doVerifyInBackground(ProgressTest.ProgressForm sender, DoWorkEventArgs e)
        {
            //MessageBox.Show(CurrentLog[ListView.SelectedItems[0].Index].StructuredData, "Some title", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BaseVerifier verifier = null;
            //BaseVerifier verifier = new HashChainVerifier(Keys);

            int i=0;
            float max = CurrentLog.Length;
            foreach (var item in CurrentLog)
            {
                int percentage = (int)(i / max * 100.0);
                sender.SetProgress(percentage, String.Format("Verifying {0}/{1} ...", i, max));
                switch (item.GetSignatureType())
                {
                    case LogItem.SignatureType.HashChains:
                        if(!(verifier is HashChainVerifier))
                        {
                            verifier = mHashChainVerifier;
                        }
                        break;
                    case LogItem.SignatureType.PublicKey:
                        if(!(verifier is ItkisReyzinVerifier))
                        {
                            verifier = mPublicKeyVerifier;
                        }
                        break;
                    case LogItem.SignatureType.Rsa:
                        if (!(verifier is RsaVerifier))
                        {
                            verifier = mRsaVerifier;
                        }
                        break;
                    case LogItem.SignatureType.TpmRsa:
                        if (!(verifier is TpmRsaVerifier))
                        {
                            verifier = mTpmRsaVerifier;
                        }
                        break;
                    default:
                        verifier = null;
                        break;
                }

                if (verifier != null)
                {
                    verifier.Verify(item);
                }
                else
                {
                    item.SetVerificationError("No signature");
                }

                //check if the user clicked cancel
                if (sender.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                i++;
            }
        }

        private void UpdateToolbarButtons()
        {
            if (CurrentLog == null || CurrentLog.Length == 0)
            {
                ToolStripVerify.Enabled = false;
                verifyToolStripMenuItem.Enabled = false;
                ToolStripTruncationCheck.Enabled = false;
            }
            else
            {
                ToolStripVerify.Enabled = true;
                verifyToolStripMenuItem.Enabled = true;
                ToolStripTruncationCheck.Enabled = true;
            }
        }

        //public VerificationKey[] LoadSecretsKeyFromSqlFile(string path)
        //{
        //    List<VerificationKey> keys = new List<VerificationKey>();

        //    try
        //    {
        //        var datasource = "data source=\"" + path + "\"";
        //        System.Data.SQLite.SQLiteConnection sql = new System.Data.SQLite.SQLiteConnection(datasource);
        //        sql.Open();

        //        using (System.Data.SQLite.SQLiteCommand command = sql.CreateCommand())
        //        {
        //            command.CommandText = @"SELECT * FROM SECRETS;";
        //            command.CommandType = System.Data.CommandType.Text;

        //            System.Data.SQLite.SQLiteDataReader reader;
        //            reader = command.ExecuteReader();
        //            while (reader.Read())
        //            {
        //                VerificationKey key = new VerificationKey(reader.GetString(1), reader.GetString(2), reader.GetDateTime(3));
        //                keys.Add(key);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }

        //    Console.WriteLine("Loaded " + keys.Count + " root key(s).");
        //    return keys.ToArray();
        //}

        public LogItem[] LoadLogDataFromSqlFile(string path)
        {
            List<LogItem> entries = new List<LogItem>();

            try
            {
                var datasource = "data source=\"" + path + "\"";
                System.Data.SQLite.SQLiteConnection sql = new System.Data.SQLite.SQLiteConnection(datasource);
                sql.Open();

                using (System.Data.SQLite.SQLiteCommand command = sql.CreateCommand())
                {
                    /*                              0        1               2              3          4        5       6        7           8               9                     */
                    command.CommandText = @"SELECT ID, FACILITY_NUMBER, SEVERITY_VALUE, TIMESTAMP, HOSTNAME, APPNAME, PROCID, MSG_TYPE, STRUCTURED_DATA, CONTENT FROM LOG_MESSAGE;";
                    command.CommandType = System.Data.CommandType.Text;

                    System.Data.SQLite.SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        LogItem entry = new LogItem((LogItem.FACILITY_NUMBER)reader.GetInt32(1),
                                                (LogItem.SEVERITY_NUMBER)reader.GetInt32(2),
                                                reader.GetString(3),
                                                reader.GetString(4),
                                                reader.GetString(5),
                                                reader.GetString(6),
                                                reader.GetString(7),
                                                reader.GetString(8),
                                                reader.GetString(9));
                        entries.Add(entry);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Console.WriteLine("Loaded " + entries.Count + " entries.");
            return entries.ToArray();
        }

        private void findPreviousError_Click(object sender, EventArgs e)
        {
            int startIndex = MainListView.SelectedIndices.Count - 1;
            if(MainListView.SelectedIndices.Count > 0)
            {
                startIndex = MainListView.SelectedIndices[0] - 1;
            }
            for(int i= startIndex; i >= 0; i--)
            {
                if (!MainListView.Items[i].Text.Equals(LogItem.VerificationStatusType.Ok.ToString()))
                {
                    MainListView.SelectedIndices.Clear();
                    MainListView.SelectedIndices.Add(i);
                    MainListView.Items[i].Selected = true;
                    MainListView.Items[i].Focused = true;
                    MainListView.Select();
                    MainListView.EnsureVisible(i);
                    break;
                }
            }
        }

        private void findNextError_Click(object sender, EventArgs e)
        {
            int startIndex = 0;
            if (MainListView.SelectedIndices.Count > 0)
            {
                startIndex = MainListView.SelectedIndices[0] + 1;
            }
            for (int i = startIndex; i < MainListView.Items.Count; i++)
            {
                if(!MainListView.Items[i].Text.Equals(LogItem.VerificationStatusType.Ok.ToString()))
                {
                    MainListView.SelectedIndices.Clear();
                    MainListView.SelectedIndices.Add(i);
                    MainListView.Items[i].Selected = true;
                    MainListView.Items[i].Focused = true;
                    MainListView.Select();
                    MainListView.EnsureVisible(i);
                    break;
                }
            }
        }

        private void MainListView_MouseDoubleClick(object sender, EventArgs e)
        {
            if (MainListView.SelectedItems.Count == 1)
            {
                LogItem item = CurrentLog[MainListView.SelectedIndices[0]];
                DetailsForm details = new DetailsForm(item);
                details.Show();
            }
        }

        private void MainListView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)13)
            {
                MainListView_MouseDoubleClick(sender, e);
            }
        }

        private void ToolStripTruncationCheck_Click(object sender, EventArgs e)
        {
            const int MAX_DETECTABLE_LOSS = 1000;
            byte[] currentHash = null;
            int counter = 0;
            int result = 0;

            //bool startTagFound = false;
            bool endTagFound = false;

            //foreach (var item in CurrentLog)
            for (int i = 0; i < CurrentLog.Length; i++)
            {
                if (currentHash != null)
                {
                    if (CurrentLog[i].StructuredDataDict.ContainsKey("ANTITRUNCATION@41717") && 
                        CurrentLog[i].StructuredDataDict["ANTITRUNCATION@41717"].ContainsKey("HASH"))
                    {
                        endTagFound = true;

                        byte[] specifiedHash = System.Convert.FromBase64String(CurrentLog[i].StructuredDataDict["ANTITRUNCATION@41717"]["HASH"]);

                        result = isTruncated(ref currentHash, specifiedHash, counter, MAX_DETECTABLE_LOSS);
                        if (result != 0)
                        {
                            MainListView.SelectedIndices.Clear();
                            for (int j = i - counter; j < i; j++ )
                            {
                                MainListView.SelectedIndices.Add(j);
                                MainListView.Items[j].Selected = true;
                                MainListView.Items[j].Focused = true;
                                MainListView.Select();
                                MainListView.EnsureVisible(j);
                            }

                            break;
                        }

                        counter = 0;
                    }
                    else
                    {
                        counter++;
                    }
                }
                else
                {
                    if (CurrentLog[i].StructuredDataDict.ContainsKey("ANTITRUNCATION@41717") && 
                        CurrentLog[i].StructuredDataDict["ANTITRUNCATION@41717"].ContainsKey("INIT"))
                    {
                        currentHash = System.Convert.FromBase64String(CurrentLog[i].StructuredDataDict["ANTITRUNCATION@41717"]["INIT"]);
                        //startTagFound = true;
                    }
                }
            }
            
            if(!endTagFound)
            {
                MessageBox.Show("No end tag found!", "Truncation check failed.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (result == 0)
            {
                MessageBox.Show("No truncation detected!", "Truncation check succeeded!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if( result > 0 )
                {
                    MessageBox.Show("Trunctation detected, " + result + " entries are missing.", "Truncation check failed.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Trunctation detected, " + Math.Abs(result) + " entries too many.", "Truncation check failed.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private int isTruncated(ref byte[] start, byte[] target, int count, int detectableLoss)
        {
            byte[] expected = null;
            byte[][] tmp = new byte[count + detectableLoss + 1][];
            //byte[] tmp = (byte[])start.Clone();

            tmp[0] = (byte[])start.Clone();
            for (int i = 1; i < tmp.Length; i++)
            {
                tmp[i] = new SHA256Managed().ComputeHash(tmp[i - 1]);

                if(i == count)
                {
                    expected = (byte[])tmp[i].Clone();
                }

            }

            if (Enumerable.SequenceEqual(expected, target))
            {
                start = (byte[])expected.Clone();
                return 0;
            }
            else
            {
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (Enumerable.SequenceEqual(tmp[i], target))
                    {
                        Console.WriteLine("Later match found! (i: " + i + ", max: " + count + ").");
                        return i - count;
                    }
                }

                return detectableLoss;
            }
        }
    }
}
