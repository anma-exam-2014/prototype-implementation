﻿using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Signers;
using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{
    class RsaVerifier : BaseVerifier
    {
        private RsaKeyParameters publickey;

        public RsaVerifier()
        {

        }

        public override void Verify(LogItem entry)
        {
            var data = entry.StructuredDataDict["RSASIGNATURE@41717"];
            var details = new SigInfoStruct() { Message = "" };
            entry.SigInfoStruct = details;

            if (data.ContainsKey("PUB_E") && data.ContainsKey("PUB_N"))
            {
                publickey = new RsaKeyParameters(false,
                    new BigInteger(data["PUB_N"].Replace(".", "")),
                    new BigInteger(data["PUB_E"].Replace(".", "")));
                details.Message = "New epoch started! See raw data above for details.";
                entry.SetVerificationOk();
                return;
            }

            //get signature
            byte[] signature = System.Convert.FromBase64String(data["SIGNATURE"]);

            //get raw message to hash
            entry.StructuredData = entry.StructuredData.Replace(data["SIGNATURE"], "");
            string msgString = entry.ToString();
            byte[] toHash = Encoding.ASCII.GetBytes(msgString);

            PssSigner eng = new PssSigner(new RsaEngine(), new Sha256Digest(), 32); //create new pss
            eng.Init(false, publickey); //initiate this one
            eng.BlockUpdate(toHash, 0, toHash.Length);
            if (!eng.VerifySignature(signature)) //verify with public key
            {
                entry.SetVerificationError("Signature could not be verified.");
                return;
            }
            entry.SetVerificationOk();
        }

        private class SigInfoStruct : LogItem.BaseSigInfoStruct
        {

            public string Message;

            public override string ToString()
            {
                return Message;
            }
        }
    }
}
