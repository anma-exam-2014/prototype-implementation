﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verifier
{
    public class VerificationKey
    {
        public string Key { get; set; }
        public string Type { set; get; }
        public DateTime Timestamp { set; get; }

        public VerificationKey(string type, string key, DateTime timestamp)
        {
            this.Key = key;
            this.Type = type;
            this.Timestamp = timestamp;
        }

        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
