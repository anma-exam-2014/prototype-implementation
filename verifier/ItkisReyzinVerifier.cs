﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Security.Cryptography;

namespace verifier
{
    class ItkisReyzinVerifier : BaseVerifier
    {
        private BigInteger N;
        private BigInteger V;
        private int T;
        private int L;

        private bool hasKey;
        private byte[] previousMessageHash;


        public ItkisReyzinVerifier()
        {
            hasKey = false;
        }

        public override void Verify(LogItem entry)
        {
            if(!hasKey)
            {
                hasKey = ParseKey(entry);
            }
            else
            {
                if(_Verify(entry))
                {
                    SigInfoStruct s = new SigInfoStruct();
                    s.Info = "Verification successful!";
                    entry.SigInfoStruct = s;
                    entry.SetVerificationOk();
                }
                else
                {

                    SigInfoStruct s = new SigInfoStruct();
                    s.Info = "Verification failed!";
                    entry.SigInfoStruct = s;
                    entry.SetVerificationError("Signature could not be verified.");
                }
            }
        }

        private bool ParseKey(LogItem entry)
        {
            var data = entry.StructuredDataDict["ITKISREYZINSIGNATURE@41717"];
            if (data == null)
            {
                return false;
            }

            try
            {
                N = BigInteger.Parse(data["PUB_N"].Replace(".", ""));
                V = BigInteger.Parse(data["PUB_V"].Replace(".", ""));
                T = int.Parse(data["PUB_T"].Replace(".", ""));
                L = int.Parse(data["PUB_L"].Replace(".", ""));
                if(data.ContainsKey("PREVIOUS"))
                {
                    previousMessageHash = System.Convert.FromBase64String(data["PREVIOUS"]);
                }

                entry.SetVerificationOk();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                entry.SetVerificationError("Parse failed.");
                return false;
            }
        }

        private bool _Verify(LogItem entry)
        {
            try
            {
                var data = entry.StructuredDataDict["ITKISREYZINSIGNATURE@41717"];
                BigInteger z =      BigInteger.Parse(data["SIG_Z"].Replace(".", ""));
                BigInteger sigma =  BigInteger.Parse(data["SIG_SIGMA"].Replace(".", ""));
                BigInteger e =      BigInteger.Parse(data["SIG_E"].Replace(".", ""));
                int j =             int.Parse(data["SIG_J"].Replace(".", ""));
                byte[] prevHash =   System.Convert.FromBase64String(data["PREVIOUS"]);

                if(previousMessageHash != null && !Enumerable.SequenceEqual(prevHash, previousMessageHash))
                {
                    entry.VerificationWarning = "Previous message(s) in database is missing!";
                }

                double tmp = (int)Math.Pow(10, Math.Log10(T) + 1);
                BigInteger min = BigInteger.Pow(2, L);
                BigInteger max = (min * new BigInteger((1.0 + ((double)j / (double)T)) * tmp)) / (int)tmp;

                if (e < min || e >= max || e % 2 == 0)
                {
                    return false;
                }
                if (z % N == 0)
                {
                    return false;
                }

                BigInteger yprim = (BigInteger.ModPow(z, e, N) * BigInteger.ModPow(V, sigma, N)) % N;

                entry.StructuredData = entry.StructuredData.Replace("SIG_Z=\"" + data["SIG_Z"] + "\"", "SIG_Z=\"\"");
                entry.StructuredData = entry.StructuredData.Replace("SIG_SIGMA=\"" + data["SIG_SIGMA"] + "\"", "SIG_SIGMA=\"\"");
                entry.StructuredData = entry.StructuredData.Replace("SIG_J=\"" + data["SIG_J"] + "\"", "SIG_J=\"\"");
                entry.StructuredData = entry.StructuredData.Replace("SIG_E=\"" + data["SIG_E"] + "\"", "SIG_E=\"\"");
                //entry.StructuredData = entry.StructuredData.Replace("PREVIOUS=\"" + data["PREVIOUS"] + "\"", "PREVIOUS=\"\"");


                var HASH_ALG = new SHA256Managed();

                byte[] jbyte = new BigInteger(j).ToByteArray();
                byte[] ebyte = e.ToByteArray();
                byte[] ybyte = yprim.ToByteArray();
                //byte[] messageHash = HASH_ALG.ComputeHash(Encoding.ASCII.GetBytes(entry.ToString()));
                byte[] message = Encoding.ASCII.GetBytes(entry.ToString());
                previousMessageHash = HASH_ALG.ComputeHash(message);

                HASH_ALG.TransformBlock(jbyte, 0, jbyte.Length, null, 0);
                HASH_ALG.TransformBlock(ebyte, 0, ebyte.Length, null, 0);
                HASH_ALG.TransformBlock(ybyte, 0, ybyte.Length, null, 0);
                HASH_ALG.TransformFinalBlock(message, 0, message.Length);
                //HASH_ALG.TransformBlock(message, 0, message.Length, null, 0);
                //HASH_ALG.TransformFinalBlock(prevHash, 0, prevHash.Length);

                IEnumerable<byte> hash = HASH_ALG.Hash.Reverse().ToArray().Concat(new byte[]{0});
                
                if (sigma == new BigInteger(hash.ToArray()))
                {
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }
    }

    class SigInfoStruct : LogItem.BaseSigInfoStruct
    {
        public string Info { set; get; }
        public override string ToString()
        {
            return Info;
        }
    }
}
