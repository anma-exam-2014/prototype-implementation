﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace verifier
{
    public partial class DetailsForm : Form
    {
        public DetailsForm(LogItem entry)
        {
            InitializeComponent();

            rawData.Text = entry.ToString(true);
            signInfo.Text = entry.SigInfo;
        }
    }
}
